.TH Wp2LaTeX 1 "2 Mar 2025"
.SH NAME
wp2latex \- Converter from MAC WP1.x, WP2,3,4.x, PC WP4.x, WP5.x, WP6.x .. 11.x to LaTeX
.SH SYNOPSIS
\fBwp2latex\fP [input_file [output_file]] [switches]

.SH DESCRIPTION
\fBwp2latex\fP It is program for conversion WordPerfect MAC WP1.x, MAC WP2,3,4.x,
PC WP4.x, WP5.x and WP6.x .. 11.x documents to the LaTeX 
typesetting system.
It is possible to convert a lot of features in the current version 
for example: Headers, Tables, Equations, Centered+Right+Left text,
a lot of extended characters (greek, math, cyrilic, accented)
and of course a normal text.
.SS Options
.TP
\fB-accents\fP
Use accents to support additional accents.
.TP
\fB-amssymb\fP
Use additional package AMSsymb with special characters.
.TP
\fB-arabtex\fP
Use arabtex.sty to support arrabic glyphs.
.TP
\fB-AmsMath\fP
Use AmsMath with special characters.
.TP
\fB-Arevmath\fP
Use arevmath with special characters.
.TP
\fB-bbm\fP
Use additional package bbm with special characters.
.TP
\fB-copyright\fP
Displays copyright information related to wp2latex package.
.TP
\fB-Cyrillic\fP
Use cyrillic.sty to support cyrillic script.
.TP
\fB-?\fP or \fB--help \fP
Write a short help on the screen. '-help images' lists supported image formats.
.TP
\fB-charset1\fP
switch internal WordPerfect char set to set 1. This option is default.
.TP
\fB-charsetCZ\fP
Switch internal WordPerfect char set to keybcs.
.TP
\fB-cjHebrew\fP
Allow Hebrew font translation.
.TP
\fB-cp-xxx\fP
switch output code table (LaTeX code page) to xxx. There are
currently supported these code pages:
852 (ISO8); 895 (Kamenicky); 1250 (Microsoft Windows); ISO8859-2;
866; 1251; KOI8R; UTF8
.TP
\fB-cp-styles\fP
This feature causes writing special styles for given codepages.
.TP
\fB-CurrentFontSet\fP
Change codepage for a current font. "Magical" WP's codepage 12.
.TP
\fB-extract-images\fP
Extract images into separate files.
.TP
\fB-fancyhdr\fP
Use package fancyhdr.sty for headers and footers.
.TP
\fB-fig-output-dir\fP
Use this directory for storing images. Prefer relative path. The relative
path is related to output .tex file. Default value='.'.
.TP
\fB-fix-spaces\fP
Try to fix more spaces by ~. This might by userfull when spacess
fills some space (e.g. in the table).
.TP
\fB-FontEnc-T1\fP
Use package fontenc.sty [T1] with additional characters.
.TP
\fB-force-xxx\fP
WP2LaTeX has sophisticaded way how to autodetect file format of given document.
Some file formats like WP4.x could not be fully detected. Turning this parameter
on switch to the selected conversion module in the case when autodetection
do not find any appropriate file format. xxx stands for conversion module shortcut.
These are WP3.x, WP4.x, WP5.x, WP6.x and even several non WP like abiword, 
Accent, MTEFF, OLE Stream, HTML, RTF, T602, UNICODE and WORD. Type 
Wp2LaTeX -v to obtain list of them. (e.g. If you have WP4.x documents it is
a good idea to use -force-WP4.x switch.)
.TP
\fB-FORCE-xxx\fP
This argument turns off any autodetection and enforces using of a given conversion
module. Use it only if you are sure about it and expect troubles, because your file 
might be corrupted.
.TP
\fB-fontenc-T1\fP
Use package fontenc.sty [T1] with additional characters. Available
for LaTeX 2.0e.
.TP
\fB-graphics\fP
Use style graphics.sty for Postscript images.
 This setting turns off any other setting for postscript images
 (e.g. -input-PS).
.TP
\fB-graphicx\fP
Use style graphicx.sty for Postscript images.
 This setting turns off any other setting for postscript images
 (e.g. -input-PS).
.TP
\fB-charset1\fP
Switch internal WordPerfect char set to set 1.
.TP
\fB-charsetCZ\fP
Switch internal WordPerfect char set to keybcs.
.TP
\fB-i\fP
Force input filename. The next argument is supposed as input file name.
.TP
\fB-ignore-tabs\fP
Remove all [TAB] symbols from WP document. [TAB] characters are usually
garbage and user may not want to convert them.
.TP
\fB-input-PS\fP
Use Postscript features in the LaTeX (mainly images).
.TP
\fB-l\fP
Force log filename. The next argument is supposed as file name of the log file.
.TP
\fB-L LANGUAGE\fP
Switch message translations to language LANGUAGE. It means that WP2LaTeX can
communicate with you with your native language. WP2LaTeX must be compiled
with gettext library for accessing this switch.
.TP
\fB-LaTeX2\fP
Optimize optput for LaTex 2.09 (default).
.TP
\fB-LaTeX3\fP
Optimize optput for LaTex 2.e or LaTeX 3.0
.TP
\fB-latexsym\fP
Use latexsym with special characters.
.TP
\fB-LineNo\fP
Use package LineNo for line numbering.
.TP
\fB-LongTable\fP
Use longtable for better tables.
.TP
\fB-MakeIdx\fP
Use makeidx for placing index.
.TP
\fB-memstream\fP
Do not create temporary files and store their data in memory.
Usually it speed up processing. (Works only if compiled with GCC >= 3.0)
.TP
\fB-mxedruli\fP
Use mxedruli for Georgia characters.
.TP
\fB-no-cjHebrew\fP
Disable Hebrew font translation.
.TP
\fB-no-columns\fP
Do not translate multicolumn text to more columns. The information about
newspaper columns will be lost.
.TP
\fB-noclobber\fP
Prevent overwriting existing files.
.TP
\fB-no-erase-strip\fP
Do not erase temporary files of wp2latex. You could extract nearly plain
text from those files. Even when this text cannot be directly used with LaTeX
it could be still useful for some tasks.
.TP
\fB-no-extract-images\fP
Turn all extracting images off.
.TP
\fB-no-input-PS\fP
Disable using anything from Postscript in the LaTeX.
.TP
\fB-no-lang-XXX\fP
Disable loading LaTeX localisation for a given language.
e.g. -no-lang-USenglish disables switching this locale.
.TP
\fB-nooptimizesection\fP
Turn a code, which optimizes and dectect section, off.
Expect more Tex errors if you do this.
.TP
\fB-no-safemode\fP
Disable checking features - faster conversion. But if your document is corrupted
the conversion results will be wrong.
.TP
\fB-no-shrink-images\fP
Turn off oversized image shrink.
.TP
\fB-notexchars\fP
All chars 32-128 will be converted to Tex sequencies (Default)
.TP
\fB-no-ulem\fP
Disable using of the style ulem.sty.
.TP
\fB-no-undoredo\fP
Expand all parts of text that might be marked as already deleted.
.TP
\fB-no-wasy\fP
Disable additional package Wasy2 with special characters.
.TP
\fB-o\fP
Force output filename. The next argument is supposed as input file name.
.TP
\fB-optimizesection\fP
Turn a code, which optimizes and dectect section, on (Default).
.TP
\fB-rsfs\fP
Allow using additional package mathrsfs with special characters.
.TP
\fB-s "password"\fP
Password for decoding WP5.x or WP4.x encrypted document. (WP4.x and WP5.x only)"
.TP
\fB-PiFont\fP
Use additional package pifont.
.TP
\fB-Rsfs\fP
Use mathrsfs with special characters.
.TP
\fB-S\fP or \fB-silent\fP
Do not echo output messages. Only errors are written.
.TP
\fB-scalerel\fP
Use additional package scalerel.
.TP
\fB-SS\fP or \fB-ssilent\fP
Do not output any message. If all errors are suppressed the output file might not
satisfy your expectation.
.TP
\fB-safemode\fP
Check safely consistency of each object and try to fix all
incorrect things. Good for corrupted WP files.
.TP
\fB-shrink-images\fP
Attempt to shrink oversized vector images. EPS processed by LaTeX has
fixed internal size limit.
.TP
\fB-texchars\fP
Do not interpret tex macros (all chars 32-128 will not be converted
to Tex sequences)
.TP
\fB-tipa\fP
Use additional package tipa with special characters.
.TP
\fB-ulem\fP
Use ulem.sty for better underlining. Some basic underlining features
are included in wp2latex.sty, but without this option it is not possible
to typeset underlined text longer than one line.
.TP
\fB-undoredo\fP
Process all Undo/Redo commands and emit last revision of text.
.TP
\fB-use-all\fP
Use all supported styles in WP2LaTex. Several styles are disabled in default.
On the other side, there are exclusive group of styles e.g. Graphics ones.
.TP
\fB-v\fP
Display information about current version of wp2latex and output a list 
of currently available conversion modules (e.g. WP4.x, WP5.x).
.TP
\fB-wasy\fP
Use additional package Wasy2 with special characters.


.SH FILES
\fI/usr/local/bin/wp2latex\fR
The main conversion program
\fIwp2latex.sty\fR
The style for future processing in the TeX system. Normally located in the
directory:
/usr/local/teTeX/share/texmf/tex/latex/wp2latex/
\fIendnotes.sty\fR
The style for including endnotes into LaTeX documents. This style was
downloaded from CTAN archive and is part of many LaTeX installations.
\fI/usr/share/locale/cs/LC_MESSAGES/\fR
\fI/usr/share/locale/ds/LC_MESSAGES/\fR
Localisation file of wp2latex. This file contains translation of all messages
into given language.


.SH SEE ALSO
latex(1), tex(1)

Visit www page 
.UR https://ftsoft.com.cz/wp2latex/wp2latex.htm
.UR http://78.108.103.11/~fojtik/wp2latex/wp2latex.htm
.UR http://ftsoft.wz.cz/wp2latex/wp2latex.htm
.UE

.SH AUTHORS
This Program has been rewritten by the last author Jaroslav Fojtik, Email
(try one of these): JaFojtik@seznam.cz, JaFojtik@yandex.com.

There are some other ports of this code. But now I hope, that
this release is best one.
.SH BUGS
Tabbings are sometimes interpreted incorrectly.
Captions of formulas is loosed.
Many WP features remain unconverted.
There remain many other LaTeX warnings during TeX processing.
