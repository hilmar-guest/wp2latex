Source: wp2latex
Section: text
Priority: optional
Maintainer: Hilmar Preuße <hille42@debian.org>
Build-Depends: debhelper-compat (= 13),
  tex-common,
  libjpeg-dev,
  libpng-dev,
  libwebp-dev (>= 0.5.0),
  dh-exec,
  texlive-latex-base <!nocheck>,
  texlive-latex-extra <!nocheck>,
  texlive-lang-other <!nocheck>,
  texlive-lang-czechslovak <!nocheck>,
  texlive-fonts-recommended <!nocheck>
Standards-Version: 4.6.2
Rules-Requires-Root: no
Homepage: http://ftsoft.com.cz/wp2latex/wp2latex.htm
Vcs-Browser: https://salsa.debian.org/hilmar/wp2latex
Vcs-Git: https://salsa.debian.org/hilmar/wp2latex.git

Package: wp2latex
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Suggests: wp2latex-styles
Description: Conversion of WordPerfect documents to LaTeX: the converter
 WP2LaTeX is a program for conversion WordPerfect documents to LaTeX.
 All known WP fileformats are supported: MAC WP1.x, WP2,3,4.x,
 PC WP WP3.x, WP4.x, WP5.x and WP6,7,8,9,10.x.
 It is possible to convert a lot of features. For example:
 Centered+Right+Left text, Endnotes, Formulas, Footers, Footnotes,
 Headers, Indentings, Tables, a lot of Extended characters (Greek, math,
 cyrilic) and of course a normal text.
 .
 This is the pure wp2latex converter to convert WPerfect files.

Package: wp2latex-styles
Architecture: all
Multi-Arch: foreign
Recommends: texlive-lang-other, texlive-science, texlive-latex-extra
Depends: ${misc:Depends}, tex-common
Replaces: wp2latex (<= 3.86-2)
Enhances: wp2latex
Breaks: wp2latex (<= 3.86-2)
Description: Conversion of WordPerfect documents to LaTeX: the LaTeX style files
 WP2LaTeX is a program for conversion WordPerfect documents to LaTeX.
 All known WP fileformats are supported: MAC WP1.x, WP2,3,4.x,
 PC WP WP3.x, WP4.x, WP5.x and WP6,7,8,9,10.x.
 It is possible to convert a lot of features. For example:
 Centered+Right+Left text, Endnotes, Formulas, Footers, Footnotes,
 Headers, Indentings, Tables, a lot of Extended characters (Greek, math,
 cyrilic) and of course a normal text.
 .
 These are the style files to compile LaTeX files created by wp2latex.
