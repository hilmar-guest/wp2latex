@echo off
echo    ************************** IMPORTANT ****************************
echo    *                       PLEASE READ THIS                        *
echo    ************************** IMPORTANT ****************************
echo    * This bat file is designed to install WP2LaTeX so that it can  *
echo    * be used with MiKTeX. Please be aware that both your texmf and *
echo    * localtexmf subdirectories will have to be located DIRECTLY in *
echo    * your root directory (C:). In all other cases, you will either *
echo    * have to edit this file or install WP2LaTeX by hand. Thank-you.*
echo    ***************************************************************** 

rem set LOCALEDIR=
set LATEXDIR=c:\texmf\miktex\bin
set LATEXTYPE=miktex

rem set LATEXDIR=g:\emtex\bin
rem set STYLEDIR=g:\emtex\texinput
rem set LATEXTYPE=emtex



choice /M "Are you sure that you would like to proceed with this operation?"
if errorlevel 2 goto rejectinstallation

choice /M "Are you running this batch file from a DOS box?"
if errorlevel 2 goto DOSbox


if exist .\install.bat cd ..


REM if not exist c:\localtexmf goto noproceed
if not exist wp2latex.faq echo Can't find file: wp2latex.faq
if not exist wp2latex.faq echo I need this file as a point of reference.
if not exist wp2latex.faq goto noproceed


rem Attempt to autodetect something
if exist instaler\inst_gt.exe instaler\inst_gt.exe
if exist igt.bat call igt.bat
del igt.bat

if "%LOCALEDIR%"=="" goto NoLocale
echo ******** Installing WP2LaTex localization messages
xcopy doc\locale\*.*  %LOCALEDIR% /s

:NoLocale



echo ******** Installing WP2LaTex executable
if not exist %LATEXDIR%\*.* echo Can't find MiKTeX installation.
if not exist %LATEXDIR%\*.* goto noproceed
chdir .\dos
if not exist wp2latex.exe echo Can't find file: wp2latex.exe
if not exist wp2latex.exe goto noproceed
copy wp2latex.exe %LATEXDIR%
chdir ..

if not exist %LATEXDIR%\wp2latex.exe goto unsuccessfulinstallation


echo ******** Installing WP2LaTex styles
if "%LATEXTYPE%"=="emtex" goto emtex
if not exist c:\localtexmf\tex\latex\WP2LaTeX\*.* md C:\localtexmf\tex\latex\WP2LaTeX
chdir .\styles.tex
if not exist wp2latex.sty echo Can't find file: wp2latex.sty
if not exist wp2latex.sty goto noproceed
copy *.* C:\localtexmf\tex\latex\WP2LaTeX
chdir ..

initexmf --admin --update-fndb

rem cls

if not exist c:\localtexmf\tex\latex\WP2LaTeX\wp2latex.sty goto unsuccessfulinstallation
goto stylesinstalled


:emtex
chdir .\styles.tex
if not exist wp2latex.sty echo Can't find file: wp2latex.sty
if not exist wp2latex.sty goto noproceed
copy *.* %STYLEDIR%
chdir ..



:stylesinstalled

echo WP2LaTeX has been installed successfully.
echo Please note that the file wp2latex.exe has been copied to
echo the subdirectory C:\texmf\miktex\bin. You can access it
echo directly just as you would your other MiKTeX programs.
echo Note also that the style files used by WP2LaTeX have been
echo copied to C:\localtexmf\tex\latex\WP2LaTeX and your MiKTeX
echo file-name database has been refreshed.
echo Enjoy WP2LaTeX!
goto end

:unsuccessfulinstallation
echo Houston, we have a problem. WP2LaTeX has NOT been installed correctly.
echo The problem is either that your WP2LaTeX style files (e.g., wp2latex.sty)
echo or wp2latex.exe have not been copied to the correct subdirectories.
echo Hopefully this should be enough information for you to figure out
echo what's gone wrong. Sorry.
goto end

:DOSbox
echo You must run this batch file from a DOS box.
goto end

:noproceed
echo Sorry, WP2LaTeX could not be installed. Either your subdirectories
echo are structured incorrectly for this batch installer to work or you
echo do not use MiKTeX. A real installer is on its way. In the meantime,
echo you should install WP2LaTeX by hand. Thanks for your patience.
goto end

:rejectinstallation
goto end

:end

pause

