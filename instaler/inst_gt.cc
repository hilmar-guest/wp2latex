
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <dir.h>

#include <strings.h>
#include <locale.h>

string & fGets2(FILE *f, string & pstr);
int GetPath(const char *FullName);


/* global functions and variables of gettext.cc */
#define EnvLanguage "LANGUAGE="

#ifdef __cplusplus
extern "C" {
#endif
void InitGettext(void);
#ifdef __cplusplus
}
#endif

#ifdef __gettext__
// #define HAVE_CATGETS 1
 #define ENABLE_NLS 1
 #include <locale.h>
 #include<libintl.h>
 #ifndef _
   #define _(EnString) gettext(EnString)
 #endif
#else
 #define _(arg) (arg)
#endif



string expand_spec(string name)
{
//#ifdef DEBUG
//  fprintf(cq->log,"\n!expand_spec(%s) ",name.ch);fflush(cq->log);
//#endif
int pos,end;
string temp;

while(StrStr(name,"$(")!=NULL)
	{
	pos=StrStr(name,"$(")-name();
	end=pos+2;
	while (name[end]!=0)
	   {
	   if (name[end]==')') break;
	   end++;
	   }
	if (name[end]==0) break;
	temp=copy(name,pos+2,end-pos-2);

        if (StrStr(temp,"$(")!=NULL)
                 temp=expand_spec(temp);
//puts(temp.ch);
        if(temp!="")
		{
                temp=getenv(temp);
		}
//puts(temp.ch);
        name=copy(name,0,pos)+temp+copy(name,end+1,length(name)-end-1);

        temp=name;//!!!!
//puts(temp.ch);
	}

return(temp);
}


void Proces_File(const string filename)
{
//#ifdef DEBUG
//  fprintf(cq->log,"\n!Proces_File(%s) ",filename.ch);fflush(cq->log);
//#endif
FILE *f;
string line,temp;
int i;

if((f=fopen(filename,"r"))==NULL) return;
while(!feof(f))
  {
  fGets2(f, line);
  line=trim(line);
  if(line=="")     continue;
  if(line[0]=='#') continue;

  temp="";
  for(i=0; i<length(line); i++)
           {
           if(isalpha(line[i])) temp+=line[i];
                           else break;
           }
  if(line[i++] != '=') continue;
  temp+='=';
  for(; i<length(line); i++)
           {
           if(!isalnum(line[i]) || (line[i]!='$') ||
                 (line[i]!='(') || (line[i]!=')') || (line[i]!='/') ||
                 (line[i]!='\\')|| (line[i]!=':')) temp+=line[i];
                                              else break;
           }
  if(temp!="") putenv(temp);
//puts(temp.ch);
  }
fclose(f);
}

/*This procedure provides inicialization  of gettext library.
  Its main task is to find a proper LOCALEDIR */
string GettextLocation(void)
{
//#ifdef DEBUG
//  fprintf(cq->log,"\n!InitGettext() ");fflush(cq->log);
//#endif
  string Locale_Dir;

#ifdef __DJGPP__
  Proces_File(expand_spec("$(DJGPP)"));
#endif

  Locale_Dir = expand_spec("$(LOCALEDIR)");

#ifdef __DJGPP__  /* The default localedir for DJGPP is: $(DJDIR)/share/locale/  */
  if (Locale_Dir=="")
      {
      Locale_Dir = expand_spec("$(DJDIR)");
      if (Locale_Dir=="%:/>DJGPP%")
  	 {
 	 Locale_Dir=getenv("DJGPP");
	 Locale_Dir=copy(Locale_Dir,0,GetPath(Locale_Dir));
  	 }
      if (Locale_Dir!="")
         {
	 Locale_Dir+="/share/locale";
         }
      else Locale_Dir = "c:/usr/local/share/locale";
      }
#else           /* The default localedir for UNIX is: /usr/share/locale/  */
if (Locale_Dir=="")
      {
//    Locale_Dir = "/usr/local/share/locale";
      Locale_Dir = "/usr/share/locale";
      }
#endif

  return Locale_Dir;
}


char *GettextBinLocation(string &TeXType)
{
struct ffblk ffblk;
char *PathMikTeX="c:\\texmf\\miktex\\bin\\*.*";
char *PathEmTeX="c:\\emtex\\bin\\*.*";
int i;
char c;

TeXType="";
for(c='c'; c<='g'; c++)
  {
  *PathMikTeX=c;
  *PathEmTeX=c;
  
  i=findfirst(PathMikTeX,&ffblk,0);
  if(!i) {TeXType="miktex";return(PathMikTeX);}
  i=findfirst(PathEmTeX,&ffblk,0);
  if(!i) {TeXType="emtex";return(PathEmTeX);}
  }

return("");
}


char *GettextStyleLocation(void)
{
struct ffblk ffblk;
char *PathMikTeX="c:\\localtexmf\\tex\\latex\\*.*";
char *PathEmTeX="c:\\emtex\\texinput\\*.*";
int i;
char c;

for(c='c'; c<='g'; c++)
  {
  *PathMikTeX=c;
  *PathEmTeX=c;
  
  i=findfirst(PathMikTeX,&ffblk,0);
  if(!i) return(PathMikTeX);
  i=findfirst(PathEmTeX,&ffblk,0);
  if(!i) return(PathEmTeX);
  }

return("");
}


int main (void)
{
FILE *F;
string locale,EXE,TeXType,STY;

locale=GettextLocation();
locale=replacesubstring(locale, "/","\\" );
EXE=GettextBinLocation(TeXType);
EXE=replacesubstring(EXE, "/","\\" );
EXE=replacesubstring(EXE, "\\*.*","" );
STY=GettextStyleLocation();
STY=replacesubstring(STY, "/","\\" );
STY=replacesubstring(STY, "\\*.*","" );

printf("<<<inst_gt>>> Path finder for WP2LaTeX installer\n");
printf("LOCALE PATH DETECTED: %s\n",locale());
printf("EXECUTABLE PATH DETECTED: %s\n",EXE());
printf("LaTeX TYPE DETECTED: %s\n",TeXType());
printf("STYLE PATH DETECTED: %s\n",STY());
puts("");

if( (F=fopen("igt.bat","w"))==NULL ) return(-2);

fprintf(F,"rem This bat file if generated automatically from inst_gt executable\n\n");
if(locale!="") fprintf(F,"set LOCALEDIR=%s\n",locale());
if(EXE!="") fprintf(F,"set LATEXDIR=%s\n",EXE());
if(TeXType!="") fprintf(F,"set LATEXTYPE=%s\n",TeXType());
if(STY!="") fprintf(F,"set STYLEDIR=%s\n",STY());

fclose(F);
return(0);
}

