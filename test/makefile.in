##########################################################################
#	     The test makefile for wp2latex package		         #
##########################################################################


.SUFFIXES: .tex .wp .wpd .dvi .602
.PHONY: test testltx testconv bigtest bigtestltx clean

LATEX = latex
OUTDIR = ./output
MKDIR = mkdir
WP2LATEX = ../sources.cc/wp2latex @WP2LATEXOPT@

.tex.dvi:
	$(LATEX) $*.tex

.wp.tex:
	$(WP2LATEX) -no-interactive -i $*.wp -o $*.tex

test: @TEST_LTX@

testltx: testconv $(MAKEFILE_LIST)
	cp ../styles.tex/*.sty $(OUTDIR)
	cp ../styles.tex/*.def $(OUTDIR)
	cd $(OUTDIR) ; latex crypt1.tex -halt-on-error
	cd $(OUTDIR) ; latex crypt3.tex -halt-on-error
	cd $(OUTDIR) ; latex crypt4.tex -halt-on-error
	cd $(OUTDIR) ; latex crypt5.tex -halt-on-error
	cd $(OUTDIR) ; latex equation.tex -halt-on-error
#	latex test.tex
	cd $(OUTDIR) ; latex chars5.tex -halt-on-error
	cd $(OUTDIR) ; latex ole6.tex -halt-on-error
	cd $(OUTDIR) ; latex hel_sk2.tex -halt-on-error
	cd $(OUTDIR) ; latex FormTab5FE.tex -halt-on-error
	cd $(OUTDIR) ; latex sample.tex -halt-on-error


testconv: $(MAKEFILE_LIST) | $(OUTDIR)
	$(WP2LATEX) -S -no-interactive -i crypt1.wp -o $(OUTDIR)/crypt1.tex -s FRIDRICH
	$(WP2LATEX) -S -no-interactive -i crypt3.wp -o $(OUTDIR)/crypt3.tex -s a
	$(WP2LATEX) -S -no-interactive -i crypt4.wp -o $(OUTDIR)/crypt4.tex -s wp4
	$(WP2LATEX) -S -no-interactive -i crypt5.wp -o $(OUTDIR)/crypt5.tex -s hello
	$(WP2LATEX) -S -no-interactive -i RTassays2.wp -o $(OUTDIR)/RTassays.tex
	$(WP2LATEX) -S -no-interactive -i algeria3.wp -o $(OUTDIR)/algeria3.tex
	$(WP2LATEX) -S -no-interactive -i torture4.wp -o $(OUTDIR)/torture.tex
	$(WP2LATEX) -S -no-interactive -i equation5.wp -o $(OUTDIR)/equation.tex
	$(WP2LATEX) -S -no-interactive -i test5.wp -o $(OUTDIR)/test.tex
	$(WP2LATEX) -S -no-interactive -i ole6.wpd -o $(OUTDIR)/ole6.tex
	$(WP2LATEX) -S -no-interactive -i hel_sk2.wpg -o $(OUTDIR)/hel_sk2.tex
	$(WP2LATEX) -S -no-interactive -i chars5.wp -o $(OUTDIR)/chars5.tex
	$(WP2LATEX) -S -no-interactive -i chars6.wp -o $(OUTDIR)/chars6.tex
	$(WP2LATEX) -S -no-interactive -i latin2.602 -o $(OUTDIR)/latin2.tex
	$(WP2LATEX) -S -no-interactive -i FormTab5FE.wp -o $(OUTDIR)/FormTab5FE.tex
	$(WP2LATEX) -S -no-interactive -i logo17.emf -o $(OUTDIR)/logo17.tex
	$(WP2LATEX) -S -no-interactive -i fant.wmf -o $(OUTDIR)/fant.tex
	$(WP2LATEX) -S -no-interactive -i sample.dcb -o $(OUTDIR)/sample.tex
	$(WP2LATEX) -S -no-interactive -i test.acc -o $(OUTDIR)/test.tex

# create destination directories
$(OUTDIR) :
	$(MKDIR) $@


bigtest: $(MAKEFILE_LIST) | $(OUTDIR)
	$(WP2LATEX) -S -i test5.wp -o $(OUTDIR)/test5_ALL.tex -use-all
	$(WP2LATEX) -S -i test5.wp -o $(OUTDIR)/test5_t.tex -tipa
	$(WP2LATEX) -S -i test5.wp -o $(OUTDIR)/test5__.tex -no-tipa
	$(WP2LATEX) -S -i test5.wp -o $(OUTDIR)/test5__b.tex -bbm
	$(WP2LATEX) -S -i test5.wp -o $(OUTDIR)/test5___.tex -no-bbm
	$(WP2LATEX) -S -i test5.wp -o $(OUTDIR)/test5____h.tex -cjhebrew
	$(WP2LATEX) -S -i test5.wp -o $(OUTDIR)/test5_____.tex -no-cjhebrew
	$(WP2LATEX) -S -i test5.wp -o $(OUTDIR)/test5_____c.tex -cyrillic
	$(WP2LATEX) -S -i test5.wp -o $(OUTDIR)/test5______.tex -no-cyrillic
	$(WP2LATEX) -S -i test5.wp -o $(OUTDIR)/test5______1.tex -fontenc-T1
	$(WP2LATEX) -S -i test5.wp -o $(OUTDIR)/test5_______.tex -no-fontenc-T1
	$(WP2LATEX) -S -no-interactive -cp UTF8 -i chars5.wp -o $(OUTDIR)/chars5_UTF8.tex
	$(WP2LATEX) -S -no-interactive -cp UTF8 -i chars6.wp -o $(OUTDIR)/chars6_UTF8.tex


bigtestltx: bigtest
	cp ../styles.tex/*.sty $(OUTDIR)
	cp ../styles.tex/*.def $(OUTDIR)
	@echo ####### Converting test5.wp -use-all
	cd $(OUTDIR) ; latex test5_ALL.tex
	@echo ####### Converting test5.wp -tipa
	cd $(OUTDIR) ; latex test5_t.tex
	@echo ####### Converting test5.wp -no-tipa
	cd $(OUTDIR) ; latex test5__.tex
	@echo ####### Converting test5.wp -bbm
	cd $(OUTDIR) ; latex test5__b.tex
	@echo ####### Converting test5.wp -no-bbm
	cd $(OUTDIR) ; latex test5___.tex
	@echo ####### Converting test5.wp -cjhebrew
	cd $(OUTDIR) ; latex test5____h.tex
	@echo ####### Converting test5.wp -no-cjhebrew
	cd $(OUTDIR) ; latex test5_____.tex
	@echo ####### Converting test5.wp -cyrilic
	cd $(OUTDIR) ; latex test5_____c.tex
	@echo ####### Converting test5.wp -no-cyrilic
	cd $(OUTDIR) ; latex test5______.tex
	@echo ####### Converting test5.wp -fontenc-T1
	cd $(OUTDIR) ; latex test5______1.tex
	@echo ####### Converting test5.wp -no-fontenc-T1
	cd $(OUTDIR) ; latex test5_______.tex

clean:
	rm -f *.tex *.dvi *.aux *.log *.ent *.toc *.pdf *.eps *.ps *.idx .nfs*
	rm -f *.sty *.def *.STR *.TBL
	rm -f $(OUTDIR)/*.tex $(OUTDIR)/*.dvi $(OUTDIR)/*.aux $(OUTDIR)/*.log $(OUTDIR)/*.ent $(OUTDIR)/*.idx $(OUTDIR)/*.toc
	rm -f $(OUTDIR)/*.sty $(OUTDIR)/*.def $(OUTDIR)/*.eps
#$(OUTDIR)/*.toc $(OUTDIR)/*.pdf $(OUTDIR)/*.ent $(OUTDIR)/*.toc $(OUTDIR)/*.pdf $(OUTDIR)/*.eps $(OUTDIR)/*.ps $(OUTDIR)/*.idx