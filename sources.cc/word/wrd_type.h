#ifndef __WRD_TYPE__
#define __WRD_TYPE__


#ifndef _FILETIME_
#define _FILETIME_
/* 64 bit number of 100 nanoseconds intervals since January 1, 1601 */
    typedef struct {
	uint32_t dwLowDateTime;
	uint32_t dwHighDateTime;
    } FILETIME;
#endif				/* _FILETIME_ */

typedef struct {
	uint16_t wIdent;			/* 0x0000 */
	uint16_t nFib;			/* 0x0002 */
	uint16_t nProduct;			/* 0x0004 */
	uint16_t lid;			/* 0x0006 */
	int16_t pnNext;			/* 0x0008 */

	uint8_t  fDot:1;			/* Bitfield 0x0001 0x000A */
	uint8_t  fGlsy:1;			/* Bitfield 0x0002 */
	uint8_t  fComplex:1;		/* Bitfield 0x0004 */
	uint8_t  fHasPic:1;		/* Bitfield 0x0008 */
	uint8_t  cQuickSaves:4;		/* Bitfield 0x00F0 */
	uint8_t  fEncrypted:1;		/* Bitfield 0x0100 */
	uint8_t  fWhichTblStm:1;		/* Bitfield 0x0200 */
	uint8_t  fReadOnlyRecommended:1;	/* Bitfield 0x0400 */
	uint8_t  fWriteReservation:1;	/* Bitfield 0x0800 */
	uint8_t  fExtChar:1;		/* Bitfield 0x1000 */
	uint8_t  fLoadOverride:1;		/* Bitfield 0x2000 */
	uint8_t  fFarEast:1;		/* Bitfield 0x4000 */
	uint8_t  fCrypto:1;		/* Bitfield 0x8000 */
	uint16_t nFibBack;//:16;		/* 0x000C */
	uint32_t lKey;			/* 0x000E */
	uint8_t  envr;//:8;		/* 0x0012 */
	uint8_t  fMac:1;			/* Bitfield 0x01 0x0013 */
	uint8_t  fEmptySpecial:1;		/* Bitfield 0x02 */
	uint8_t  fLoadOverridePage:1;	/* Bitfield 0x04 */
	uint8_t  fFutureSavedUndo:1;	/* Bitfield 0x08 */
	uint8_t  fWord97Saved:1;		/* Bitfield 0x10 */
	uint8_t  fSpare0:3;		/* Bitfield 0xFE */
	uint16_t chse;//:16;		/* 0x0014 *//*was chs */
	uint16_t chsTables;		/* 0x0016 */
	uint32_t fcMin;			/* 0x0018 */
	uint32_t fcMac;			/* 0x001C */
	uint16_t csw;			/* 0x0020 */
	uint16_t wMagicCreated;		/* 0x0022 */
	uint16_t wMagicRevised;		/* 0x0024 */
	uint16_t wMagicCreatedPrivate;	/* 0x0026 */
	uint16_t wMagicRevisedPrivate;	/* 0x0028 */
	int16_t pnFbpChpFirst_W6;		/* 0x002A */
	int16_t pnChpFirst_W6;		/* 0x002C */
	int16_t cpnBteChp_W6;		/* 0x002E */
	int16_t pnFbpPapFirst_W6;		/* 0x0030 */
	int16_t pnPapFirst_W6;		/* 0x0032 */
	int16_t cpnBtePap_W6;		/* 0x0034 */
	int16_t pnFbpLvcFirst_W6;		/* 0x0036 */
	int16_t pnLvcFirst_W6;		/* 0x0038 */
	int16_t cpnBteLvc_W6;		/* 0x003A */
	int16_t lidFE;			/* 0x003C */
	uint16_t clw;			/* 0x003E */
	int32_t cbMac;			/* 0x0040 */
	uint32_t lProductCreated;		/* 0x0044 */
	uint32_t lProductRevised;		/* 0x0048 */
	uint32_t ccpText;			/* 0x004C */
	int32_t ccpFtn;			/* 0x0050 */
	int32_t ccpHdr;			/* 0x0054 */
	int32_t ccpMcr;			/* 0x0058 */
	int32_t ccpAtn;			/* 0x005C */
	int32_t ccpEdn;			/* 0x0060 */
	int32_t ccpTxbx;			/* 0x0064 */
	int32_t ccpHdrTxbx;		/* 0x0068 */
	int32_t pnFbpChpFirst;		/* 0x006C */
	int32_t pnChpFirst;		/* 0x0070 */
	int32_t cpnBteChp;		/* 0x0074 */
	int32_t pnFbpPapFirst;		/* 0x0078 */
	int32_t pnPapFirst;		/* 0x007C */
	int32_t cpnBtePap;		/* 0x0080 */
	int32_t pnFbpLvcFirst;		/* 0x0084 */
	int32_t pnLvcFirst;		/* 0x0088 */
	int32_t cpnBteLvc;		/* 0x008C */
	int32_t fcIslandFirst;		/* 0x0090 */
	int32_t fcIslandLim;		/* 0x0094 */
	uint16_t cfclcb;			/* 0x0098 */
	int32_t fcStshfOrig;		/* 0x009A */
	uint32_t lcbStshfOrig;		/* 0x009E */
	int32_t fcStshf;			/* 0x00A2 */
	uint32_t lcbStshf;			/* 0x00A6 */
	int32_t fcPlcffndRef;		/* 0x00AA */
	uint32_t lcbPlcffndRef;		/* 0x00AE */
	int32_t fcPlcffndTxt;		/* 0x00B2 */
	uint32_t lcbPlcffndTxt;		/* 0x00B6 */
	int32_t fcPlcfandRef;		/* 0x00BA */
	uint32_t lcbPlcfandRef;		/* 0x00BE */
	int32_t fcPlcfandTxt;		/* 0x00C2 */
	uint32_t lcbPlcfandTxt;		/* 0x00C6 */
	int32_t fcPlcfsed;		/* 0x00CA */
	uint32_t lcbPlcfsed;		/* 0x00CE */
	int32_t fcPlcpad;			/* 0x00D2 */
	uint32_t lcbPlcpad;		/* 0x00D6 */
	int32_t fcPlcfphe;		/* 0x00DA */
	uint32_t lcbPlcfphe;		/* 0x00DE */
	int32_t fcSttbfglsy;		/* 0x00E2 */
	uint32_t lcbSttbfglsy;		/* 0x00E6 */
	int32_t fcPlcfglsy;		/* 0x00EA */
	uint32_t lcbPlcfglsy;		/* 0x00EE */
	int32_t fcPlcfhdd;		/* 0x00F2 */
	uint32_t lcbPlcfhdd;		/* 0x00F6 */
	int32_t fcPlcfbteChpx;		/* 0x00FA */
	uint32_t lcbPlcfbteChpx;		/* 0x00FE */
	int32_t fcPlcfbtePapx;		/* 0x0102 */
	uint32_t lcbPlcfbtePapx;		/* 0x0106 */
	int32_t fcPlcfsea;		/* 0x010A */
	uint32_t lcbPlcfsea;		/* 0x010E */
	int32_t fcSttbfffn;		/* 0x0112 */
	uint32_t lcbSttbfffn;		/* 0x0116 */
	int32_t fcPlcffldMom;		/* 0x011A */
	uint32_t lcbPlcffldMom;		/* 0x011E */
	int32_t fcPlcffldHdr;		/* 0x0122 */
	uint32_t lcbPlcffldHdr;		/* 0x0126 */
	int32_t fcPlcffldFtn;		/* 0x012A */
	uint32_t lcbPlcffldFtn;		/* 0x012E */
	int32_t fcPlcffldAtn;		/* 0x0132 */
	uint32_t lcbPlcffldAtn;		/* 0x0136 */
	int32_t fcPlcffldMcr;		/* 0x013A */
	uint32_t lcbPlcffldMcr;		/* 0x013E */
	int32_t fcSttbfbkmk;		/* 0x0142 */
	uint32_t lcbSttbfbkmk;		/* 0x0146 */
	int32_t fcPlcfbkf;		/* 0x014A */
	uint32_t lcbPlcfbkf;		/* 0x014E */
	int32_t fcPlcfbkl;		/* 0x0152 */
	uint32_t lcbPlcfbkl;		/* 0x0156 */
	int32_t fcCmds;			/* 0x015A */
	uint32_t lcbCmds;			/* 0x015E */
	int32_t fcPlcmcr;			/* 0x0162 */
	uint32_t lcbPlcmcr;		/* 0x0166 */
	int32_t fcSttbfmcr;		/* 0x016A */
	uint32_t lcbSttbfmcr;		/* 0x016E */
	int32_t fcPrDrvr;			/* 0x0172 */
	uint32_t lcbPrDrvr;		/* 0x0176 */
	int32_t fcPrEnvPort;		/* 0x017A */
	uint32_t lcbPrEnvPort;		/* 0x017E */
	int32_t fcPrEnvLand;		/* 0x0182 */
	uint32_t lcbPrEnvLand;		/* 0x0186 */
	int32_t fcWss;			/* 0x018A */
	uint32_t lcbWss;			/* 0x018E */
	int32_t fcDop;			/* 0x0192 */
	uint32_t lcbDop;			/* 0x0196 */
	int32_t fcSttbfAssoc;		/* 0x019A */
	uint32_t lcbSttbfAssoc;		/* 0x019E */
	int32_t fcClx;			/* 0x01A2 */
	uint32_t lcbClx;			/* 0x01A6 */
	int32_t fcPlcfpgdFtn;		/* 0x01AA */
	uint32_t lcbPlcfpgdFtn;		/* 0x01AE */
	int32_t fcAutosaveSource;		/* 0x01B2 */
	uint32_t lcbAutosaveSource;	/* 0x01B6 */
	int32_t fcGrpXstAtnOwners;	/* 0x01BA */
	uint32_t lcbGrpXstAtnOwners;	/* 0x01BE */
	int32_t fcSttbfAtnbkmk;		/* 0x01C2 */
	uint32_t lcbSttbfAtnbkmk;		/* 0x01C6 */
	int32_t fcPlcdoaMom;		/* 0x01CA */
	uint32_t lcbPlcdoaMom;		/* 0x01CE */
	int32_t fcPlcdoaHdr;		/* 0x01D2 */
	uint32_t lcbPlcdoaHdr;		/* 0x01D6 */
	int32_t fcPlcspaMom;		/* 0x01DA */
	uint32_t lcbPlcspaMom;		/* 0x01DE */
	int32_t fcPlcspaHdr;		/* 0x01E2 */
	uint32_t lcbPlcspaHdr;		/* 0x01E6 */
	int32_t fcPlcfAtnbkf;		/* 0x01EA */
	uint32_t lcbPlcfAtnbkf;		/* 0x01EE */
	int32_t fcPlcfAtnbkl;		/* 0x01F2 */
	uint32_t lcbPlcfAtnbkl;		/* 0x01F6 */
	int32_t fcPms;			/* 0x01FA */
	uint32_t lcbPms;			/* 0x01FE */
	int32_t fcFormFldSttbs;		/* 0x0202 */
	uint32_t lcbFormFldSttbs;		/* 0x0206 */
	int32_t fcPlcfendRef;		/* 0x020A */
	uint32_t lcbPlcfendRef;		/* 0x020E */
	int32_t fcPlcfendTxt;		/* 0x0212 */
	uint32_t lcbPlcfendTxt;		/* 0x0216 */
	int32_t fcPlcffldEdn;		/* 0x021A */
	uint32_t lcbPlcffldEdn;		/* 0x021E */
	int32_t fcPlcfpgdEdn;		/* 0x0222 */
	uint32_t lcbPlcfpgdEdn;		/* 0x0226 */
	int32_t fcDggInfo;		/* 0x022A */
	uint32_t lcbDggInfo;		/* 0x022E */
	int32_t fcSttbfRMark;		/* 0x0232 */
	uint32_t lcbSttbfRMark;		/* 0x0236 */
	int32_t fcSttbCaption;		/* 0x023A */
	uint32_t lcbSttbCaption;		/* 0x023E */
	int32_t fcSttbAutoCaption;	/* 0x0242 */
	uint32_t lcbSttbAutoCaption;	/* 0x0246 */
	int32_t fcPlcfwkb;		/* 0x024A */
	uint32_t lcbPlcfwkb;		/* 0x024E */
	int32_t fcPlcfspl;		/* 0x0252 */
	uint32_t lcbPlcfspl;		/* 0x0256 */
	int32_t fcPlcftxbxTxt;		/* 0x025A */
	uint32_t lcbPlcftxbxTxt;		/* 0x025E */
	int32_t fcPlcffldTxbx;		/* 0x0262 */
	uint32_t lcbPlcffldTxbx;		/* 0x0266 */
	int32_t fcPlcfhdrtxbxTxt;		/* 0x026A */
	uint32_t lcbPlcfhdrtxbxTxt;	/* 0x026E */
	int32_t fcPlcffldHdrTxbx;		/* 0x0272 */
	uint32_t lcbPlcffldHdrTxbx;	/* 0x0276 */
	int32_t fcStwUser;		/* 0x027A */
	uint32_t lcbStwUser;		/* 0x027E */
	int32_t fcSttbttmbd;		/* 0x0282 */
	uint32_t cbSttbttmbd;		/* 0x0286 */
	int32_t fcUnused;			/* 0x028A */
	uint32_t lcbUnused;		/* 0x028E */
	int32_t fcPgdMother;		/* 0x0292 */
	uint32_t lcbPgdMother;		/* 0x0296 */
	int32_t fcBkdMother;		/* 0x029A */
	uint32_t lcbBkdMother;		/* 0x029E */
	int32_t fcPgdFtn;			/* 0x02A2 */
	uint32_t lcbPgdFtn;		/* 0x02A6 */
	int32_t fcBkdFtn;			/* 0x02AA */
	uint32_t lcbBkdFtn;		/* 0x02AE */
	int32_t fcPgdEdn;			/* 0x02B2 */
	uint32_t lcbPgdEdn;		/* 0x02B6 */
	int32_t fcBkdEdn;			/* 0x02BA */
	uint32_t lcbBkdEdn;		/* 0x02BE */
	int32_t fcSttbfIntlFld;		/* 0x02C2 */
	uint32_t lcbSttbfIntlFld;		/* 0x02C6 */
	int32_t fcRouteSlip;		/* 0x02CA */
	uint32_t lcbRouteSlip;		/* 0x02CE */
	int32_t fcSttbSavedBy;		/* 0x02D2 */
	uint32_t lcbSttbSavedBy;		/* 0x02D6 */
	int32_t fcSttbFnm;		/* 0x02DA */
	uint32_t lcbSttbFnm;		/* 0x02DE */
	int32_t fcPlcfLst;		/* 0x02E2 */
	uint32_t lcbPlcfLst;		/* 0x02E6 */
	int32_t fcPlfLfo;			/* 0x02EA */
	uint32_t lcbPlfLfo;		/* 0x02EE */
	int32_t fcPlcftxbxBkd;		/* 0x02F2 */
	uint32_t lcbPlcftxbxBkd;		/* 0x02F6 */
	int32_t fcPlcftxbxHdrBkd;		/* 0x02FA */
	uint32_t lcbPlcftxbxHdrBkd;	/* 0x02FE */
	int32_t fcDocUndo;		/* 0x0302 */
	uint32_t lcbDocUndo;		/* 0x0306 */
	int32_t fcRgbuse;			/* 0x030A */
	uint32_t lcbRgbuse;		/* 0x030E */
	int32_t fcUsp;			/* 0x0312 */
	uint32_t lcbUsp;			/* 0x0316 */
	int32_t fcUskf;			/* 0x031A */
	uint32_t lcbUskf;			/* 0x031E */
	int32_t fcPlcupcRgbuse;		/* 0x0322 */
	uint32_t lcbPlcupcRgbuse;		/* 0x0326 */
	int32_t fcPlcupcUsp;		/* 0x032A */
	uint32_t lcbPlcupcUsp;		/* 0x032E */
	int32_t fcSttbGlsyStyle;		/* 0x0332 */
	uint32_t lcbSttbGlsyStyle;		/* 0x0336 */
	int32_t fcPlgosl;			/* 0x033A */
	uint32_t lcbPlgosl;		/* 0x033E */
	int32_t fcPlcocx;			/* 0x0342 */
	uint32_t lcbPlcocx;		/* 0x0346 */
	int32_t fcPlcfbteLvc;		/* 0x034A */
	uint32_t lcbPlcfbteLvc;		/* 0x034E */
	FILETIME ftModified;		/* 0x0352 */
	int32_t fcPlcflvc;		/* 0x035A */
	uint32_t lcbPlcflvc;		/* 0x035E */
	int32_t fcPlcasumy;		/* 0x0362 */
	uint32_t lcbPlcasumy;		/* 0x0366 */
	int32_t fcPlcfgram;		/* 0x036A */
	uint32_t lcbPlcfgram;		/* 0x036E */
	int32_t fcSttbListNames;		/* 0x0372 */
	uint32_t lcbSttbListNames;		/* 0x0376 */
	int32_t fcSttbfUssr;		/* 0x037A */
	uint32_t lcbSttbfUssr;		/* 0x037E */

	/* Added for Word 2 */
	uint32_t Spare;			/* 0x000E */
	uint16_t rgwSpare0[3];		/* 0x0012 */
	uint32_t fcSpare0;			/* 0x0024 */
	uint32_t fcSpare1;			/* 0x0028 */
	uint32_t fcSpare2;			/* 0x002C */
	uint32_t fcSpare3;			/* 0x0030 */
	uint32_t ccpSpare0;		/* 0x0048 */
	uint32_t ccpSpare1;		/* 0x004C */
	uint32_t ccpSpare2;		/* 0x0050 */

	uint32_t ccpSpare3;		/* 0x0054 */
	uint32_t fcPlcfpgd;		/* 0x0082 */
	uint16_t cbPlcfpgd;		/* 0x0086 */

	uint32_t fcSpare5;			/* 0x0130 */
	uint16_t cbSpare5;			/* 0x0136 */
	uint32_t fcSpare6;			/* 0x0130 */
	uint16_t cbSpare6;			/* 0x0136 */
	uint16_t wSpare4;			/* 0x013C */
	} FIB;

typedef enum {
	cbATRD = 30,
	cbANLD = 84,
	cbANLV = 16,
	cbASUMY = 4,
	cbASUMYI = 12,
	cbBTE = 4,
	cbBKD = 6,
	cbBKF = 4,
	cbBKL = 2,
	cbBRC = 4,
	cbBRC10 = 2,
	cbCHP = 136,
	cbDTTM = 4,
	cbDCS = 2,
	cbDOGRID = 10,
	cbDOPTYPOGRAPHY = 310,
	cbFSPA = 26,
	cbFIB = 898,
	cbLSPD = 4,
	cbOLST = 212,
	cbNUMRM = 128,
	cbPGD = 10,
	cbPHE = 12,
	cbPAP = 610,
	cbPCD = 8,
	/*
	   cbPLC
	 */
	cbPRM = 2,
	cbRS = 16,
	cbRR = 4,
	cbSED = 12,
	cbSEP = 704,
	cbSHD = 2,
	cbTBD = 1,
	cbTC = 20,
	cbTLP = 4,
	cbTAP = 1728,
	cbWKB = 12,
	cbLSTF = 28,
	cbFDOA = 6,
	cbFTXBXS = 22,

	cb7DOP = 88,

	cb6BTE = 2,
	cb6FIB = 682,
	cb6PHE = 6,
	cb6ANLD = 52,
	cb6BRC = 2,
	cb6DOP = 84,
	cb6PGD = 6,
	cb6TC = 10,
	cb6CHP = 42
    } cbStruct;

    typedef enum _SprmName {
	/*
	   these ones are ones I made up entirely to match
	   unnamed patterns in word 95 files, whose
	   purpose is currently unknown
	 */
	sprmTUNKNOWN1 = 0xD400,
	sprmPUNKNOWN2 = 0x2400,	/* word 7 0x39 */
	sprmPUNKNOWN3 = 0x2401,	/* word 7 0x3a */
	sprmPUNKNOWN4 = 0x4400,	/* word 7 0x3b */
	sprmCUNKNOWN5 = 0x4800,	/* word 7 0x6f */
	sprmCUNKNOWN6 = 0x4801,	/* word 7 0x70 */
	sprmCUNKNOWN7 = 0x4802,	/* word 7 0x71 */


	/*
	   these ones showed up in rgsprmPrm and are mostly
	   out of date i reckon
	 */
	sprmNoop = 0x0000,	/* this makes sense */
	sprmPPnbrRMarkNot = 0x0000,	/* never seen this one */

	/*
	   this subset were not listed in word 8, but i recreated them
	   from the word 8 guidelines and the original word 6, so
	   basically they will blow things up when ms decides to reuse them
	   in word 2000 or later versions, but what the hell...
	 */
	sprmCFStrikeRM = 0x0841,
	sprmPNLvlAnm = 0x240D,
	sprmCFtc = 0x483D,
	/*end subset */

	/*
	   one of the sprm's that shows up in word 6 docs is "0", which
	   appears to be either the pap.istd or just an index, seeing
	   as the word 6 people didn't list it, lets just ignore it.
	   as it only happens in word 6 docs, our code happens to
	   function fine in the current setup, but at some stage
	   im sure it will bite me hard
	 */

	sprmPIstd = 0x4600,
	sprmPIstdPermute = 0xC601,
	sprmPIncLvl = 0x2602,
	sprmPJc = 0x2403,
	sprmPFSideBySide = 0x2404,
	sprmPFKeep = 0x2405,
	sprmPFKeepFollow = 0x2406,
	sprmPFPageBreakBefore = 0x2407,
	sprmPBrcl = 0x2408,
	sprmPBrcp = 0x2409,
	sprmPIlvl = 0x260A,
	sprmPIlfo = 0x460B,
	sprmPFNoLineNumb = 0x240C,
	sprmPChgTabsPapx = 0xC60D,
	sprmPDxaRight = 0x840E,
	sprmPDxaLeft = 0x840F,
	sprmPNest = 0x4610,
	sprmPDxaLeft1 = 0x8411,
	sprmPDyaLine = 0x6412,
	sprmPDyaBefore = 0xA413,
	sprmPDyaAfter = 0xA414,
	sprmPChgTabs = 0xC615,
	sprmPFInTable = 0x2416,
	sprmPFTtp = 0x2417,
	sprmPDxaAbs = 0x8418,
	sprmPDyaAbs = 0x8419,
	sprmPDxaWidth = 0x841A,
	sprmPPc = 0x261B,
	sprmPBrcTop10 = 0x461C,
	sprmPBrcLeft10 = 0x461D,
	sprmPBrcBottom10 = 0x461E,
	sprmPBrcRight10 = 0x461F,
	sprmPBrcBetween10 = 0x4620,
	sprmPBrcBar10 = 0x4621,
	sprmPDxaFromText10 = 0x4622,
	sprmPWr = 0x2423,
	sprmPBrcTop = 0x6424,
	sprmPBrcLeft = 0x6425,
	sprmPBrcBottom = 0x6426,
	sprmPBrcRight = 0x6427,
	sprmPBrcBetween = 0x6428,
	sprmPBrcBar = 0x6629,
	sprmPFNoAutoHyph = 0x242A,
	sprmPWHeightAbs = 0x442B,
	sprmPDcs = 0x442C,
	sprmPShd = 0x442D,
	sprmPDyaFromText = 0x842E,
	sprmPDxaFromText = 0x842F,
	sprmPFLocked = 0x2430,
	sprmPFWidowControl = 0x2431,
	sprmPRuler = 0xC632,
	sprmPFKinsoku = 0x2433,
	sprmPFWordWrap = 0x2434,
	sprmPFOverflowPunct = 0x2435,
	sprmPFTopLinePunct = 0x2436,
	sprmPFAutoSpaceDE = 0x2437,
	sprmPFAutoSpaceDN = 0x2438,
	sprmPWAlignFont = 0x4439,
	sprmPFrameTextFlow = 0x443A,
	sprmPISnapBaseLine = 0x243B,
	sprmPAnld = 0xC63E,
	sprmPPropRMark = 0xC63F,
	sprmPOutLvl = 0x2640,
	sprmPFBiDi = 0x2441,
	sprmPFNumRMIns = 0x2443,
	sprmPCrLf = 0x2444,
	sprmPNumRM = 0xC645,
	sprmPHugePapx = 0x6645,
	sprmPHugePapx2 = 0x6646,	/* this is the one I have found in
					   the wild, maybe the doc is incorrect
					   in numbering it 6645 C. */
	sprmPFUsePgsuSettings = 0x2447,
	sprmPFAdjustRight = 0x2448,

	sprmCFRMarkDel = 0x0800,
	sprmCFRMark = 0x0801,
	sprmCFFldVanish = 0x0802,
	sprmCPicLocation = 0x6A03,
	sprmCIbstRMark = 0x4804,
	sprmCDttmRMark = 0x6805,
	sprmCFData = 0x0806,
	sprmCIdslRMark = 0x4807,
	sprmCChs = 0xEA08,
	sprmCSymbol = 0x6A09,
	sprmCFOle2 = 0x080A,
	sprmCIdCharType = 0x480B,
	sprmCHighlight = 0x2A0C,
	sprmCObjLocation = 0x680E,
	sprmCFFtcAsciSymb = 0x2A10,
	sprmCIstd = 0x4A30,
	sprmCIstdPermute = 0xCA31,
	sprmCDefault = 0x2A32,
	sprmCPlain = 0x2A33,
	sprmCKcd = 0x2A34,
	sprmCFBold = 0x0835,
	sprmCFItalic = 0x0836,
	sprmCFStrike = 0x0837,
	sprmCFOutline = 0x0838,
	sprmCFShadow = 0x0839,
	sprmCFSmallCaps = 0x083A,
	sprmCFCaps = 0x083B,
	sprmCFVanish = 0x083C,
	sprmCFtcDefault = 0x4A3D,
	sprmCKul = 0x2A3E,
	sprmCSizePos = 0xEA3F,
	sprmCDxaSpace = 0x8840,
	sprmCLid = 0x4A41,
	sprmCIco = 0x2A42,
	sprmCHps = 0x4A43,
	sprmCHpsInc = 0x2A44,
	sprmCHpsPos = 0x4845,
	sprmCHpsPosAdj = 0x2A46,
	sprmCMajority = 0xCA47,
	sprmCIss = 0x2A48,
	sprmCHpsNew50 = 0xCA49,
	sprmCHpsInc1 = 0xCA4A,
	sprmCHpsKern = 0x484B,
	sprmCMajority50 = 0xCA4C,
	sprmCHpsMul = 0x4A4D,
	sprmCYsri = 0x484E,
	sprmCRgFtc0 = 0x4A4F,
	sprmCRgFtc1 = 0x4A50,
	sprmCRgFtc2 = 0x4A51,
	sprmCCharScale = 0x4852,
	sprmCFDStrike = 0x2A53,
	sprmCFImprint = 0x0854,
	sprmCFSpec = 0x0855,
	sprmCFObj = 0x0856,
	sprmCPropRMark = 0xCA57,
	sprmCFEmboss = 0x0858,
	sprmCSfxText = 0x2859,
	sprmCFBiDi = 0x085A,
	sprmCFDiacColor = 0x085B,
	sprmCFBoldBi = 0x085C,
	sprmCFItalicBi = 0x085D,
	sprmCFtcBi = 0x4A5E,
	sprmCLidBi = 0x485F,
	sprmCIcoBi = 0x4A60,
	sprmCHpsBi = 0x4A61,
	sprmCDispFldRMark = 0xCA62,
	sprmCIbstRMarkDel = 0x4863,
	sprmCDttmRMarkDel = 0x6864,
	sprmCBrc = 0x6865,
	sprmCShd = 0x4866,
	sprmCIdslRMarkDel = 0x4867,
	sprmCFUsePgsuSettings = 0x0868,
	sprmCCpg = 0x486B,
	sprmCRgLid0 = 0x486D,
	sprmCRgLid1 = 0x486E,
	sprmCIdctHint = 0x286F,

	sprmPicBrcl = 0x2E00,
	sprmPicScale = 0xCE01,
	sprmPicBrcTop = 0x6C02,
	sprmPicBrcLeft = 0x6C03,
	sprmPicBrcBottom = 0x6C04,
	sprmPicBrcRight = 0x6C05,

	sprmScnsPgn = 0x3000,
	sprmSiHeadingPgn = 0x3001,
	sprmSOlstAnm = 0xD202,
	sprmSDxaColWidth = 0xF203,
	sprmSDxaColSpacing = 0xF204,
	sprmSFEvenlySpaced = 0x3005,
	sprmSFProtected = 0x3006,
	sprmSDmBinFirst = 0x5007,
	sprmSDmBinOther = 0x5008,
	sprmSBkc = 0x3009,
	sprmSFTitlePage = 0x300A,
	sprmSCcolumns = 0x500B,
	sprmSDxaColumns = 0x900C,
	sprmSFAutoPgn = 0x300D,
	sprmSNfcPgn = 0x300E,
	sprmSDyaPgn = 0xB00F,
	sprmSDxaPgn = 0xB010,
	sprmSFPgnRestart = 0x3011,
	sprmSFEndnote = 0x3012,
	sprmSLnc = 0x3013,
	sprmSGprfIhdt = 0x3014,
	sprmSNLnnMod = 0x5015,
	sprmSDxaLnn = 0x9016,
	sprmSDyaHdrTop = 0xB017,
	sprmSDyaHdrBottom = 0xB018,
	sprmSLBetween = 0x3019,
	sprmSVjc = 0x301A,
	sprmSLnnMin = 0x501B,
	sprmSPgnStart = 0x501C,
	sprmSBOrientation = 0x301D,
	sprmSBCustomize = 0x301E,
	sprmSXaPage = 0xB01F,
	sprmSYaPage = 0xB020,
	sprmSDxaLeft = 0xB021,
	sprmSDxaRight = 0xB022,
	sprmSDyaTop = 0x9023,
	sprmSDyaBottom = 0x9024,
	sprmSDzaGutter = 0xB025,
	sprmSDmPaperReq = 0x5026,
	sprmSPropRMark = 0xD227,
	sprmSFBiDi = 0x3228,
	sprmSFFacingCol = 0x3229,
	sprmSFRTLGutter = 0x322A,
	sprmSBrcTop = 0x702B,
	sprmSBrcLeft = 0x702C,
	sprmSBrcBottom = 0x702D,
	sprmSBrcRight = 0x702E,
	sprmSPgbProp = 0x522F,
	sprmSDxtCharSpace = 0x7030,
	sprmSDyaLinePitch = 0x9031,
	sprmSClm = 0x5032,
	sprmSTextFlow = 0x5033,

	sprmTJc = 0x5400,
	sprmTDxaLeft = 0x9601,
	sprmTDxaGapHalf = 0x9602,
	sprmTFCantSplit = 0x3403,
	sprmTTableHeader = 0x3404,
	sprmTTableBorders = 0xD605,
	sprmTDefTable10 = 0xD606,
	sprmTDyaRowHeight = 0x9407,
	sprmTDefTable = 0xD608,
	sprmTDefTableShd = 0xD609,
	sprmTTlp = 0x740A,
	sprmTFBiDi = 0x560B,
	sprmTHTMLProps = 0x740C,
	sprmTSetBrc = 0xD620,
	sprmTInsert = 0x7621,
	sprmTDelete = 0x5622,
	sprmTDxaCol = 0x7623,
	sprmTMerge = 0x5624,
	sprmTSplit = 0x5625,
	sprmTSetBrc10 = 0xD626,
	sprmTSetShd = 0x7627,
	sprmTSetShdOdd = 0x7628,
	sprmTTextFlow = 0x7629,
	sprmTDiagLine = 0xD62A,
	sprmTVertMerge = 0xD62B,
	sprmTVertAlign = 0xD62C
    } SprmName;


typedef struct _Sprm {	/*16 bits in total */
	unsigned ispmd:9;		/*ispmd unique identifier within sgc group */
	unsigned fSpec:1;		/*fSpec sprm requires special handling */
	unsigned sgc:3;		/*sgc   sprm group; type of sprm (PAP, CHP, etc) */
	unsigned spra:3;		/*spra  size of sprm argument */
    } Sprm;


typedef struct _PRM {	/*full total of bits should be 16 */
	uint16_t fComplex:1;
	union {
	    struct {
		uint16_t isprm:7;
		uint16_t val:8;
	    } var1;
	    struct {
		uint16_t igrpprl:15;
	    } var2;
	} para;
    } PRM;

typedef struct _PCD {	/*this should be 16 bits for bitfields */
	uint16_t fNoParaLast:1;
	uint16_t fPaphNil:1;
	uint16_t fCopied:1;
	uint16_t reserved:5;
	uint16_t fn:8;
	uint32_t fc;
	PRM prm;
    } PCD;

typedef struct _CLX {
	PCD *pcd;
	uint32_t *pos;
	uint32_t nopcd;

	uint16_t grpprl_count;
	uint16_t *cbGrpprl;
	uint8_t **grpprl;
    } CLX;

typedef struct _BRC {
	uint8_t dptLineWidth;
	uint8_t brcType;
	uint8_t ico;
	uint8_t dptSpace:5;
	uint8_t fShadow:1;
	uint8_t fFrame:1;
	uint8_t reserved:1;
    } BRC;

typedef struct _DTTM {
	unsigned mint:6;
	unsigned hr:5;
	unsigned dom:5;
	unsigned mon:4;
	unsigned yr:9;
	unsigned wdy:3;
    } DTTM;


typedef struct _ANLV {
	uint8_t nfc;
	uint8_t cxchTextBefore;

	unsigned cxchTextAfter:8;
	unsigned jc:2;
	unsigned fPrev:1;
	unsigned fHang:1;
	unsigned fSetBold:1;
	unsigned fSetItalic:1;
	unsigned fSetSmallCaps:1;
	unsigned fSetCaps:1;
	unsigned fSetStrike:1;
	unsigned fSetKul:1;
	unsigned fPrevSpace:1;
	unsigned fBold:1;
	unsigned fItalic:1;
	unsigned fSmallCaps:1;
	unsigned fCaps:1;
	unsigned fStrike:1;
	unsigned kul:3;
	unsigned ico:5;

	int16_t ftc;
	uint16_t hps;
	uint16_t iStartAt;
	uint16_t dxaIndent;
	uint16_t dxaSpace;
    } ANLV;

typedef uint16_t XCHAR;

typedef struct _OLST {
	ANLV rganlv[9];
	uint8_t fRestartHdr;
	uint8_t fSpareOlst2;
	uint8_t fSpareOlst3;
	uint8_t fSpareOlst4;
	XCHAR rgxch[64];
    } OLST;


typedef struct _SEP {
	uint8_t bkc;
	uint8_t fTitlePage;
	int8_t fAutoPgn;
	uint8_t nfcPgn;
	uint8_t fUnlocked;
	uint8_t cnsPgn;
	uint8_t fPgnRestart;
	uint8_t fEndNote;
	uint8_t lnc;
	int8_t grpfIhdt;
	uint16_t nLnnMod;
	int32_t dxaLnn;
	int16_t dxaPgn;
	int16_t dyaPgn;
	int8_t fLBetween;
	int8_t vjc;
	uint16_t dmBinFirst;
	uint16_t dmBinOther;
	uint16_t dmPaperReq;
	BRC brcTop;
	BRC brcLeft;
	BRC brcBottom;
	BRC brcRight;
	int16_t fPropRMark;
	int16_t ibstPropRMark;
	DTTM dttmPropRMark;
	int32_t dxtCharSpace;
	int32_t dyaLinePitch;
	uint16_t clm;
	int16_t reserved1;
	uint8_t dmOrientPage;
	uint8_t iHeadingPgn;
	uint16_t pgnStart;
	int16_t lnnMin;
	int16_t wTextFlow;
	int16_t reserved2;

	uint16_t pgbProp;		//:16;
	uint8_t pgbApplyTo:3;
	uint8_t pgbPageDepth:2;
	uint8_t pgbOffsetFrom:3;
	uint8_t reserved:8;

	uint32_t xaPage;
	uint32_t yaPage;
	uint32_t xaPageNUp;
	uint32_t yaPageNUp;
	uint32_t dxaLeft;
	uint32_t dxaRight;
	int32_t dyaTop;
	int32_t dyaBottom;
	uint32_t dzaGutter;
	uint32_t dyaHdrTop;
	uint32_t dyaHdrBottom;
	int16_t ccolM1;
	int8_t fEvenlySpaced;
	int8_t reserved3;
	int32_t dxaColumns;
	int32_t rgdxaColumnWidthSpacing[89];
	int32_t dxaColumnWidth;
	uint8_t dmOrientFirst;
	uint8_t fLayout;
	int16_t reserved4;
	OLST olstAnm;
    } SEP;

typedef struct _SED {
	int16_t fn;
	uint32_t fcSepx;
	int16_t fnMpr;
	uint32_t fcMpr;
    } SED;


typedef uint16_t FTC;

/* STSHI: STyleSHeet Information, as stored in a file
  Note that new fields can be added to the STSHI without invalidating
  the file format, because it is stored preceded by it's length.
  When reading a STSHI from an older version, new fields will be zero. */
typedef struct _STSHI {
	uint16_t cstd;		/* Count of styles in stylesheet */
	uint16_t cbSTDBaseInFile;	/* Length of STD Base as stored in a file */
	unsigned fStdStylenamesWritten:1;	/* Are built-in stylenames stored? */
	unsigned reserved:15;	/* Spare flags */
	uint16_t stiMaxWhenSaved;	/* Max sti known when this file was written */
	uint16_t istdMaxFixedWhenSaved;	/* How many fixed-index istds are there? */
	uint16_t nVerBuiltInNamesWhenSaved;	/* Current version of built-in stylenames */
	FTC rgftcStandardChpStsh[3];	/* ftc used by StandardChpStsh for this document */
    } STSHI;


typedef union _UPX {
	struct {
	    uint8_t *grpprl;
	} chpx;
	struct {
	    uint16_t istd;
	    uint8_t *grpprl;
	} papx;
	uint8_t *rgb;
    } UPX;


typedef struct _LSPD {
       int16_t dyaLine;
       int16_t fMultLinespace;
    } LSPD;

typedef union _PHE {
	struct {
	    unsigned fSpare:1;
	    unsigned fUnk:1;
	    unsigned fDiffLines:1;
	    unsigned reserved1:5;
	    unsigned clMac:8;
	    unsigned reserved2:16;
	    int32_t dxaCol;
	    int32_t dymHeight;	/*also known as dymLine and dymTableHeight in docs */
	} var1;
	struct {
	    unsigned fSpare:1;
	    unsigned fUnk:1;
	    //unsigned dcpTtpNext:30;
	    uint32_t dcpTtpNext;
	    int32_t dxaCol;
	    int32_t dymHeight;	/*also known as dymLine and dymTableHeight in docs */
	} var2;
    } PHE;


typedef struct _TLP {
	int itl:16;
	unsigned fBorders:1;
	unsigned fShading:1;
	unsigned fFont:1;
	unsigned fColor:1;
	unsigned fBestFit:1;
	unsigned fHdrRows:1;
	unsigned fLastRow:1;
	unsigned fHdrCols:1;
	unsigned fLastCol:1;
	unsigned unused:7;
    } TLP;


#define itcMax 64

typedef struct _TC {
	unsigned fFirstMerged:1;
	unsigned fMerged:1;
	unsigned fVertical:1;
	unsigned fBackward:1;
	unsigned fRotateFont:1;
	unsigned fVertMerge:1;
	unsigned fVertRestart:1;
	unsigned vertAlign:2;
	unsigned fUnused:7;
	unsigned wUnused:16;
	BRC brcTop;
	BRC brcLeft;
	BRC brcBottom;
	BRC brcRight;
    } TC;


typedef struct _SHD {
	/*16 bits in total */
	unsigned icoFore:5;
	unsigned icoBack:5;
	unsigned ipat:6;
    } SHD;


typedef struct _TAP {
	int16_t jc;
	int32_t dxaGapHalf;
	int32_t dyaRowHeight;
	uint8_t fCantSplit;
	uint8_t fTableHeader;
	TLP tlp;
	int32_t lwHTMLProps;
	unsigned fCaFull:1;
	unsigned fFirstRow:1;
	unsigned fLastRow:1;
	unsigned fOutline:1;
	unsigned reserved:12;
	int itcMac:16;
	int32_t dxaAdjust;
	int32_t dxaScale;
	int32_t dxsInch;
	int16_t rgdxaCenter[itcMax + 1];
	int16_t rgdxaCenterPrint[itcMax + 1];
	TC rgtc[itcMax];
	SHD rgshd[itcMax];
	BRC rgbrcTable[6];
    } TAP;


typedef struct _DCS {
	/* 16 bits for bitfields */
	unsigned fdct:3;
	unsigned count:5;
	unsigned reserved:8;
    } DCS;


typedef struct _ANLD {
	uint8_t nfc;
	uint8_t cxchTextBefore;

	unsigned cxchTextAfter:8;
	unsigned jc:2;
	unsigned fPrev:1;
	unsigned fHang:1;
	unsigned fSetBold:1;
	unsigned fSetItalic:1;
	unsigned fSetSmallCaps:1;
	unsigned fSetCaps:1;
	unsigned fSetStrike:1;
	unsigned fSetKul:1;
	unsigned fPrevSpace:1;
	unsigned fBold:1;
	unsigned fItalic:1;
	unsigned fSmallCaps:1;
	unsigned fCaps:1;
	unsigned fStrike:1;
	unsigned kul:3;
	unsigned ico:5;

	int16_t ftc;
	uint16_t hps;
	uint16_t iStartAt;
	int16_t dxaIndent;
	uint16_t dxaSpace;
	uint8_t fNumber1;
	uint8_t fNumberAcross;
	uint8_t fRestartHdn;
	uint8_t fSpareX;
	XCHAR rgxch[32];
    } ANLD;


typedef struct _NUMRM {
	uint8_t fNumRM;
	uint8_t Spare1;
	int16_t ibstNumRM;
	DTTM dttmNumRM;
	uint8_t rgbxchNums[9];
	uint8_t rgnfc[9];
	int16_t Spare2;
	int32_t PNBR[9];
	XCHAR xst[32];
    } NUMRM;


#define itbdMax 64
typedef struct _TBD	/* 8 bits */
    {
    unsigned	jc:3;
    unsigned	tlc:3;
    unsigned	reserved:2;
    } TBD;


typedef struct _PAP {
	uint16_t istd;
	uint8_t jc;
	uint8_t fKeep;
	uint8_t fKeepFollow;

	unsigned fPageBreakBefore:8;
	unsigned fBrLnAbove:1;
	unsigned fBrLnBelow:1;
	unsigned fUnused:2;
	unsigned pcVert:2;
	unsigned pcHorz:2;
	unsigned brcp:8;
	unsigned brcl:8;

	uint8_t reserved1;
	uint8_t ilvl;
	uint8_t fNoLnn;
	int16_t ilfo;
	uint8_t nLvlAnm;
	uint8_t reserved2;
	uint8_t fSideBySide;
	uint8_t reserved3;
	uint8_t fNoAutoHyph;
	uint8_t fWidowControl;
	int32_t dxaRight;
	int32_t dxaLeft;
	int32_t dxaLeft1;
	LSPD lspd;
	uint32_t dyaBefore;
	uint32_t dyaAfter;
	PHE phe;
	uint8_t fCrLf;
	uint8_t fUsePgsuSettings;
	uint8_t fAdjustRight;
	uint8_t reserved4;
	uint8_t fKinsoku;
	uint8_t fWordWrap;
	uint8_t fOverflowPunct;
	uint8_t fTopLinePunct;
	uint8_t fAutoSpaceDE;
	uint8_t fAtuoSpaceDN;
	uint16_t wAlignFont;

	unsigned fVertical:1;
	unsigned fBackward:1;
	unsigned fRotateFont:1;
	unsigned reserved5:13;
	unsigned reserved6:16;

	int8_t fInTable;
	int8_t fTtp;
	uint8_t wr;
	uint8_t fLocked;
	TAP ptap;
	int32_t dxaAbs;
	int32_t dyaAbs;
	int32_t dxaWidth;
	BRC brcTop;
	BRC brcLeft;
	BRC brcBottom;
	BRC brcRight;
	BRC brcBetween;
	BRC brcBar;
	int32_t dxaFromText;
	int32_t dyaFromText;
	/*16 bits for the next two entries */
	int dyaHeight:15;
	int fMinHeight:1;
	SHD shd;
	DCS dcs;
	int8_t lvl;
	int8_t fNumRMIns;
	ANLD anld;
	int16_t fPropRMark;
	int16_t ibstPropRMark;
	DTTM dttmPropRMark;
	NUMRM numrm;
	int16_t itbdMac;
	int16_t rgdxaTab[itbdMax];
	TBD rgtbd[itbdMax];
/* >>------------------PATCH */
	char stylename[100];
/* -----------------------<< */
      /* BiDi */
      unsigned fBidi:1;
    } PAP;

#define istdNil 4095

typedef uint16_t LID;

#define istdNormalChar 10

typedef struct _CHP {
    unsigned fBold:1;
    unsigned fItalic:1;
    unsigned fRMarkDel:1;
    unsigned fOutline:1;
    unsigned fFldVanish:1;
    unsigned fSmallCaps:1;
    unsigned fCaps:1;
    unsigned fVanish:1;
    unsigned fRMark:1;
    unsigned fSpec:1;
    unsigned fStrike:1;
    unsigned fObj:1;
    unsigned fShadow:1;
    unsigned fLowerCase:1;
    unsigned fData:1;
    unsigned fOle2:1;
    unsigned fEmboss:1;
    unsigned fImprint:1;
    unsigned fDStrike:1;
    int		fUsePgsuSettings:1;/*? */
    unsigned reserved1:12;
    uint32_t reserved2;
    uint16_t reserved11;
    uint16_t ftc;
    uint16_t ftcAscii;
    uint16_t ftcFE;
    uint16_t ftcOther;
    uint16_t hps;
    int32_t dxaSpace;
    unsigned iss:3;
    unsigned kul:4;
    unsigned fSpecSymbol:1;
    unsigned ico:5;
    unsigned reserved3:1;
    unsigned fSysVanish:1;
    unsigned hpsPos:1;
    int		super_sub:16;
    LID lid;
    LID lidDefault;
    LID lidFE;
    uint8_t idct;
    uint8_t idctHint;
    uint8_t wCharScale;
    int32_t fcPic_fcObj_lTagObj;
    int16_t ibstRMark;
    int16_t ibstRMarkDel;
    DTTM dttmRMark;
    DTTM dttmRMarkDel;
    int16_t reserved4;
    uint16_t istd;
    int16_t ftcSym;
    XCHAR xchSym;
    int16_t idslRMReason;
    int16_t idslReasonDel;
    uint8_t ysr;
    uint8_t chYsr;
    uint16_t cpg;
    uint16_t hpsKern;
    unsigned icoHighlight:5;
    unsigned fHighlight:1;
    unsigned kcd:3;
    unsigned fNavHighlight:1;
    unsigned fChsDiff:1;
    unsigned fMacChs:1;
    unsigned fFtcAsciSym:1;
    unsigned reserved5:3;
    unsigned fPropRMark:16;	/*was typo of fPropMark in documentation */
    int16_t ibstPropRMark;
    DTTM dttmPropRMark;
    uint8_t sfxtText;
    uint8_t reserved6;
    uint8_t reserved7;
    uint16_t reserved8;
    uint16_t reserved9;
    DTTM reserved10;
    uint8_t fDispFldRMark;
    int16_t ibstDispFldRMark;
    DTTM dttmDispFldRMark;
    XCHAR xstDispFldRMark[16];
    SHD shd;
    BRC brc;

      /* BiDi properties */
      unsigned  fBidi:1;
      unsigned  fBoldBidi:1;
      unsigned  fItalicBidi:1;
      uint16_t ftcBidi;
      uint16_t hpsBidi;
      uint8_t  icoBidi;
      LID lidBidi;

    } CHP;

typedef struct _CHPX
    {
    uint16_t	istd;
    uint8_t	cbGrpprl;
    uint8_t	*grpprl;
    } CHPX;


typedef union _UPD {
	PAP apap;
	CHP achp;
	CHPX chpx;
    } UPD;

/* The UPE structure is the non-zero prefix of a UPD structure

For my purposes we'll call them the same, and when we get around
to writing word files, when we'll make a distinction. */
    typedef UPD UPE;


typedef struct _UPXF {
	uint16_t cbUPX;
	UPX upx;
    } UPXF;


/* STD: STyle Definition
   The STD contains the entire definition of a style.
   It has two parts, a fixed-length base (cbSTDBase bytes long)
   and a variable length remainder holding the name, and the upx and upe
   arrays (a upx and upe for each type stored in the style, std.cupx)
   Note that new fields can be added to the BASE of the STD without
   invalidating the file format, because the STSHI contains the length
   that is stored in the file.  When reading STDs from an older version,
   new fields will be zero.
*/
    typedef struct _wvSTD {
	/* Base part of STD: */
	unsigned sti:12;		/* invariant style identifier */
	unsigned fScratch:1;		/* spare field for any temporary use,
				   always reset back to zero! */
	unsigned fInvalHeight:1;	/* PHEs of all text with this style are wrong */
	unsigned fHasUpe:1;		/* UPEs have been generated */
	unsigned fMassCopy:1;	/* std has been mass-copied; if unused at
				   save time, style should be deleted */
	unsigned sgc:4;		/* style type code */
	unsigned istdBase:12;	/* base style */

	unsigned cupx:4;		/* # of UPXs (and UPEs) */
	unsigned istdNext:12;	/* next style */
	uint16_t bchUpe;		/* offset to end of upx's, start of upe's */

	/* 16 bits in the following bitfields */
	unsigned fAutoRedef:1;	/* auto redefine style when appropriate */
	unsigned fHidden:1;		/* hidden from UI? */
	unsigned reserved:14;	/* unused bits */

		/* Variable length part of STD: */
		/*	XCHAR *xstzName;*/	/* sub-names are separated by chDelimStyle */
	char *xstzName;

	UPXF *grupxf;		/*was UPX *grupx in the spec, but for my
							  purposes its different */

	/* the UPEs are not stored on the file; they are a cache of the based-on
	   chain */
	UPE *grupe;
    } STD;

typedef enum {
	sgcPara = 1,
	sgcChp,
	sgcPic,
	sgcSep,
	sgcTap
	} sgcval;


/*The style sheet (STSH) is stored in the file in two parts, a STSHI and then
an array of STDs. The STSHI contains general information about the following
stylesheet, including how many styles are in it. After the STSHI, each style
is written as an STD. Both the STSHI and each STD are preceded by a U16
that indicates their length.

 Field     Size        Comment
 cbStshi   2 bytes     size of the following STSHI structure
 STSHI     (cbStshi)   Stylesheet Information
 Then for each style in the stylesheet (stshi.cstd), the following is
 stored:
 cbStd     2 bytes     size of the following STD structure
 STD       (cbStd)     the style description*/
typedef struct _STSH {
	STSHI Stshi;
	STD *std;
    } STSH;

typedef struct _SEPX {
	uint16_t cb;
	uint8_t *grpprl;
    } SEPX;

    typedef struct _BRC10 {	/* 16 bits in total */
	unsigned dxpLine2Width:3;
	unsigned dxpSpaceBetween:3;
	unsigned dxpLine1Width:3;
	unsigned dxpSpace:5;
	unsigned fShadow:1;
	unsigned fSpare:1;
    } BRC10;

#define WV_PAGESIZE 512

 typedef struct _BX {
	uint8_t offset;
	PHE phe;
    } BX;

 typedef struct _PAPX {
	uint16_t cb;
	uint16_t istd;
	uint8_t *grpprl;
    } PAPX;

 typedef struct _PAPX_FKP {
	uint32_t *rgfc;
	BX *rgbx;
	PAPX *grppapx;
	uint8_t crun;
    } PAPX_FKP;

 typedef struct _BTE {
	uint32_t pn; //:22;
	unsigned unused:10;
    } BTE;

 typedef struct _CHPX_FKP {
	uint32_t *rgfc;
	uint8_t *rgb;
	CHPX *grpchpx;
	uint8_t crun;
    } CHPX_FKP;

typedef enum {
	DOCBEGIN,
	DOCEND,
	SECTIONBEGIN,
	SECTIONEND,
	PARABEGIN,
	PARAEND,
	CHARPROPBEGIN,
	CHARPROPEND,
	COMMENTBEGIN,
	COMMENTEND
    } wvTag;


  typedef struct _DOGRID {
	uint16_t xaGrid;
	uint16_t yaGrid;
	uint16_t dxaGrid;
	uint16_t dyaGrid; //:16;
	unsigned dyGridDisplay:7;
	unsigned fTurnItOff:1;
	unsigned dxGridDisplay:7;
	unsigned fFollowMargins:1;
    } DOGRID;


    typedef struct _COPTS {
	/* 16 bits for bitfields */
	unsigned fNoTabForInd:1;
	unsigned fNoSpaceRaiseLower:1;
	unsigned fSuppressSpbfAfterPageBreak:1;
	unsigned fWrapTrailSpaces:1;
	unsigned fMapPrintTextColor:1;
	unsigned fNoColumnBalance:1;
	unsigned fConvMailMergeEsc:1;
	unsigned fSuppressTopSpacing:1;
	unsigned fOrigWordTableRules:1;
	unsigned fTransparentMetafiles:1;
	unsigned fShowBreaksInFrames:1;
	unsigned fSwapBordersFacingPgs:1;
	unsigned reserved:4;
    } COPTS;


    typedef struct _DOPTYPOGRAPHY {
	unsigned fKerningPunct:1;
	unsigned iJustification:2;
	unsigned iLevelOfKinsoku:2;
	unsigned f2on1:1;
	unsigned reserved:10;
	unsigned cchFollowingPunct:16;

	uint16_t cchLeadingPunct;
	uint16_t rgxchFPunct[101];
	uint16_t rgxchLPunct[51];
    } DOPTYPOGRAPHY;


    typedef struct _ASUMYI {
	unsigned fValid:1;
	unsigned fView:1;
	unsigned iViewBy:2;
	unsigned fUpdateProps:1;
	unsigned reserved:11;
	unsigned wDlgLevel:16;

	uint32_t lHighestLevel;
	uint32_t lCurrentLevel;
    } ASUMYI;


    typedef struct _DOP {
	unsigned fFacingPages:1;
	unsigned fWidowControl:1;
	unsigned fPMHMainDoc:1;
	unsigned grfSuppression:2;
	unsigned fpc:2;		/*where footnotes are put */
	unsigned reserved1:1;
	unsigned grpfIhdt:8;
	unsigned rncFtn:2;		/*how to restart footnotes */
	unsigned fFtnRestart:1;	/* Word 2 */

	unsigned nFtn:15;		/*first footnote no. WORD 2: int :15 */

	uint8_t irmBar;		/* W2 */
	unsigned irmProps:7;		/* W2 */

	unsigned fOutlineDirtySave:1;
	unsigned reserved2:7;
	unsigned fOnlyMacPics:1;
	unsigned fOnlyWinPics:1;
	unsigned fLabelDoc:1;
	unsigned fHyphCapitals:1;
	unsigned fAutoHyphen:1;
	unsigned fFormNoFields:1;
	unsigned fLinkStyles:1;

	unsigned fRevMarking:1;
	unsigned fBackup:1;
	unsigned fExactCWords:1;
	unsigned fPagHidden:1;
	unsigned fPagResults:1;
	unsigned fLockAtn:1;
	unsigned fMirrorMargins:1;

	unsigned fKeepFileFormat:1;	/* W2 */

	unsigned reserved3:1;

	unsigned fDfltTrueType:1;
	unsigned fPagSuppressTopSpacing:1;

	unsigned fRTLAlignment:1;	/* W2 */
	unsigned reserved3a:6;	/* " */
	unsigned reserved3b:7;	/* " */

	unsigned fSpares:16;		/* W2 */

	unsigned fProtEnabled:1;
	unsigned fDispFormFldSel:1;
	unsigned fRMView:1;
	unsigned fRMPrint:1;
	unsigned reserved4:1;
	unsigned fLockRev:1;
	unsigned fEmbedFonts:1;

	COPTS copts;

	uint16_t dxaTab;

	uint32_t ftcDefaultBi;	/* W2 */

	uint16_t wSpare;
	uint16_t dxaHotZ;
	uint16_t cConsecHypLim;
	uint16_t wSpare2;

	uint32_t wSpare3;		/* W2 */

	DTTM dttmCreated;
	DTTM dttmRevised;
	DTTM dttmLastPrint;

	uint16_t nRevision;
	uint32_t tmEdited;
	uint32_t cWords;
	uint32_t cCh;
	uint16_t cPg;
	uint32_t cParas;

	uint16_t rgwSpareDocSum[3];	/* W2 */

	unsigned rncEdn:2;		/*how endnotes are restarted */
	unsigned nEdn:14;		/*beginning endnote no */
	unsigned epc:2;		/*where endnotes go */
	unsigned nfcFtnRef:4;	/*number format code for auto footnotes, i think use the new_* instead */
	unsigned nfcEdnRef:4;	/*number format code for auto endnotes, i thing use the new_* instead */
	unsigned fPrintFormData:1;
	unsigned fSaveFormData:1;
	unsigned fShadeFormData:1;
	unsigned reserved6:2;
	unsigned fWCFtnEdn:1;

	uint32_t cLines;
	uint32_t cWordsFtnEnd;
	uint32_t cChFtnEdn;
	uint16_t cPgFtnEdn;
	uint32_t cParasFtnEdn;
	uint32_t cLinesFtnEdn;
	uint32_t lKeyProtDoc;	/*password protection key (! ?) */

	unsigned wvkSaved:3;
	unsigned wScaleSaved:9;
	unsigned zkSaved:2;
	unsigned fRotateFontW6:1;
	unsigned iGutterPos:1;
	unsigned fNoTabForInd:1;
	unsigned fNoSpaceRaiseLower:1;
	unsigned fSuppressSpbfAfterPageBreak:1;
	unsigned fWrapTrailSpaces:1;
	unsigned fMapPrintTextColor:1;
	unsigned fNoColumnBalance:1;
	unsigned fConvMailMergeEsc:1;
	unsigned fSuppressTopSpacing:1;
	unsigned fOrigWordTableRules:1;
	unsigned fTransparentMetafiles:1;
	unsigned fShowBreaksInFrames:1;
	unsigned fSwapBordersFacingPgs:1;
	unsigned reserved7:4;

	unsigned fSuppressTopSpacingMac5:1;
	unsigned fTruncDxaExpand:1;
	unsigned fPrintBodyBeforeHdr:1;
	unsigned fNoLeading:1;
	unsigned reserved8:1;
	unsigned fMWSmallCaps:1;
	unsigned reserved9:10;
	unsigned adt:16;

	DOPTYPOGRAPHY doptypography;
	DOGRID dogrid;

	unsigned reserver11:1;
	unsigned lvl:4;
	unsigned fGramAllDone:1;
	unsigned fGramAllClean:1;
	unsigned fSubsetFonts:1;
	unsigned fHideLastVersion:1;
	unsigned fHtmlDoc:1;
	unsigned reserved10:1;
	unsigned fSnapBorder:1;
	unsigned fIncludeHeader:1;
	unsigned fIncludeFooter:1;
	unsigned fForcePageSizePag:1;
	unsigned fMinFontSizePag:1;
	unsigned fHaveVersions:1;
	unsigned fAutoVersion:1;
	unsigned reserved11:14;

	ASUMYI asumyi;

	uint32_t cChWS;
	uint32_t cChWSFtnEdn;
	uint32_t grfDocEvents;
	unsigned fVirusPrompted:1;
	unsigned fVirusLoadSafe:1;
	uint32_t KeyVirusSession30; //:30;

	uint8_t Spare[30];
	uint32_t reserved12;
	uint32_t reserved13;

	uint32_t cDBC;
	uint32_t cDBCFtnEdn;
	uint32_t reserved14;
	uint16_t new_nfcFtnRef;	/*number format code for auto footnote references */
	uint16_t new_nfcEdnRef;	/*number format code for auto endnote references */
	uint16_t hpsZoonFontPag;
	uint16_t dywDispPag;
    } DOP;


#ifdef __cplusplus
extern "C" {
#endif

uint32_t wvNormFC(uint32_t fc, int *flag);

Sprm wvApplySprmFromBucket (uint8_t ver, uint16_t sprm, PAP * apap, CHP * achp,
		SEP * asep, STSH * stsh, uint8_t * pointer, uint16_t * pos,
		FILE * data);
int wvAssembleSimplePAP (int ver, PAP * apap, uint32_t fc, PAPX_FKP * fkp,
		STSH * stsh, FILE * data);
void wvInitPAP (PAP * item);
void wvInitPAPX_FKP (PAPX_FKP * fkp);
#define wvReleasePAPX_FKP(X) {}

int wvGetIndexFCInFKP_PAPX (PAPX_FKP * fkp, uint32_t currentfc);

void wvCopyPAP (PAP * dest, PAP * src);
void wvCopyTAP (TAP * dest, TAP * src);

static uint8_t wvEatSprm (uint16_t sprm, uint8_t * pointer, uint16_t * pos);
SprmName wvGetrgsprmWord6 (uint8_t in);

int wvGetPieceBoundsFC(uint32_t * begin, uint32_t * end, CLX * clx, uint32_t piececount);
uint32_t wvGetPieceFromCP (uint32_t currentcp, CLX * clx);
uint32_t wvConvertCPToFC (uint32_t currentcp, CLX * clx);
void wvInitSEP (SEP * item);
void wvGetSEPX(SEPX * item, FILE * fd);
int wvAddSEPXFromBucket (SEP * asep, SEPX * item, STSH * stsh);
int wvAddSEPXFromBucket6 (SEP * asep, SEPX * item, STSH * stsh);
void wvReleaseSEPX (SEPX * item);
int wvGetBTE_FromFC (BTE * bte, uint32_t currentfc, BTE * list, uint32_t * fcs, int nobte);
void wvGetPAPX_FKP (int ver, PAPX_FKP * fkp, uint32_t pn, FILE * fd);
void wvGetCHPX_FKP(CHPX_FKP * fkp, uint32_t pn, FILE * fd);
#define wvReleaseCHPX_FKP(XX) {}
int wvGetPieceBoundsCP (uint32_t * begin, uint32_t * end, CLX * clx, uint32_t piececount);
int wvAssembleSimpleCHP(int ver, CHP * achp, uint32_t fc, CHPX_FKP * fkp, STSH * stsh);
void wvInitCLX(CLX *item);
void wvInitSTSH(STSH * item);
void wvInitCHPX_FKP (CHPX_FKP * fkp);

void wvGetFIB2(FIB *item, FILE* fd);
void wvGetFIB6(FIB *item, FILE* fd);
void wvGetFIB8(FIB *item, FILE* fd);
void wvGetCLX(uint8_t ver, CLX *clx, uint32_t offset, uint32_t len, uint8_t fExtChar, FILE *fd);
void wvBuildCLXForSimple6(CLX *clx, FIB * fib);
void wvGetSTSH(STSH *item, uint32_t offset, uint32_t len, FILE *fd);
int wvGetBTE_PLCF6(BTE ** bte, uint32_t ** pos, uint32_t * nobte, uint32_t offset, uint32_t len, FILE * fd);
int wvGetBTE_PLCF(BTE ** bte, uint32_t ** pos, uint32_t * nobte, uint32_t offset, uint32_t len, FILE * fd);
int wvGetSED_PLCF (SED ** item, uint32_t ** pos, uint32_t * noitem, uint32_t offset, uint32_t len, FILE * fd);
void wvReleaseCLX(CLX *clx);
void wvReleaseSTSH (STSH * item);
void internal_wvReleasePAPX_FKP (PAPX_FKP * fkp);
void internal_wvReleaseCHPX_FKP (CHPX_FKP * fkp);
uint32_t wvSearchNextLargestFCPAPX_FKP (PAPX_FKP * fkp, uint32_t currentfc);
int wvQuerySamePiece (uint32_t fcTest, CLX * clx, uint32_t piece);
uint32_t wvGetEndFCPiece (uint32_t piece, CLX * clx);
uint32_t wvSearchNextSmallestFCPAPX_FKP (PAPX_FKP * fkp, uint32_t currentfc);

uint8_t bread_8ubit (uint8_t * in, uint16_t * pos);
uint16_t bread_16ubit (uint8_t * in, uint16_t * pos);

SprmName wvGetrgsprmWord6 (uint8_t in);
SprmName wvGetrgsprmPrm (uint16_t in);

extern TAP * cache_TAP;

#ifdef __cplusplus
}
#endif

#endif