#include <stdio.h>
#include <stdlib.h>

#include "raster.h"


/** This error is emitted when some internal failure of library occurs. */
void AbstractError(const char *Name)
{
 fprintf(stderr, "Abstract function '%s' called!\n", Name==NULL?"":Name);
 abort();
}
