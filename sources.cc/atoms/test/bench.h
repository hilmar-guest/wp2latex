#ifndef __BENCH_H__
#define __BENCH_H__

#ifdef __cplusplus
extern "C"
{
#endif

extern long MeasureTime;	///< Measuring time for AutoMeasure in ms
extern float CallibrationTime;	///< Time used for callibration.


float Measure(void(procedure)(long i),long i);
float AutoMeasure(void(procedure)(long i));

double GetInterval(void);

 
#ifdef __cplusplus
}
#endif

#endif  /* __BENCH_H__ */
