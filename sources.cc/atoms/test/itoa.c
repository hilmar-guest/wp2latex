
void reverse(char *s)
 {
   int i, j;
   char c;

   for (i = 0, j = strlen(s)-1; i<j; i++, j--) {
       c = s[i];
       s[i] = s[j];
       s[j] = c;
   }
}


char *itoa(int value, char *buffer, int radix)
{
  int i = 0 , n,s;

  if(value==0)
  {
    buffer[i++] = '0';
  }
  else
  {
    if(value<0)
    {
      n = -value;
      buffer[i++] = '-';
    }
    else
      n = value;

    while(n > 0)
    {
     s = n % radix;
      n = n / radix;
      buffer[i++] = '0' + s;
    }
  }
  buffer[i] = '\0';

  if(buffer[0]=='-')
    reverse(buffer+1);
  else
    reverse(buffer);
return buffer;
}