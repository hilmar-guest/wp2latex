;***************************************************************************
; unit:    raster_BCC.asm      release 0.33                                *
; purpose: general manipulation n dimensional matrices n = 1, 2 and 3.     *
;          Use this file or rasterc.c. You cannot link both files together *
; licency:     GPL or LGPL                                                 *
; Copyright: (c) 1998-2024 Jaroslav Fojtik                                 *
;***************************************************************************

.486              ;Target processor.  Use instructions for Pentium class machines
.MODEL FLAT, C    ;Use the flat memory model. Use C calling conventions

.CODE             ;Indicates the start of a code segment.

USE8087	EQU	1

LOCALS @@


;void Conv1_4(BYTE *Dest, const BYTE *Src, unsigned Size1D)
        public  Conv1_4
Conv1_4 proc \
        uses edi esi, \
        Dest:ptr byte, \
        Src:ptr byte, \
        count:DWORD

        mov     ecx,[count]     ; cx=amount of pixels
	or	ecx,ecx
        jz	@@ToEnd     ; it's as if strings are equal

        mov     esi,[Src]      ; 
        mov     edi,[Dest]     ; di=destination pointer (es=segment part)

@@octet:  mov     al,[esi]	; new octet

	cbw			; Extend 8th bit to AH	

	dec	ecx
	mov	dl,ah
        jz      @@ToEndNbl
        and	dl,0F0h		; mask low nibble
        
        rol     al,1		; 40h
        cbw			; Extend 7th bit to AH
        and	ah,0Fh		; mask high nibble		
        or	ah,dl
	mov	[edi],ah	; store converted byte

	dec	ecx
        jz      @@ToEnd
	inc	edi      
        
        rol     al,1		; 20h
        cbw			; Extend 6th bit to AH	

	dec	ecx
	mov	dl,ah
        jz      @@ToEndNbl
        and	dl,0F0h		; mask low nibble
        
        rol     al,1		; 10h
        cbw			; Extend 5th bit to AH
        and	ah,0Fh
        or	ah,dl	
	mov	[edi],ah	; store converted byte

	dec	ecx
        jz      @@ToEnd
	inc	edi   

        rol     al,1		; 08h
        cbw			; Extend 4th bit to AH	

	dec	ecx
	mov	dl,ah
        jz      @@ToEndNbl
        and	dl,0F0h		; mask low nibble
        
        rol     al,1		; 04h
	cbw			; Extend 3rd bit to AH
        and	ah,0Fh
        or	ah,dl	
	mov	[edi],ah	; store converted byte

	dec	ecx
        jz      @@ToEnd
	inc	edi        
        
        rol     al,1		; 02h
        cbw			; Extend 2nd bit to AH

	dec	ecx
	mov	dl,ah
        jz      @@ToEndNbl
        and	dl,0F0h		; mask low nibblde
        
        rol     al,1		; 01h
        cbw			; Extend 1st bit to AH
	and	ah,0Fh
        or	ah,dl	        
	mov	[edi],ah	; store converted byte
	
	inc	esi
	inc	edi

	dec	ecx
	jne	@@octet

@@ToEnd:
        ret                     ; _cdecl return
        
@@ToEndNbl:
	and	ah,0F0h
	mov	[edi],ah	; store converted low nibble
	ret        
        
Conv1_4 endp


;*************************************************************************************


;void Conv1_8(BYTE *Dest, const BYTE *Src, unsigned Size1D)
        public  Conv1_8
Conv1_8 proc \
        uses edi esi, \
        Dest:ptr byte, \
        Src:ptr byte, \
        count:DWORD

        mov     ecx,[count]     ; cx=amount of pixels
        jecxz	@@ToEnd		; it's as if strings are equal

        mov     esi,[Src]      ; 
        mov     edi,[Dest]     ; di=destination pointer (es=segment part)

@@octet:  mov     al,[esi]	; new octet

	cbw			; Extend 8th bit to AH
	mov	[edi],ah	; store converted byte

	dec	ecx
        jz      @@ToEnd
	inc	edi        
        
        rol     al,1		; 40h
        cbw			; Extend 7th bit to AH
	mov	[edi],ah	; store converted byte

	dec	ecx
        jz      @@ToEnd
	inc	edi        
        
        rol     al,1		; 20h
        cbw			; Extend 6th bit to AH
	mov	[edi],ah	; store converted byte

	dec	ecx
        jz      @@ToEnd
	inc	edi        
        
        rol     al,1		; 10h
        cbw			; Extend 5th bit to AH
	mov	[edi],ah	; store converted byte

	dec	ecx
        jz      @@ToEnd
	inc	edi        

        rol     al,1		; 08h
        cbw			; Extend 4th bit to AH
	mov	[edi],ah	; store converted byte

	dec	ecx
        jz      @@ToEnd
	inc	edi        
        
        rol     al,1		; 04h
	cbw			; Extend 3rd bit to AH
	mov	[edi],ah	; store converted byte

	dec	ecx
        jz      @@ToEnd
	inc	edi        
        
        rol     al,1		; 02h
        cbw			; Extend 2nd bit to AH
	mov	[edi],ah	; store converted byte

	dec	ecx
        jz      @@ToEnd
	inc	edi        
        
        rol     al,1		; 01h
        cbw			; Extend 1st bit to AH
	mov	[edi],ah	; store converted byte
	
	inc	esi
	inc	edi

	dec	ecx
	jne	@@octet

@@ToEnd:
        ret                     ; _cdecl return
        
Conv1_8 endp


;*************************************************************************************

        public  Conv1_16
Conv1_16 proc \
        uses edi esi, \
        Dest:ptr word, \
        Src:ptr byte, \
        count:DWORD

        mov     ecx,[count]     ; cx=amount of pixels
	or	ecx,ecx
        jz	@@ToEnd		; array has zero size

        mov     edi,[Dest]     ; di=first pointer (es=segment part)
        mov     esi,[Src]      ; 

@@Octet:  mov     ah,[esi]	; new octet
        
        cwd			; extend 8th bit to DX
        mov	[edi],dx
        add	edi,2

	dec	ecx
        jz      @@ToEnd
        
        rol	ax,1        
        cwd			; extend 7th bit to DX
        mov	[edi],dx
        add	edi,2

	dec	ecx
        jz      @@ToEnd
        
        rol	ax,1        
        cwd			; extend 6th bit to DX
        mov	[edi],dx
        add	edi,2

	dec	ecx
        jz      @@ToEnd        
        
        rol	ax,1        
        cwd			; extend 5th bit to DX
        mov	[edi],dx
        add	edi,2

	dec	ecx
        jz      @@ToEnd                


        rol	ax,1        
        cwd			; extend 4th bit to DX
        mov	[edi],dx
        add	edi,2

	dec	ecx
        jz      @@ToEnd
        
        
        rol	ax,1        
        cwd			; extend 3rd bit to DX
        mov	[edi],dx
        add	edi,2

	dec	ecx
        jz      @@ToEnd
        
        rol	ax,1        
        cwd			; extend 2nd bit to DX
        mov	[edi],dx
        add	edi,2

	dec	ecx
        jz      @@ToEnd        
        
        rol	ax,1        
        cwd			; extend 1st bit to DX
        mov	[edi],dx

        inc	esi
        add	edi,2
                
	dec	ecx
	jnz	@@Octet

@@ToEnd:
        ret                     ; _cdecl return

Conv1_16 endp



;void Conv1_24(BYTE *Dest, const BYTE *Src, unsigned Size1D)
        public  Conv1_24
Conv1_24 proc \
        uses edi esi, \
        Dest:ptr byte, \
        Src:ptr byte, \
        count:DWORD

        mov     ecx,[count]     ; cx=amount of pixels
        or	ecx,ecx
        jz	@@ToEnd     ; it's as if strings are equal

        mov     edi,[Dest]     ; di=destination pointer (es=segment part)
        mov     esi,[Src]      ; 

@@octet:  mov     ah,[esi]	; new octet

	cwd			; Extend 8th bit to DX
	mov	[edi],dx	; store converted byte
	add	edi,2
	mov	[edi],dl	; store converted byte
	inc	edi

	dec	ecx
        jz      @@ToEnd

        rol     ax,1		; 40h
        cwd			; Extend 7th bit to DH
	mov	[edi],dl	; store converted byte
	inc	edi
	mov	[edi],dx
	add	edi,2

	dec	ecx
        jz      @@ToEnd
        
        rol     ax,1		; 20h
        cwd			; Extend 6th bit to DX
	mov	[edi],dx	; store converted byte
	add	edi,2
	mov	[edi],dl	; store converted byte
	inc	edi

	dec	ecx
        jz      @@ToEnd
        
        rol     ax,1		; 10h
        cwd			; Extend 5th bit to AH	
	mov	[edi],dl	; store converted byte
	inc	edi
	mov	[edi],dx
	add	edi,2

	dec	ecx
        jz      @@ToEnd

        rol     ax,1		; 08h
        cwd			; Extend 4th bit to AH
	mov	[edi],dx	; store converted byte
	add	edi,2
	mov	[edi],dl	; store converted byte
	inc	edi	

	dec	ecx
        jz      @@ToEnd

        rol     ax,1		; 04h
	cwd			; Extend 3rd bit to AH
	mov	[edi],dl	; store converted byte
	inc	edi
	mov	[edi],dx
	add	edi,2

	dec	ecx
        jz      @@ToEnd
        
        rol     ax,1		; 02h
        cwd			; Extend 2nd bit to AH
	mov	[edi],dx	; store converted byte
	add	edi,2
	mov	[edi],dl	; store converted byte
	inc	edi	

	dec	ecx
        jz      @@ToEnd

        rol     ax,1		; 01h
        cwd			; Extend 1st bit to AH
	mov	[edi],dl	; store converted byte	
	inc	edi	
	mov	[edi],dx	

        inc	esi
        add	edi,2
        
	dec	ecx
	jne	@@octet

@@ToEnd:
        ret                     ; _cdecl return
        
Conv1_24 endp


;*************************************************************************************

        public  Conv1_32
Conv1_32 proc \
        uses edi esi, \
        Dest:ptr dword, \
        Src:ptr byte, \
        count:DWORD
        
        mov     ecx,[count]     ; cx=amount of pixels        
        jecxz	@@ToEnd		; array has zero size

        mov     edi,[Dest]     ; di=first pointer (es=segment part)
        mov     esi,[Src]      ; 

@@Octet:  mov     ah,[esi]	; new octet        

	shl	eax,16
	
	cdq			; extend 8th bit to EDX        
        mov	[edi],edx
        add	edi,4

	dec	ecx
        jz      @@ToEnd
        
        rol	eax,1        
        cdq			; extend 7th bit to EDX
        mov	[edi],edx
        add	edi,4

	dec	ecx
        jz      @@ToEnd
        
        rol	eax,1        
        cdq			; extend 6th bit to EDX
        mov	[edi],edx
        add	edi,4

	dec	ecx
        jz      @@ToEnd        
        
        rol	eax,1        
        cdq			; extend 5th bit to EDX
        mov	[edi],edx
        add	edi,4

	dec	ecx
        jz      @@ToEnd                

        rol	eax,1        
        cdq			; extend 4th bit to DX
        mov	[edi],edx
        add	edi,4

	dec	ecx
        jz      @@ToEnd
        
        
        rol	eax,1        
        cdq			; extend 3rd bit to EDX
        mov	[edi],edx
        add	edi,4

	dec	ecx
        jz      @@ToEnd
        
        rol	eax,1        
        cdq			; extend 2nd bit to EDX
        mov	[edi],edx        
        add	edi,4

	dec	ecx
        jz      @@ToEnd        
        
        rol	eax,1        
        cdq			; extend 1st bit to EDX
        mov	[edi],edx        

	inc	esi
	add	edi,4
	
	dec	ecx
	jnz	@@Octet

@@ToEnd:
        ret                     ; _cdecl return

Conv1_32 endp


;*************************************************************************************


        public  Conv1_64
Conv1_64 proc \
        uses edi esi, \
        Dest:ptr dword, \
        Src:ptr byte, \
        count:DWORD
        
        mov     ecx,[count]     ; cx=amount of pixels
        or	ecx,ecx
        jz	@@ToEnd		; array has zero size

        mov     edi,[Dest]     ; di=first pointer (es=segment part)
        mov     esi,[Src]      ; 

@@Octet:  mov     ah,[esi]	; new octet
        
        shl	eax,16
        
        cdq			; extend 8th bit to DX
        mov	[edi],edx
        add	edi,4
        mov	[edi],edx
        add	edi,4

	dec	ecx
        jz      @@ToEnd
        
        rol	eax,1        
        cdq			; extend 7th bit to DX        
        mov	[edi],edx
        add	edi,4
        mov	[edi],edx
        add	edi,4

	dec	ecx
        jz      @@ToEnd
        
        rol	eax,1        
        cdq			; extend 6th bit to DX        
        mov	[edi],edx
        add	edi,4
        mov	[edi],edx
        add	edi,4

	dec	ecx
        jz      @@ToEnd        
        
        rol	eax,1        
        cdq			; extend 5th bit to DX        
        mov	[edi],edx
        add	edi,4
        mov	[edi],edx
        add	edi,4

	dec	ecx
        jz      @@ToEnd                

        rol	eax,1        
        cdq			; extend 4th bit to DX
        mov	[edi],edx
        add	edi,4
        mov	[edi],edx
        add	edi,4

	dec	ecx
        jz      @@ToEnd
        
        
        rol	eax,1        
        cdq			; extend 3rd bit to DX
        mov	[edi],edx
        add	edi,4
        mov	[edi],edx
        add	edi,4

	dec	ecx
        jz      @@ToEnd
        
        rol	eax,1        
        cdq			; extend 2nd bit to DX
        mov	[edi],edx        
        add	edi,4
        mov	[edi],edx        
        add	edi,4

	dec	ecx
        jz      @@ToEnd        
        
        rol	eax,1        
        cdq			; extend 1st bit to DX
        mov	[edi],edx
        add	edi,4
        mov	[edi],edx

        inc	esi
        add	edi,4        
        
	dec	ecx
	jnz	@@Octet

@@ToEnd:
        ret                     ; _cdecl return

Conv1_64 endp


;*************************************************************************************
;*************************************************************************************


;void Conv4_1(BYTE *Dest, const BYTE *Src, unsigned Size1D)
        public  Conv4_1
Conv4_1 proc \
        uses edi esi, \
        Dest:ptr qword, \
        Src:ptr byte, \
        count:DWORD

        mov     ecx,[count]     ; cx=amount of pixels        
        jecxz	@@ToEnd		; array has zero size

        mov     edi,[Dest]     ; di = destination pointer (es=segment part)
        mov     esi,[Src]      ; di = source pointer
        
        cld        
        mov	ah,1		; add end byte mark
@@PIXEL:	lodsb
	rol	al,1		; copy the highest bit to CY
	rcl	ah,1		; transfer bit from CY to AH
	
	dec	ecx		; 2nd nibble
	jz	@@First1
	rol	al,4		; copy original 4th bit to CY
	rcl	ah,1		; transfer bit from CY to AH
			
	jnc	@@NoOctet			
	mov	[edi],ah	; Full 8 bits finished, 1 travelled to CY.
	inc	edi
	mov	ah,1		; add end byte mark
	loop	@@PIXEL
	jmp	@@ToEnd		; all done here
	
@@NoOctet:loop	@@PIXEL

@@First1:	sal	ah,1		; shift must be finished to 8th bit
	jnc	@@First1
	mov	[edi],ah	; store last incomplete byte

@@ToEnd:
        ret                     ; _cdecl return
                
Conv4_1 endp



;void Conv4_8(BYTE *Dest, const BYTE *Src, unsigned Size1D)
        public  Conv4_8
Conv4_8 proc \
        uses edi esi ebx, \
        Dest:ptr byte, \
        Src:ptr byte, \
        count:DWORD

        mov     ecx,[count]     ; cx=amount of pixels
        jecxz	@@ToEnd		; array has zero size

        mov     edi,[Dest]     ; di = destination pointer (es=segment part)
        mov     esi,[Src]      ; di = source pointer
        
        cld
@@PIXEL:	lodsb
        mov	bl,al
	mov	ah,al
	and	al,0F0h		; copy the highest bit to CY
	shr	ah,4
	or	al,ah		; byte from 1st nibble finished
	stosb

	dec	ecx		; 2nd nibble
	jz	@@ToEnd

	mov	al,bl
	and	al,0Fh
	sal	bl,4
	or	al,bl		; byte from 2nd nibble finished
	stosb
@@NoOctet:loop	@@PIXEL

@@ToEnd:
        ret                     ; _cdecl return
                
Conv4_8 endp



;void Conv4_16(WORD *Dest, const BYTE *Src, unsigned Size1D)
        public  Conv4_16
Conv4_16 proc \
        uses edi esi ebx, \
        Dest:ptr word, \
        Src:ptr byte, \
        count:DWORD

        mov     ecx,[count]     ; cx=amount of pixels
        jecxz	@@ToEnd		; array has zero size

        mov     edi,[Dest]     ; di = destination pointer (es=segment part)
        mov     esi,[Src]      ; di = source pointer
        
        cld
@@PIXEL:	lodsb
        mov	bl,al
	mov	ah,al
	and	al,0F0h		; copy the highest bit to CY
	shr	ah,4
	or	al,ah		; byte from 1st nibble finished
	mov	ah,al
	stosw

	dec	ecx		; 2nd nibble
	jz	@@ToEnd

	mov	al,bl
	and	al,0Fh
	sal	bl,4
	or	al,bl		; byte from 2nd nibble finished
	mov	ah,al
	stosw
@@NoOctet:loop	@@PIXEL

@@ToEnd:
        ret                     ; _cdecl return
                
Conv4_16 endp



;void Conv4_32(DWORD *Dest, const BYTE *Src, unsigned Size1D)
        public  Conv4_32
Conv4_32 proc \
        uses edi esi ebx, \
        Dest:ptr dword, \
        Src:ptr byte, \
        count:DWORD

        mov     ecx,[count]     ; cx=amount of pixels
        jecxz	@@ToEnd		; array has zero size

        mov     edi,[Dest]     ; di = destination pointer (es=segment part)
        mov     esi,[Src]      ; di = source pointer
        
        cld
@@PIXEL:	lodsb
        mov	bl,al
	mov	ah,al
	and	al,0F0h		; copy the highest bit to CY
	shr	ah,4
	or	al,ah		; byte from 1st nibble finished
	mov	ah,al	
	stosw
	stosw

	dec	ecx		; 2nd nibble
	jz	@@ToEnd

	mov	al,bl
	and	al,0Fh
	sal	bl,4
	or	al,bl		; byte from 2nd nibble finished
	mov	ah,al
	mov	bx,ax
	shl	eax,16
	mov	ax,bx		; dword from 2nd nibble finished
	stosd
@@NoOctet:loop	@@PIXEL

@@ToEnd:
        ret                     ; _cdecl return
                
Conv4_32 endp




;*************************************************************************************
;*************************************************************************************

        public  Conv8_1
Conv8_1 proc \
        uses edi esi ebx, \
        Dest:ptr qword, \
        Src:ptr byte, \
        count:DWORD

        mov     ecx,[count]     ; cx=amount of pixels
        jecxz	@@ToEnd		; array has zero size

        mov     edi,[Dest]     ; di=first pointer (es=segment part)
        mov     esi,[Src]      ;
        
        cld        
        mov	ah,1		; add end byte mark
@@PIXEL:	lodsb
	rol	al,1		; copy the highest bit to CY
	rcl	ah,1		; transfer bit from CY
	jnc	@@NoOctet
	mov	[edi],ah
	inc	edi
	mov	ah,1		; add end byte mark
	loop	@@PIXEL
	jmp	@@ToEnd		; all done here
	
@@NoOctet:loop	@@PIXEL

@@First1:	sal	ah,1		; shift must be finished to 8th bit
	jnc	@@First1
	mov	[edi],ah	; store last incomplete byte

@@ToEnd:
        ret                     ; _cdecl return
                
Conv8_1 endp


;*************************************************************************************


        public  Conv8_4
Conv8_4 proc \
        uses edi esi, \
        Dest:ptr byte, \
        Src:ptr byte, \
        count:DWORD

        mov     ecx,[count]     ; cx=amount of pixels
        jecxz	@@ToEnd		; array has zero size

        mov     edi,[Dest]     ; di=first pointer (es=segment part)
        mov     esi,[Src]      ;
        
        cld
@@PIXEL:	lodsb			; load 1st byte
	and	al,0F0h
	
	dec	ecx
	jnz	@@NIBBLE2
	stosb			;store incomplete nibble
	jmp	@@ToEnd

@@NIBBLE2:mov	ah,al
        lodsb			; load 2nd byte
        and	al,0F0h
	ror	al,4
	or	al,ah
	stosb			;store both nibbles
	loop	@@PIXEL
        
@@ToEnd:
        ret                     ; _cdecl return
                
Conv8_4 endp



;*************************************************************************************


        public  Conv8_16
Conv8_16 proc \
        uses edi esi ebx, \
        Dest:ptr qword, \
        Src:ptr byte, \
        count:DWORD

        mov     ecx,[count]     ; cx=amount of pixels
        jecxz	@@ToEnd		; array has zero size

        mov     edi,[Dest]     ; di=first pointer (es=segment part)
        mov     esi,[Src]      ;
        
        cld
@@PIXEL4: cmp	ecx,4	
        jl	@@PIXEL
       
        lodsd			; pixels 1,2,3,4
        mov	ebx,eax
        
        mov	al,bh
        sal	eax,16
        mov	ah,bl
        mov	al,bl
        stosd			; converted pixel 1 & 2
        
        shr	ebx,16
        mov	al,bh
        mov	ah,bh
        sal	eax,16
        mov	ah,bl
        mov	al,bl
        stosd			; converted pixel 3 & 4
        
        sub	ecx,4
        jnz	@@PIXEL4
        jmp	@@ToEnd
                
@@PIXEL:	lodsb
	mov	ah,al
	stosw
	loop	@@PIXEL
        
@@ToEnd:	ret			; _cdecl return
                
Conv8_16 endp


;*************************************************************************************

;void Conv8_24(BYTE *Dest, const BYTE *Src, unsigned Size1D);
        public  Conv8_24
Conv8_24 proc \
        uses edi esi, \
        Dest:ptr byte, \
        Src:ptr byte, \
        count:DWORD

        mov     ecx,[count]     ; cx=amount of pixels
        jecxz	@@ToEnd		; array has zero size

        mov     edi,[Dest]     ; di=first pointer (es=segment part)
        mov     esi,[Src]      ;
        
        cld
@@PIXEL:	lodsb	
	stosb
	stosb
	stosb
	loop	@@PIXEL
        
@@ToEnd:
        ret                     ; _cdecl return
                
Conv8_24 endp


;*************************************************************************************

        public  Conv8_32
Conv8_32 proc \
        uses edi esi, \
        Dest:ptr qword, \
        Src:ptr byte, \
        count:DWORD

        mov     ecx,[count]     ; cx=amount of pixels
        jecxz	@@ToEnd		; array has zero size

        mov     edi,[Dest]     ; di=first pointer (es=segment part)
        mov     esi,[Src]      ;
        
        cld
@@PIXEL:	lodsb
	mov	ah,al
	mov	dx,ax
	rol	eax,16
	mov	ax,dx
	stosd
	loop	@@PIXEL
        
@@ToEnd:
        ret                     ; _cdecl return
                
Conv8_32 endp


;*************************************************************************************

;void Conv8_64(QWORD *Dest, const BYTE *Src, unsigned Size1D);
        public  Conv8_64
Conv8_64 proc \
        uses edi esi ebx, \
        Dest:ptr qword, \
        Src:ptr byte, \
        count:DWORD

        mov     ecx,[count]     ; cx=amount of pixels
        jecxz	@@ToEnd		; array has zero size

        mov     edi,[Dest]     ; di=first pointer (es=segment part)
        mov     esi,[Src]      ;
        
        cld
@@PIXEL:	lodsb
	mov	ah,al
	mov	bx,ax
	rol	eax,16
	mov	ax,bx
	stosd
	stosd
	loop	@@PIXEL
        
@@ToEnd:
        ret                     ; _cdecl return
                
Conv8_64 endp


;*************************************************************************************
;*************************************************************************************

        public  Conv16_1
Conv16_1 proc \
        uses edi esi ebx, \
        Dest:ptr byte, \
        Src:ptr word, \
        count:DWORD

        mov     ecx,[count]     ; cx=amount of pixels
        jecxz	@@ToEnd		; array has zero size

        mov     edi,[Dest]     ; di=first pointer (es=segment part)
        mov     esi,[Src]      ;
        
        cld        
        mov	bl,1		; add end byte mark
@@PIXEL:	lodsw
	rol	ax,1		; copy the highest bit to CY
	rcl	bl,1		; transfer bit from CY
	jnc	@@NoOctet
	mov	[edi],bl
	inc	edi
	mov	bl,1		; add end byte mark
	loop	@@PIXEL
	jmp	@@ToEnd		; all done here
	
@@NoOctet:loop	@@PIXEL

@@First1:	sal	bl,1		; shift must be finished to 8th bit
	jnc	@@First1
	mov	[edi],bl	; store last incomplete byte

@@ToEnd:
        ret                     ; _cdecl return
                
Conv16_1 endp



;*************************************************************************************


        public  Conv16_4
Conv16_4 proc \
        uses edi esi, \
        Dest:ptr byte, \
        Src:ptr word, \
        count:DWORD

        mov     ecx,[count]     ; cx=amount of pixels
        jecxz	@@ToEnd		; array has zero size

        mov     edi,[Dest]     ; di=first pointer (es=segment part)
        mov     esi,[Src]      ;
        
        cld
@@PIXEL:	inc	si
	lodsb			; load 1st hi byte
	and	al,0F0h
	
	dec	ecx
	jnz	@@NIBBLE2
	stosb			;store incomplete nibble
	jmp	@@ToEnd

@@NIBBLE2:mov	ah,al
	inc	si
        lodsb			; load 2nd byte
        and	al,0F0h
	ror	al,4
	or	al,ah
	stosb			;store both nibbles
	loop	@@PIXEL
        
@@ToEnd:
        ret                     ; _cdecl return
                
Conv16_4 endp



;*************************************************************************************


        public  Conv16_8
Conv16_8 proc \
        uses edi esi ebx, \
        Dest:ptr byte, \
        Src:ptr word, \
        count:DWORD

        mov     ecx,[count]     ; cx=amount of pixels
        jecxz	@@ToEnd		; array has zero size

        mov     edi,[Dest]     ; di=first pointer (es=segment part)
        mov     esi,[Src]      ;
        
        cld
        
	sub	ecx,4
        jl	CLASSIC
        
@@PIXEL4: lodsd
        mov	bl,ah		; pixel 1
        shr	eax,16
        mov	bh,ah		; pixel 2
        
        bswap	ebx		; store pixels to high hals of EBX.
        
        lodsd
        mov	bh,ah		; pixel 3
        shr	eax,16
        mov	bl,ah		; pixel 4
        
        bswap	ebx		; store pixels to high hals of EBX.
        mov	eax,ebx
        stosd
        
        sub	ecx,4
        jae	@@PIXEL4        
        
CLASSIC:add	ecx,4
	jz	@@ToEnd

@@PIXEL:	lodsw
	mov	al,ah
	stosb
	loop	@@PIXEL
        
@@ToEnd:
        ret                     ; _cdecl return
                
Conv16_8 endp


;*************************************************************************************


        public  Conv16_24
Conv16_24 proc \
        uses edi esi, \
        Dest:ptr byte, \
        Src:ptr WORD, \
        count:DWORD

        mov     ecx,[count]     ; cx=amount of pixels
        jecxz	@@ToEnd		; array has zero size

        mov     edi,[Dest]     ; di=first pointer (es=segment part)
        mov     esi,[Src]      ;
        
        cld
@@PIXEL:	lodsw
	mov	[edi],ah
	inc	edi
	stosb
	mov	[edi],ah
	inc	edi	
	loop	@@PIXEL
        
@@ToEnd:
        ret                     ; _cdecl return
                
Conv16_24 endp


;*************************************************************************************

        public  Conv16_32
Conv16_32 proc \
        uses edi esi ebx, \
        Dest:ptr dword, \
        Src:ptr byte, \
        count:DWORD

        mov     ecx,[count]     ; cx=amount of pixels
        jecxz	@@ToEnd		; array has zero size

        mov     edi,[Dest]     ; di=first pointer (es=segment part)
        mov     esi,[Src]      ;
        
        cld
@@PIXEL:	lodsw
	mov	bx,ax
	rol	eax,16
	mov	ax,bx
	stosd
	loop	@@PIXEL
        
@@ToEnd:
        ret                     ; _cdecl return
                
Conv16_32 endp


;*************************************************************************************
;*************************************************************************************


        public  Conv24_8
Conv24_8 proc \
        uses edi esi, \
        Dest:ptr byte, \
        Src:ptr byte, \
        count:DWORD

        mov     ecx,[count]     ; cx=amount of pixels
        jecxz	@@ToEnd		; array has zero size

        mov     edi,[Dest]     ; di=destination pointer (es=segment part)
        mov     esi,[Src]      ; si=source pointer 
        
        add	esi,2
        cld
@@PIXEL:	mov	al,[esi]
        add	esi,3	
	stosb
	loop	@@PIXEL
        
@@ToEnd:
        ret                     ; _cdecl return
                
Conv24_8 endp


;*************************************************************************************

        public  Conv24_16
Conv24_16 proc \
        uses edi esi, \
        Dest:ptr word, \
        Src:ptr byte, \
        count:DWORD

        mov     ecx,[count]     ; cx=amount of pixels
        jecxz	@@ToEnd		; array has zero size

        mov     edi,[Dest]     ; di=destination pointer (es=segment part)
        mov     esi,[Src]      ; si=source pointer 
        
        inc	esi
        cld
@@PIXEL:	mov	al,[esi]
	inc	esi
	mov	ah,[esi]
        add	esi,2	
	stosw
	loop	@@PIXEL
        
@@ToEnd:
        ret                     ; _cdecl return
                
Conv24_16 endp


;*************************************************************************************

        public  Conv24_32
Conv24_32 proc \
        uses edi esi, \
        Dest:ptr dword, \
        Src:ptr byte, \
        count:DWORD

        mov     ecx,[count]     ; cx=amount of pixels
        jecxz	@@ToEnd		; array has zero size

        mov     edi,[Dest]     ; di=destination pointer (es=segment part)
        mov     esi,[Src]      ; si=source pointer 

        cld
        
@@PIXEL:	lodsw
	shl	eax,16		; B2 B1 x x
	lodsb			; B2 B1 x B3
	mov	ah,al		; B2 B1 B3 B3  duplicate last 8 bits
	ror	eax,8		; B3 B2 B1 B3

	stosd
	
	dec	ecx
	jz	@@ToEnd
	
	lodsb			; x x x B1
	ror	eax,8		; B1 x x x
	lodsw			; B1 x B3 B2
	ror	eax,8		; B2 B1 x B3
	mov	ah,al		; B2 B1 B3 B3	
	ror	eax,8		; B3 B2 B1 B3	
	stosd
	
	loop	@@PIXEL
        
@@ToEnd:
        ret                     ; _cdecl return
                
Conv24_32 endp



        public  Conv24_64
Conv24_64 proc \
        uses edi esi, \
        Dest:ptr qword, \
        Src:ptr byte, \
        count:DWORD

        mov     ecx,[count]     ; cx=amount of pixels
        jecxz	@@ToEnd		; array has zero size

        mov     edi,[Dest]     ; di=destination pointer (es=segment part)
        mov     esi,[Src]      ; si=source pointer 

        cld
        
@@PIXEL:	lodsw			;  x x B2 B1
	mov	dh,ah
	shl	eax,16		; B2 B1 x x
	mov	ah,[esi]	; B2 B1 B3 x
	mov	al,dh		; B2 B1 B3 B2  << Stored lo DWORD
	mov	dl,ah
	inc	esi
	
	stosd
	rol	eax,16		; B3 B2 B2 B1
	mov	ah,al		; B3 B2 B1 B1
	mov	al,dl		; B3 B2 B1 B3  << Stored hi DWORD
	stosd

	dec	ecx
	jz	@@ToEnd
	
	lodsb			; x x x B1
	ror	eax,8		; B1 x x x
	lodsw			; B1 x B3 B2
	mov	dl,al		; B2	
	ror	eax,8		; B2 B1 x B3
	mov	ah,al		; B2 B1 B3 B3
	mov	al,dl		; B2 B1 B3 B2
	mov	dh,ah		; B3	
	stosd
	
	rol	eax,16		; B3 B2 B2 B1
	mov	ah,al		; B3 B2 B1 B1
	mov	al,dh		; B3 B2 B1 B3
	stosd
	
	loop	@@PIXEL
        
@@ToEnd:
        ret                     ; _cdecl return
                
Conv24_64 endp


;*************************************************************************************
;*************************************************************************************


        public  Conv32_1
Conv32_1 proc \
        uses edi esi ebx, \
        Dest:ptr dword, \
        Src:ptr byte, \
        count:DWORD

        mov     ecx,[count]     ; cx=amount of pixels
        jecxz	@@ToEnd		; array has zero size

        mov     edi,[Dest]     ; di=first pointer (es=segment part)
        mov     esi,[Src]      ;
        
        cld        
        mov	bl,1		; add end byte mark
@@PIXEL:	lodsd
	rol	eax,1		; copy the highest bit to CY
	rcl	bl,1		; transfer bit from CY
	jnc	@@NoOctet
	mov	[edi],bl
	inc	edi
	mov	bl,1		; add end byte mark
	loop	@@PIXEL
	jmp	@@ToEnd		; all done here
	
@@NoOctet:loop	@@PIXEL

@@First1:	sal	bl,1		; shift must be finished to 8th bit
	jnc	@@First1
	mov	[edi],bl	; store last incomplete byte

@@ToEnd:
        ret                     ; _cdecl return
                
Conv32_1 endp


;*************************************************************************************


        public  Conv32_4
Conv32_4 proc \
        uses edi esi, \
        Dest:ptr byte, \
        Src:ptr dword, \
        count:DWORD

        mov     ecx,[count]     ; cx=amount of pixels
        jecxz	@@ToEnd		; array has zero size

        mov     edi,[Dest]     ; di=first pointer (es=segment part)
        mov     esi,[Src]      ;
        
        cld
@@PIXEL:	add	esi,3
	lodsb			; load 1st hi byte
	and	al,0F0h
	
	dec	ecx
	jnz	@@NIBBLE2
	stosb			;store incomplete nibble
	jmp	@@ToEnd

@@NIBBLE2:mov	ah,al
	add	esi,3
        lodsb			; load 2nd hi byte
        and	al,0F0h
	ror	al,4
	or	al,ah
	stosb			;store both nibbles
	loop	@@PIXEL
        
@@ToEnd:
        ret                     ; _cdecl return
                
Conv32_4 endp



;*************************************************************************************


        public  Conv32_8
Conv32_8 proc \
        uses edi esi, \
        Dest:ptr qword, \
        Src:ptr byte, \
        count:DWORD

        mov     ecx,[count]     ; cx=amount of pixels
        jecxz	@@ToEnd		; array has zero size

        mov     edi,[Dest]     ; di=first pointer (es=segment part)
        mov     esi,[Src]      ;
        
        cld
@@PIXEL:	add	esi,3
	movsb
	loop	@@PIXEL
        
@@ToEnd:
        ret                     ; _cdecl return
                
Conv32_8 endp


;*************************************************************************************


        public  Conv32_16
Conv32_16 proc \
        uses edi esi, \
        Dest:ptr word, \
        Src:ptr dword, \
        count:DWORD

        mov     ecx,[count]     ; cx=amount of pixels
        jecxz	@@ToEnd		; array has zero size

        mov     edi,[Dest]     ; di=first pointer (es=segment part)
        mov     esi,[Src]      ;
        
        cld
@@PIXEL:	add	esi,2
	movsw
	loop	@@PIXEL
        
@@ToEnd:
        ret                     ; _cdecl return
                
Conv32_16 endp


;*************************************************************************************


        public  Conv32_24
Conv32_24 proc \
        uses edi esi, \
        Dest:ptr word, \
        Src:ptr dword, \
        count:DWORD

        mov     ecx,[count]     ; cx=amount of pixels
        jecxz	@@ToEnd		; array has zero size

        mov     esi,[Src]      ;
        mov     edi,[Dest]     ; di=first pointer (es=segment part)
        
        cld

@@PIXEL:	lodsd

	shr	eax,8
	mov	[edi],ax
	add	edi,2
	shr	eax,8
	mov	[edi],ah
	inc	edi

	dec	ecx
	jz	@@ToEnd
	
	lodsd
	shr	eax,8
	mov	[edi],al
	inc	edi
	shr	eax,8
	mov	[edi],ax
        add	edi,2		

	loop	@@PIXEL
        
@@ToEnd:
        ret                     ; _cdecl return
                
Conv32_24 endp


;*************************************************************************************


        public  Conv32_64
Conv32_64 proc \
        uses edi esi, \
        Dest:ptr qword, \
        Src:ptr dword, \
        count:DWORD

        mov     ecx,[count]     ; cx=amount of pixels
        jecxz	@@ToEnd		; array has zero size

        mov     edi,[Dest]     ; di=first pointer (es=segment part)
        mov     esi,[Src]      ;
        
        cld
@@PIXEL:	lodsd
	stosd
	stosd
	loop	@@PIXEL
        
@@ToEnd:
        ret                     ; _cdecl return
                
Conv32_64 endp




;########################################################################################
;########################################################################################
;########################################################################################

	public  Flip1
Flip1	proc \
        uses edi esi, \
        Data:ptr byte, \        
        count:DWORD
        
	mov	edx, [count]
	cmp	edx, 1
        jle	@@ToEnd		; array has zero size
        dec	edx
        
        mov	ecx, edx
        shr	edx, 3
	mov	edi, [Data]
	mov	esi, edi	; first byte pointer
	add	edi, edx	; last byte pointer
	
	and	ecx, 7
	
	mov	ax, 8080h	; shift mask
	shr	ah, cl		; upper mask 2 in cl
	
	cmp	edi, esi
	mov	cl, BYTE ptr [esi]	;cl should be prepared for shortened loop
	je	@@LoopMidFlip
	
	mov	ch, BYTE ptr [edi]		
LoopFlip:
	test	al, cl
	jz	@@FirstZero
	
	test	ah, ch	
	jnz	@@BitFlipped		; both bits are 1, nothing to do
	
	or	ch, ah			; set tail bit to 1 (use cached ch)
	xor	cl, al			; XOR will reset bite set - set front bit to 0	
	jmp	@@BitFlipped
	
@@FirstZero:
	test	ah, ch
	jz	@@BitFlipped		; both bits are 0, nothing to do
	
	or	cl, al	
	xor	ch, ah			; XOR will reset bite set

@@BitFlipped:
	ror	al,1
	jnc	@@NoIncFront
	
	rol	ah,1
	jnc	@@FrontNoTail		; No ptr change, continue in loop
	
	mov	BYTE ptr [esi], cl	; flush both ends
	mov	BYTE ptr [edi], ch
	dec	edi			; increment on carry
	inc	esi
	mov	cl, BYTE ptr [esi]
	mov	ch, BYTE ptr [edi]
	cmp	esi, edi
	jb	LoopFlip		; begin before tail continue loop esi<edi
	ja	@@ToEnd			; begin after tail - finished
	jmp	@@LoopMidGo
	
@@FrontNoTail:		
	mov	BYTE ptr [esi], cl	; flush both ends	
	inc	esi			; increment on carry
	mov	cl, BYTE ptr [esi]	
	cmp	esi, edi
	jb	LoopFlip		; continue in looping esi<edi; begin is before tail

	mov	BYTE ptr [edi], ch	; we must store ch to [edi] because of possible finish
	ja	@@ToEnd			; begin after tail - finished
	mov	cl, ch			; the value pointed by [esi] is be overwritten during [edi],ch; note edi==esi.
	jmp	@@LoopMidGo
	
@@NoIncFront:	
	rol	ah,1
	jnc	LoopFlip		; We can continue in loop as far as no byte progress.
		
	mov	BYTE ptr [edi], ch
	dec	edi			; decrement on carry	
	mov	ch, BYTE ptr [edi]
	cmp	esi, edi
	jb	LoopFlip		; begin before tail continue loop esi<edi
	mov	BYTE ptr [esi], cl	; we must store cl to [esi] because of possible finish
	ja	@@ToEnd			; begin after tail - finished	

@@LoopMidGo:
	cmp	al, ah			; bit mask must be compared
	jbe	@@ToEnd			; ah<al unsigned - finish
	
	; **************** begin==tail *******************
	
@@LoopMidFlip:
	test	al, cl
	jz	@@FirstMidZero
	
	test	ah, cl
	jnz	@@BitMidFlipped		; both bits are 1, nothing to do
	
	or	cl, ah			; set tail bit to 1 (use cached ch)
	xor	cl, al			; set front bit to 0
	jmp	@@BitMidFlipped
	
@@FirstMidZero:
	test	ah, cl
	jz	@@BitMidFlipped		; both bits are 0, nothing to do
	
	or	cl, al
	xor	cl, ah			; set tail bit to 0

@@BitMidFlipped:
	ror	al,1	
	rol	ah,1					
					
	cmp	al, ah			; bit mask must be compared
	ja	@@LoopMidFlip		; ah>al unsigned
	mov	BYTE ptr [esi], cl

@@ToEnd:
	ret

Flip1	endp


;########################################################################################


	public  Flip2
Flip2	proc \
        uses edi esi, \
        Data:ptr byte, \        
        count:DWORD
        
	mov	edx, [count]
	cmp	edx, 1
        jle	ToEnd		; ignore values 0 and 1
        add	edx,edx
        sub	edx,2
        
	mov	edi, [Data]
        mov	cx, dx
	shr	edx, 3
	mov	esi, edi	; first byte pointer
	add	edi, edx	; last byte pointer
	
	and	cx, 7
	
	mov	ax, 8080h	; shift mask
	shr	ah, cl		; upper mask 2 in cl
	
	cmp	edi, esi
	mov	cl, BYTE ptr [esi]	;cl should be prepared for shortened loop
	je	@@LoopMidFlip
	
	mov	ch, BYTE ptr [edi]
		
@@LoopFlip:
	test	al, cl
	jz	@@FirstZero
	
	test	ah, ch	
	jnz	@@BitFlipped		; both bits are 1, nothing to do
	
	or	ch, ah			; set tail bit to 1 (use cached ch)
	xor	cl, al			; XOR will reset bite set - set front bit to 0	
	jmp	@@BitFlipped
	
@@FirstZero:
	test	ah, ch
	jz	@@BitFlipped		; both bits are 0, nothing to do
	
	or	cl, al	
	xor	ch, ah			; XOR will reset bite set

@@BitFlipped:
	ror	al,1			; shift to 2nd bit
	ror	ah,1

	test	al, cl
	jz	FirstZero2
	
	test	ah, ch	
	jnz	BitFlipped2		; both bits are 1, nothing to do
	
	or	ch, ah			; set tail bit to 1 (use cached ch)
	xor	cl, al			; XOR will reset bite set - set front bit to 0	
	jmp	BitFlipped2
	
FirstZero2:
	test	ah, ch
	jz	BitFlipped2		; both bits are 0, nothing to do
	
	or	cl, al	
	xor	ch, ah			; XOR will reset bite set

BitFlipped2:
	ror	al,1
	jnc	@@NoIncFront
	
	rol	ah,3
	test	ah,02h	
	jz	@@FrontNoTail		; No ptr change, continue in loop

	
	mov	BYTE ptr [esi], cl	; flush both ends
	mov	BYTE ptr [edi], ch
	dec	edi			; increment on carry
	inc	esi
	mov	cl, BYTE ptr [esi]
	mov	ch, BYTE ptr [edi]
	cmp	esi, edi
	jb	@@LoopFlip		; begin before tail continue loop esi<edi
	ja	ToEnd			; begin after tail - finished
	jmp	@@LoopMidGo
	
@@FrontNoTail:		
	mov	BYTE ptr [esi], cl	; flush both ends	
	inc	esi			; increment on carry
	mov	cl, BYTE ptr [esi]	
	cmp	esi, edi
	jb	LoopFlip		; continue in looping esi<edi; begin is before tail

	mov	BYTE ptr [edi], ch	; we must store ch to [edi] because of possible finish
	ja	ToEnd			; begin after tail - finished
	mov	cl, ch			; the value pointed by [esi] is be overwritten during [edi],ch; note edi==esi.
	jmp	@@LoopMidGo
	
@@NoIncFront:	
	rol	ah,3
	test	ah,02h
	jz	LoopFlip		; We can continue in loop as far as no byte progress.
		
	mov	BYTE ptr [edi], ch
	dec	edi			; decrement on carry	
	mov	ch, BYTE ptr [edi]
	cmp	esi, edi
	jb	LoopFlip		; begin before tail continue loop esi<edi
	mov	BYTE ptr [esi], cl	; we must store cl to [esi] because of possible finish
	ja	ToEnd			; begin after tail - finished	

@@LoopMidGo:
	cmp	al, ah			; bit mask must be compared
	jbe	ToEnd			; ah<al unsigned - finish
	
	; **************** begin==tail *******************
	
@@LoopMidFlip:
	test	al, cl
	jz	@@FirstMidZero
	
	test	ah, cl
	jnz	@@BitMidFlipped		; both bits are 1, nothing to do
	
	or	cl, ah			; set tail bit to 1 (use cached ch)
	xor	cl, al			; set front bit to 0
	jmp	@@BitMidFlipped
	
@@FirstMidZero:
	test	ah, cl
	jz	@@BitMidFlipped		; both bits are 0, nothing to do
	
	or	cl, al
	xor	cl, ah			; set tail bit to 0

@@BitMidFlipped:
	ror	al,1	
	ror	ah,1
	
	test	al, cl
	jz	FirstMidZero2
	
	test	ah, cl
	jnz	@@BitMidFlipped2		; both bits are 1, nothing to do
	
	or	cl, ah			; set tail bit to 1 (use cached ch)
	xor	cl, al			; set front bit to 0
	jmp	@@BitMidFlipped2
	
FirstMidZero2:
	test	ah, cl
	jz	@@BitMidFlipped2		; both bits are 0, nothing to do
	
	or	cl, al
	xor	cl, ah			; set tail bit to 0

@@BitMidFlipped2:
	ror	al,1	
	rol	ah,3
					
	cmp	al, ah			; bit mask must be compared
	ja	@@LoopMidFlip		; ah>al unsigned
	mov	BYTE ptr [esi], cl

ToEnd:
	ret

Flip2	endp


;########################################################################################


	public  Flip4
Flip4	proc \
        uses edi esi, \
        Data:ptr byte, \        
        count:DWORD

        mov     ecx,[count]     ; cx=amount of pixels
        cmp	ecx,1		; 1 or less pixels makes no sense to flip
        jle	@@ToEnd		; array has zero size

        mov     edi,[Data]	; di=first pointer (es=segment part)
        mov     esi,edi	;
        
        shr	ecx,1		; divide 2       
        jc	PixOdd
        
	add	esi,ecx		; pixel count is even
        dec	esi
        
        cmp	edi,esi
        je	LastNibble	; This can occur for size=2.
LoopEven:mov	al,[edi]	; Process first byte with nibbles
	rol	al,4		; this shift flips nibbles	

	mov	dl,[esi]	; Process second byte with nibbles
	rol	dl,4

	mov	[esi],al
	mov	[edi],dl

	dec	esi
	inc	edi
	cmp	edi,esi
	jb	LoopEven	; esi<edi
	jne	@@ToEnd		; No one byte nible needs to be flipped.
LastNibble:
	mov	al,[edi]
	rol	al,4
	mov	[edi],al	; Last byte needs to flip nibbles.	
	jmp	@@ToEnd

                
PixOdd:	add	esi,ecx		; pixel count is odd i.e. >=3.
LoopOdd:mov	al,[edi]
	mov	ah,al
	and	ax,0F00Fh
	
	mov	dl,[esi]
	mov	dh,dl
	and	dx,0FF0h	
		
	or	ax,dx
	;mov	[edi],al	; nibble 1 flipped with nibble n - no need to store here	
	mov	[esi],ah	; nibble n flipped with nibble 1

	dec	esi
	cmp	esi,edi
	je	@@ToEndStore
	
	mov	ah,al		; contained in [edi]
	and	ax,0F00Fh	
	
	mov	dl,[esi]
	mov	dh,dl
	and	dx,0FF0h
	
	or	ax,dx
	mov	[edi],ah	; nibble 2 flipped with nibble n-1		
	mov	[esi],al	; nibble n-1 flipped with nibble 2

	inc	edi	
	cmp	edi,esi
	jb	LoopOdd

@@ToEnd:
        ret                     ; _cdecl return
        
@@ToEndStore:
	mov	[edi],al	; nibble 1 flipped with nibble n	
        ret                     ; _cdecl return
                
Flip4 endp


;*************************************************************************************

	public  Flip8
Flip8	proc \
        uses edi esi, \
        Data:ptr byte, \
        count:DWORD

        mov     ecx,[count]     ; cx=amount of pixels
        cmp	ecx,1
        jle	@@ToEnd		; ignore values 0 and 1

        mov     edi,[Data]	; di=first pointer (es=segment part)
        mov     esi,edi		;

        add	esi,ecx

        test	ecx,1
	jz	PxOp16s		; Test for even value.

			; Classic pixel exchange that works for odd and even values.
	dec	esi
@@PIXEL:	mov	al,[edi]
	mov	ah,[esi]
	mov	[edi],ah
	mov	[esi],al
	inc	edi
	dec	esi
	cmp	edi,esi
	jl	@@PIXEL
@@ToEnd:
	ret			; _cdecl return

			; Optimised word loop for even 'x' only.
PxOp16L:mov	dx,[esi]
	mov	ax,[edi]
	xchg	dl,dh
	xchg	al,ah
	mov	[edi],dx
	mov	[esi],ax
	add	edi,2
PxOp16s:sub	esi,2		; Loop entry point here is quite tricky. It alligns ESI to WORD boundary and fixes special case x=2.
	cmp	edi,esi
	jl	PxOp16L
	jnz	@@ToEnd2		; No middle WORD, bail out.

	mov	ax,[edi]	; Middle WORD must be also flipped.
	xchg	al,ah
	mov	[edi],ax
@@ToEnd2:	ret


Flip8 endp


;*************************************************************************************


	public  Flip16
Flip16	proc \
        uses edi esi, \
        Data:ptr byte, \        
        count:DWORD

        mov     ecx,[count]     ; cx=amount of pixels
        cmp	ecx,1
        jle	@@ToEnd		; array has zero size

        mov     edi,[Data]	; di=first pointer (es=segment part)
        mov     esi,edi		;
        
        add	esi,ecx
        add	esi,ecx
        sub	esi,2
                
@@PIXEL:	mov	ax,[edi]
	mov	cx,[esi]
	mov	[edi],cx
	mov	[esi],ax
	add	edi,2
	sub	esi,2
	cmp	edi,esi
	jl	@@PIXEL	

@@ToEnd:
        ret                     ; _cdecl return
                
Flip16 endp


;*************************************************************************************


	public  Flip24
Flip24	proc \
        uses edi esi, \
        Data:ptr byte, \        
        count:DWORD

        mov     ecx,[count]     ; cx=amount of pixels
        cmp	ecx,1
        jle	@@ToEnd		; array has zero size

        mov     edi,[Data]	; di=first pointer (es=segment part)
        mov     esi,edi
        
        dec	ecx
        add	esi,ecx
        add	esi,ecx
        add	esi,ecx		; 3*(size-1)        
                
@@PIXEL:	mov	al,[edi]	; byte 1
        mov	cl,[esi]
        mov	[edi],cl
        mov	[esi],al
        
        inc	edi
        inc	esi
        mov	al,[edi]	; byte 2
        mov	cl,[esi]
        mov	[edi],cl
        mov	[esi],al
        
        inc	edi
        inc	esi
        mov	al,[edi]	; byte 3
        mov	cl,[esi]
        mov	[edi],cl
        mov	[esi],al
        
        inc	edi
        sub	esi,5		; move to previous pixel +2 needs to shift -3 ...  ofs -5
	
	cmp	edi,esi
	jb	@@PIXEL		; unsigned comparison	

@@ToEnd:
        ret                     ; _cdecl return
                
Flip24 endp


;*************************************************************************************


	public  Flip32
Flip32	proc \
        uses edi esi, \
        Data:ptr byte, \        
        count:DWORD

        mov     ecx,[count]     ; cx=amount of pixels
        cmp	ecx,1
        jle	@@ToEnd		; array has zero size

        mov     edi,[Data]	; di=first pointer (es=segment part)
        mov     esi,edi		;

	dec	ecx        
        add	ecx,ecx		; count*2 - 2
        add	ecx,ecx		; count*4 - 4
        add	esi,ecx        
                
@@PIXEL:	mov	eax,[edi]
	mov	ecx,[esi]
	mov	[edi],ecx
	mov	[esi],eax
	add	edi,4
	sub	esi,4
	cmp	edi,esi
	jl	@@PIXEL	

@@ToEnd:
        ret                     ; _cdecl return
                
Flip32	endp


;*************************************************************************************


	public  Flip64
Flip64	proc \
        uses edi esi, \
        Data:ptr byte, \        
        count:DWORD

        mov     ecx,[count]     ; cx=amount of pixels
        cmp	ecx,1
        jle	@@ToEnd		; array has zero size

        mov     edi,[Data]	; di=first pointer (es=segment part)
        mov     esi, edi

	dec	ecx        
        add	ecx,ecx		; count*2 - 2
        add	ecx,ecx		; count*4 - 4
        add	ecx,ecx		; count*8 - 8
        add	esi,ecx        
       
@@PIXEL:       
       
if USE8087

	fild	QWORD ptr [edi]		; stack ->P0
	fild	QWORD ptr [esi]		; stack ->Pn; P0
	fistp	QWORD ptr [edi]		; stack <-Pn; P0
	fistp	QWORD ptr [esi]		; stack <-P0
	add	edi,8
	sub	esi,8

else	
	mov	eax,[edi]
	mov	ecx,[esi]
	mov	[edi],ecx
	mov	[esi],eax
	add	edi,4
	add	esi,4
	
	mov	eax,[edi]
	mov	ecx,[esi]
	mov	[edi],ecx
	mov	[esi],eax
	add	edi,4
	
	sub	esi,12		; +4 needs to shift -8 i.e. -12
endif

	cmp	edi,esi
	jl	@@PIXEL
@@ToEnd:
        ret                     ; _cdecl return
                
Flip64	endp


;*************************************************************************************


;void Join1BitNStep(const uint8_t *Buffer1Bit, uint8_t *Buffer, unsigned count, uint16_t PlaneStep)
	public  Join1BitNStep
Join1BitNStep proc \
        uses edi esi ebx, \
        Buffer1Bit: ptr byte, \
        Buffer: ptr byte, \
        count:DWORD, \
        PlaneStep: WORD
        
	mov	edi, [Buffer1Bit]
	or	edi,edi
	jz	@@ToEnd
	mov	esi, [Buffer]
	or	esi,esi
	jz	@@ToEnd
	mov	edx,[count]
	or	edx,edx
	jz	@@ToEnd
	mov	bx,[PlaneStep]
	mov	cl,bl		; nth bit
	mov	bl,bh
	and	ebx,0FFh	; byte increment

	mov	ch,1
	shl	ch,cl		; OR mask
	mov	cl,ch
	not	cl		; AND mask

	mov	al,[edi]	; 1 bit datastream
	stc
	rcl	al,1		; Feed one abundant bit from CY. CY contains bit 8.
	jmp	@@BitLoop2

@@BitLoop:shl	al,1
@@BitLoop2:mov	ah,[esi]
	jc	SetBit
	and	ah,cl
        jmp	StorByte	
	
SetBit:	or	ah,ch
StorByte:mov	[esi],ah
	add	esi,ebx

	cmp	al,80h
	je	Inc1Bit
	dec	edx
	jnz	@@BitLoop
@@ToEnd:
        ret                     ; _cdecl return		

Inc1Bit:dec	edx
	jz	@@ToEnd

	inc	edi
	mov	al,[edi]	; Get a new byte from 1 bit datastream
	stc
	rcl	al,1
	jmp	@@BitLoop2


Join1BitNStep	endp



;void Peel1BitNStep(uint8_t *Buffer1Bit, const uint8_t *BufferSrc, unsigned count, uint16_t PlaneStep)
	public  Peel1BitNStep
Peel1BitNStep proc \
        uses edi esi ebx, \
        Buffer1Bit: ptr BYTE, \
        BufferSrc: ptr BYTE, \
        count:DWORD, \
        PlaneStep: WORD
        
	mov	edi, [Buffer1Bit]
	or	edi,edi
	jz	@@ToEnd
	mov	esi, [BufferSrc]
	or	esi,esi
	jz	@@ToEnd
	mov	edx,[count]
	or	edx,edx
	jz	@@ToEnd
	mov	bx,[PlaneStep]
	mov	cl,bl		; nth bit
	mov	bl,bh
	and	ebx,0FFh	; byte increment

	inc	cl
	
	mov	al,1
	cld
@@BitLoop:mov	ah,[esi]
	add	esi,ebx
	
	shr	ah,cl		; needed bit goes to CY
	rcl	al,1
	jc	StoreBy
	dec	edx	
	jnz	@@BitLoop

ShiftAll:sal	al,1		; incomplete bits should be shaped and stored.
	jnc	ShiftAll
	mov	[edi],al
	jmp	@@ToEnd
	
StoreBy:stosb			; store 8 bits
	mov	al,1
	dec	edx	
	jnz	@@BitLoop

@@ToEnd:
        ret                     ; _cdecl return
                
Peel1BitNStep	endp


;void Join8BitNStep(const uint8_t *Buffer8Bit, uint8_t *Buffer, unsigned count, uint8_t ByteStep)
	public  Join8BitNStep
Join8BitNStep proc \
        uses edi esi, \
        Buffer8Bit: ptr byte, \
        Buffer: ptr byte, \
        count:DWORD, \
        ByteStep: BYTE
        
        mov	edi, [Buffer]
        or	edi,edi
        jz	@@ToEnd
        mov	esi, [Buffer8Bit]
        or	esi,esi
        jz	@@ToEnd
        xor	edx,edx
        mov	dl, [ByteStep]
        mov	ecx, [count]
        jecxz	@@ToEnd

	cld
ByteLop:lodsb
	mov	[edi],al
	add	edi,edx
	loop	ByteLop
@@ToEnd:
        ret                     ; _cdecl return        
        
Join8BitNStep endp


;void Peel8BitNStep(uint8_t *Buffer8Bit, const uint8_t *BufferSrc, unsigned count, uint8_t ByteStep)
	public  Peel8BitNStep
Peel8BitNStep proc \
        uses edi esi, \
        Buffer8Bit: ptr byte, \
        BufferSrc: ptr byte, \
        count:DWORD, \
        ByteStep: BYTE
        
        mov	edi, [Buffer8Bit]
        or	edi,edi
        jz	@@ToEnd
        mov	esi, [BufferSrc]
        or	esi,esi
        jz	@@ToEnd
        xor	edx,edx
        mov	dl, [ByteStep]
        mov	ecx, [count]
        jecxz	@@ToEnd

	cld
@@ByteLop:mov	al,[esi]
	add	esi,edx
	stosb
	loop	@@ByteLop
@@ToEnd:
        ret                     ; _cdecl return        
        
Peel8BitNStep endp


        end
