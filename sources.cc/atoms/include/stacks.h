/****************************************************
* unit:    stack           release 0.5              *
* purpose: general manipulation with FIFO structure *
****************************************************/
#ifndef __stacks_h
#define __stacks_h

#include <common.h>

#ifndef No_Memory
 #define No_Memory     0x1
#endif
#ifndef Bad_Allocated
 #define Bad_Allocated 0x2
#endif

#ifdef Streams
 #ifdef IOSTREAM_H_ONLY
  #include<iostream.h>
 #else
  #include<iostream>
 #endif
#endif




// Main definition part of Stack
class stack {
protected:
	    int *data;
	    int Level, Allocated;

public:
            stack(void);
            stack(const stack & s);
	    ~stack(void)                        {erase(*this);};

	    stack &operator=(const stack &s);

	    int pop(void);
	    void push(int value);
	    void Erase(void)			{erase(*this);};
	    int operator[](int i) const;

	    friend int Card(const stack &s)	  {return(s.Level);}
	    friend int EmptyCheck(const stack &s) {return(s.Level<=0);}

            friend void erase(stack &s);
            friend int check(stack &s);
#ifdef Streams
 #ifdef IOSTREAM_H_ONLY
	    friend ostream &operator<<(ostream & xout, const stack & s);
 #else
            friend std::ostream &operator<<(std::ostream & xout, const stack & s);
 #endif
#endif
            };


#ifdef Streams
 #ifdef IOSTREAM_H_ONLY
	    ostream &operator<<(ostream & xout, const stack & s);
 #else
            std::ostream &operator<<(std::ostream & xout, const stack & s);
 #endif
#endif


#endif
