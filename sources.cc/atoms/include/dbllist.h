/*****************************************************************
* unit:    doublelists             release 0.7                   *
* purpose: general manipulation with array of couples of strings *
* Licency: GPL or LGPL                                           *
* Copyright: (c) 1998-2023 Jaroslav Fojtik                       *
******************************************************************/
#ifndef __DoubleList_h
#define __DoubleList_h


#ifndef __Lists_h
  #include "lists.h"
#endif


class doublelist:public list {
           int flipped;
           
   public:
	   doublelist(void): flipped(0) {};
           doublelist(const doublelist &dl);

           const char *Find(const char *key) const;
	   char *Member(const int i, const int j);
           const char *Member(const int i, const int j) const;

	   //int operator+=(const char *str);
	   doublelist &operator=(const doublelist &dl);

	   void Add(const char *str1,const char *str2);

           void Flip(int FlipMode);
	   };


#ifdef Streams
 #ifdef IOSTREAM_H_ONLY
  ostream &operator<<(ostream &xout, const doublelist & d);
 #else
  std::ostream &operator<<(std::ostream &xout, const doublelist & d);
  //istream &operator>>(istream &, doublelist & s);
 #endif
#endif



#endif
