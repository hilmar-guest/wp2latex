#ifndef __SWAP_BITS_XLAT__
#define __SWAP_BITS_XLAT__


#ifdef __cplusplus
extern "C" {
#endif
extern const unsigned char swap_bits_xlat[256];
extern const unsigned char swap_bits2_xlat[256];
#ifdef __cplusplus
}
#endif


#endif
