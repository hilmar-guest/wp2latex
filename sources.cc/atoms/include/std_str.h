/* String functions that do not depend on the string object */
#ifndef __std_str_h
#define __std_str_h

#include <string.h>

#ifndef __Common_H_INCLUDED
 #include "common.h"
#endif   //__Common_H_INCLUDED



#ifdef __cplusplus


/**This function fixes error in finding substring in NULL string*/
#ifdef __Safe_strchr
 #ifdef __Strict_Const_Procs
  inline const char *StrChr(const char *s, int c) {return(strchr(s,c));}
  inline char *StrChr(char *s, int c) {return(strchr(s,c));}
 #else
  inline char *StrChr(const char *s, int c) {return(strchr(s,c));}
 #endif
#else
 #ifdef __Strict_Const_Procs
  const char *StrChr(const char *s, int c);
  char *StrChr(char *s, int c);
 #else
  char *StrChr(const char *s, int c);
 #endif
#endif

#ifdef __Safe_strstr
 #ifdef __Strict_Const_Procs
  inline const char *StrStr(const char *str1,const char *str2) {return(strstr(str1,str2));}
  inline char *StrStr(char *str1,const char *str2) {return(strstr(str1,str2));}
 #else
  inline char *StrStr(const char *str1,const char *str2) {return(strstr(str1,str2));}
 #endif
#else
 #ifdef __Strict_Const_Procs
  const char *StrStr(const char *str1,const char *str2);
  char *StrStr(char *str1,const char *str2);
 #else
  char *StrStr(const char *str1,const char *str2);
 #endif
#endif


/**This function fixes up the error in comparing two NULL string*/
#ifdef __Safe_strcmp
 inline int StrCmp(const char *str1,const char *str2) {return(strcmp(str1,str2));}
#else
 int StrCmp(const char *str1,const char *str2);
#endif


#ifdef __Safe_strncmp
inline int StrNCmp(const char *str1,const char *str2,int str_size)
	{return(strcmp(str1,str2,str_size));}
#else
int StrNCmp(const char *str1,const char *str2,int str_size);
#endif


/**This function fixes up the error in measuring a length of the NULL string*/
#ifdef __Safe_strlen
 #define StrLen strlen
#else
 unsigned StrLen(const char *str);
#endif


/** this check is aimed for string argument inside printf("%s",NULL) */
#ifdef __Disable_NULL_printf
 #define chk(x) x==NULL?"NULL":x
#else
 #define chk(x) x
#endif


#if defined(__ARMCC_VERSION)
extern "C" {
char *strdup(const char *str);
}
#endif

/** This function fixes up the error in duplicating NULL string. */
inline char *StrDup (const char *source)
{
 if(source==NULL) return NULL;
 return strdup(source);
}

#define __InlineStrdup

#endif	/* __cplusplus */

#endif  //__std_str_h
