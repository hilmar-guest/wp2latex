/******************************************************************************
 * Unit:    raster            release 0.33                                    *
 * Prototype declaration of separate classes. Normally this is not needed     *
 * to be included.                                                            *
 * licency:     GPL or LGPL                                                   *
 * Copyright: (c) 1998-2025 Jaroslav Fojtik                                   *
 ******************************************************************************/
#ifndef _RAS_PROT_H_
#define _RAS_PROT_H_

//#define OPTIMISE_SPEED
#include "raster.h"


#ifdef _MSC_VER
 #pragma warning (push)
 #pragma warning (disable : 4250) 
#endif


/********************* Specialised Raster 1D modules *******************/

/** 1 bit plane */
class Raster1D_1Bit: virtual public Raster1DAbstract
	{
public: virtual int GetPlanes(void) const {return(1);};

	virtual uint32_t GetValue1D(unsigned x) const;
	virtual void SetValue1D(unsigned x, uint32_t NewValue);
#ifdef _REENTRANT
        virtual uint32_t PTR_GetValue1D(const void *RAW_Data1D, unsigned x) const;
        virtual void PTR_SetValue1D(void *RAW_Data1D, unsigned x, uint32_t NewValue) const;
#endif
#ifdef OPTIMISE_SPEED
	virtual void Get(Raster1DAbstract &R1) const;
	virtual void Set(const Raster1DAbstract &R1);
        virtual void Peel1Bit(void *Buffer1Bit, uint8_t plane) const;
        void Join1Bit(const void *Buffer1Bit, uint8_t plane);
#endif
	};


/** 2 bit planes */
class Raster1D_2Bit: virtual public Raster1DAbstract
	{
public: virtual int GetPlanes(void) const {return(2);};

	virtual uint32_t GetValue1D(unsigned x) const;
	virtual void SetValue1D(unsigned x, uint32_t NewValue);
#ifdef _REENTRANT
        virtual uint32_t PTR_GetValue1D(const void *RAW_Data1D, unsigned x) const;
        virtual void PTR_SetValue1D(void *RAW_Data1D, unsigned x, uint32_t NewValue) const;
#endif
	};


/** 4 bit planes */
class Raster1D_4Bit: virtual public Raster1DAbstract
	{
public: virtual int GetPlanes(void) const {return(4);};

	virtual uint32_t GetValue1D(unsigned x) const;
	virtual void SetValue1D(unsigned x, uint32_t NewValue);
#ifdef _REENTRANT
        virtual uint32_t PTR_GetValue1D(const void *RAW_Data1D, unsigned x) const;
        virtual void PTR_SetValue1D(void *RAW_Data1D, unsigned x, uint32_t NewValue) const;
#endif

#ifdef OPTIMISE_SPEED
	virtual void Set(const Raster1DAbstract &R1);
	virtual void Get(Raster1DAbstract &R1) const;
#endif	//OPTIMISE_SPEED
	};


class Raster1D_4BitIDX: public Raster1D_4Bit
{
public: virtual int GetPlanes(void) const {return(-4);};
};


/** 8 bit planes */
class Raster1D_8Bit: virtual public Raster1DAbstract
	{
public:	Raster1D_8Bit(int InitSize1D) {if((Data1D=malloc(InitSize1D))!=NULL) Size1D=InitSize1D;}
	Raster1D_8Bit(void)	{};

	virtual int GetPlanes(void) const {return(8);};
#ifdef OPTIMISE_SPEED
	virtual void Set(const Raster1DAbstract &R1);
	virtual void Get(Raster1DAbstract &R1) const;
        virtual void Peel1Bit(void *Buffer1Bit, uint8_t plane) const;
        virtual void Join1Bit(const void *Buffer1Bit, uint8_t plane);
	virtual void Peel8Bit(void *Buffer8Bit, uint8_t plane8) const;
	virtual void Join8Bit(const void *Buffer8Bit, uint8_t plane8);
#endif	//OPTIMISE_SPEED

	virtual uint32_t GetValue1D(unsigned x) const;
	virtual void SetValue1D(unsigned x, uint32_t NewValue);

#ifdef _REENTRANT
        virtual uint32_t PTR_GetValue1D(const void *RAW_Data1D, unsigned x) const;
        virtual void PTR_SetValue1D(void *RAW_Data1D, unsigned x, uint32_t NewValue) const;
        virtual double PTR_GetValue1Dd(const void *RAW_Data1D, unsigned x) const;
	virtual void PTR_SetValue1Dd(void *RAW_Data1D, unsigned x, double NewValue) const;	
#endif
	};


class Raster1D_8BitIDX: public Raster1D_8Bit
{
public: virtual int GetPlanes(void) const {return(-8);};
};



/** 16 bit planes */
class Raster1D_16Bit: virtual public Raster1DAbstract
	{
public: Raster1D_16Bit(int InitSize1D) {if((Data1D=malloc(2*InitSize1D))!=NULL) Size1D=InitSize1D;}
	Raster1D_16Bit(void)	{};

	virtual int GetPlanes(void) const {return(16);};

	virtual uint32_t GetValue1D(unsigned x) const {if(Size1D<=x) return(0); return(((uint16_t *)Data1D)[x]);};
	virtual void SetValue1D(unsigned x, uint32_t NewValue) {if(Size1D<=x) return; ((uint16_t *)Data1D)[x]=(uint16_t)NewValue;};
#ifdef _REENTRANT
        virtual uint32_t PTR_GetValue1D(const void *RAW_Data1D, unsigned x) const;
        virtual void PTR_SetValue1D(void *RAW_Data1D, unsigned x, uint32_t NewValue) const;
	virtual double PTR_GetValue1Dd(const void *RAW_Data1D, unsigned x) const;
	virtual void PTR_SetValue1Dd(void *RAW_Data1D, unsigned x, double NewValue) const;
#endif
#ifdef OPTIMISE_SPEED
	virtual void Get(Raster1DAbstract &R1) const;
	virtual void Set(const Raster1DAbstract &R1);
        virtual void Peel1Bit(void *Buffer1Bit, uint8_t plane) const;
        virtual void Join1Bit(const void *Buffer1Bit, uint8_t plane);
#endif
	};


/*
class Raster1D_16BitIDX: public Raster1D_16Bit
{
public: Raster1D_16BitIDX(int InitSize1D, Raster1DAbstractRGB *newPalette=NULL);
        Raster1D_16BitIDX(void)	{pPalette=NULL;};
        ~Raster1D_16BitIDX();

        virtual int GetPlanes(void) const {return(-16);};
        virtual void Set(const Raster1DAbstract &R1);

        Raster1DAbstractRGB *pPalette;
};
*/


/** 24 bit planes Gray */
class Raster1D_24Bit:virtual public Raster1DAbstract
	{
public: virtual int GetPlanes(void) const {return(24);};

	virtual uint32_t GetValue1D(unsigned x) const;
	virtual void SetValue1D(unsigned x, uint32_t NewValue);
#ifdef _REENTRANT
        virtual uint32_t PTR_GetValue1D(const void *RAW_Data1D, unsigned x) const;
	virtual void PTR_SetValue1D(void *RAW_Data1D, unsigned x, uint32_t NewValue) const;
#endif
#ifdef OPTIMISE_SPEED
	virtual void Get(Raster1DAbstract &R1) const;
	virtual void Set(const Raster1DAbstract &R1);
        virtual void Peel1Bit(void *Buffer1Bit, uint8_t plane) const;
        virtual void Join1Bit(const void *Buffer1Bit, uint8_t plane);
#endif
	virtual void Get24BitRGB(void *Buffer24Bit) const;

	virtual void Peel8Bit(void *Buffer8Bit, uint8_t plane8) const;
	virtual void Join8Bit(const void *Buffer8Bit, uint8_t plane8);
	};


/** 32 bit planes */
class Raster1D_32Bit:virtual public Raster1DAbstract
	{
public: virtual int GetPlanes(void) const {return(32);};

	virtual uint32_t GetValue1D(unsigned x) const {if(x<Size1D) return(((uint32_t *)Data1D)[x]); return(0);};
	virtual void SetValue1D(unsigned x, uint32_t NewValue) {if(x<Size1D) ((uint32_t *)Data1D)[x]=NewValue; return;};
	virtual void SetValue1Dd(unsigned x, double NewValue);
#ifdef _REENTRANT
        virtual uint32_t PTR_GetValue1D(const void *RAW_Data1D, unsigned x) const;
        virtual void PTR_SetValue1D(void *RAW_Data1D, unsigned x, uint32_t NewValue) const;
        virtual void PTR_SetValue1Dd(void *RAW_Data1D, unsigned x, double NewValue) const;
#endif
#ifdef OPTIMISE_SPEED
	virtual void Get(Raster1DAbstract &R1) const;
	virtual void Set(const Raster1DAbstract &R1);
        virtual void Peel1Bit(void *Buffer1Bit, uint8_t plane) const;
        virtual void Join1Bit(const void *Buffer1Bit, uint8_t plane);
#endif
	virtual void Peel8Bit(void *Buffer8Bit, uint8_t plane8) const;
	virtual void Join8Bit(const void *Buffer8Bit, uint8_t plane8);
	};


#if defined(uint64_t_defined)
/** 64 bit planes */
class Raster1D_64Bit:virtual public Raster1DAbstract
	{
public: virtual int GetPlanes(void) const {return(64);};

	virtual uint32_t GetValue1D(unsigned x) const;
	virtual double GetValue1Dd(unsigned x) const {if(x<Size1D) return((double)((uint64_t *)Data1D)[x]); return(0);};
	virtual void SetValue1D(unsigned x, uint32_t NewValue);
	virtual void SetValue1Dd(unsigned x, double NewValue);
		// These 4 functions cannot be replaced by abstract predecesor as far as it supports max 32bpp.
        virtual void Peel1Bit(void *Buffer1Bit, uint8_t plane) const;
        virtual void Join1Bit(const void *Buffer1Bit, uint8_t plane);
	virtual void Peel8Bit(void *Buffer8Bit, uint8_t plane8) const;
	virtual void Join8Bit(const void *Buffer8Bit, uint8_t plane8);
#ifdef _REENTRANT
        virtual uint32_t PTR_GetValue1D(const void *RAW_Data1D, unsigned x) const;
        virtual double PTR_GetValue1Dd(const void *RAW_Data1D, unsigned x) const;
        virtual void PTR_SetValue1D(void *RAW_Data1D, unsigned x, uint32_t NewValue) const;
        virtual void PTR_SetValue1Dd(void *RAW_Data1D, unsigned x, double NewValue) const;
#endif
#ifdef OPTIMISE_SPEED
	virtual void Get(Raster1DAbstract &R1) const;
	virtual void Set(const Raster1DAbstract &R1);
#endif
	};
#endif


/** 32 bit planes - float */
class Raster1D_32FltBit: virtual public Raster1D_32Bit
	{
public: float Min, Max;
	Raster1D_32FltBit(): Min(0), Max(1) {};

	virtual int GetPlanes(void) const {return(-32);};

	virtual uint32_t GetValue1D(unsigned x) const;
	virtual double GetValue1Dd(unsigned x) const;
	virtual void SetValue1D(unsigned x, uint32_t NewValue);
	virtual void SetValue1Dd(unsigned x, double NewValue);
#ifdef _REENTRANT
	virtual uint32_t PTR_GetValue1D(const void *RAW_Data1D, unsigned x) const;
	virtual double PTR_GetValue1Dd(const void *RAW_Data1D, unsigned x) const;
	virtual void PTR_SetValue1D(void *RAW_Data1D, unsigned x, uint32_t NewValue) const;
	virtual void PTR_SetValue1Dd(void *RAW_Data1D, unsigned x, double NewValue) const;
#endif
#ifdef OPTIMISE_SPEED
        virtual void Get(Raster1DAbstract &R1) const;
        virtual void Set(const Raster1DAbstract &R1);
#endif
	};


/** 64 bit planes - double */
class Raster1D_64FltBit: virtual public 
#if defined(uint64_t_defined)
					Raster1D_64Bit
#else
					Raster1DAbstract
#endif
{
public: double Min, Max;
	Raster1D_64FltBit(): Min(0), Max(1) {};

	virtual int GetPlanes(void) const {return(-64);};

	virtual uint32_t GetValue1D(unsigned x) const;
	virtual double GetValue1Dd(unsigned x) const;
	virtual void SetValue1D(unsigned x, uint32_t NewValue);
	virtual void SetValue1Dd(unsigned x, double NewValue);
#ifdef _REENTRANT
	virtual uint32_t PTR_GetValue1D(const void *RAW_Data1D, unsigned x) const;
	virtual double PTR_GetValue1Dd(const void *RAW_Data1D, unsigned x) const;
	virtual void PTR_SetValue1D(void *RAW_Data1D, unsigned x, uint32_t NewValue) const;
	virtual void PTR_SetValue1Dd(void *RAW_Data1D, unsigned x, double NewValue) const;
#endif
#ifdef OPTIMISE_SPEED
        virtual void Get(Raster1DAbstract &R1) const;
        virtual void Set(const Raster1DAbstract &R1);
#endif
};


//class Raster1D_5BitRGB: virtual public Raster1D_16Bit, virtual public Raster1DAbstractRGB
class Raster1D_16Bit565: virtual public Raster1D_16Bit, virtual public Raster1DAbstractRGB
{
public: Raster1D_16Bit565(int InitSize1D): Raster1D_16Bit(InitSize1D) {}
        Raster1D_16Bit565() {}

	virtual uint32_t R(unsigned x) const;
	virtual uint32_t G(unsigned x) const;
	virtual uint32_t B(unsigned x) const;

	virtual uint32_t GetValue1D(unsigned x) const;
	virtual void SetValue1D(unsigned x, uint32_t NewValue);
	virtual void Get(unsigned index, RGBQuad *RGB) const;
	virtual void Set(unsigned index, const RGBQuad *RGB);

	//virtual void Get24BitRGB(void *Buffer24Bit) const;
        virtual uint32_t GetValue1DRAW(unsigned x) const {if(x<Size1D) return(((uint16_t *)Data1D)[x]); return(0);};
	virtual void SetValue1DRAW(unsigned x, uint32_t NewValue) {if(x<Size1D) ((uint16_t *)Data1D)[x]=NewValue;};
#ifdef OPTIMISE_SPEED
	//virtual void Get(Raster1DAbstract &R1) const {Raster1DAbstractRGB::Get(R1);}
	void Set(const Raster1DAbstract &R1) {Raster1DAbstractRGB::Set(R1);}
#endif
#ifdef _REENTRANT
        virtual uint32_t PTR_GetValue1DRAW(const void *RAW_Data1D, unsigned x) const;
	virtual void PTR_SetValue1DRAW(void *RAW_Data1D, unsigned x, uint32_t NewValue);
#endif
};



/** 8 bit planes for 3 channels R,G,B */
class Raster1D_8BitRGB: virtual public Raster1DAbstractRGB
	{
public:	Raster1D_8BitRGB(int InitSize1D)  {Allocate1D(InitSize1D);};
	Raster1D_8BitRGB(void)		{};

	virtual int GetPlanes(void) const {return(3*8);};
	virtual unsigned GetSize1D(void) const {return(Size1D);};

	virtual void Get(unsigned index, RGBQuad *RGB) const;
#ifdef OPTIMISE_SPEED
        virtual void Set(unsigned x, const RGBQuad *RGB);        
#endif

        void Set(const Raster1DAbstract &R1);

	virtual void Get24BitRGB(void *Buffer24Bit) const;

	virtual uint32_t GetValue1DRAW(unsigned x) const {if(x<3*Size1D) return(((uint8_t *)Data1D)[x]); return(0);};
	virtual void SetValue1DRAW(unsigned x, uint32_t NewValue) {if(x<3*Size1D) ((uint8_t *)Data1D)[x]=(uint8_t)NewValue;};

#ifdef _REENTRANT
        virtual uint32_t PTR_GetValue1DRAW(const void *RAW_Data1D, unsigned x) const 
            {if(x<3*Size1D) return(((uint8_t *)RAW_Data1D)[x]); return(0);};
	virtual void PTR_SetValue1DRAW(void *RAW_Data1D, unsigned x, uint32_t NewValue)
            {if(x<3*Size1D) ((uint8_t *)RAW_Data1D)[x]=NewValue;};
        virtual uint32_t PTR_GetValue1D(const void *RAW_Data1D, unsigned x) const; 
	virtual void PTR_SetValue1D(void *RAW_Data1D, unsigned x, uint32_t NewValue) const;
#endif

	virtual void Peel8Bit(void *Buffer8Bit, uint8_t plane8) const;
	virtual void Join8Bit(const void *Buffer8Bit, uint8_t plane8);

	virtual uint32_t GetValue1D(unsigned x) const;
	virtual void SetValue1D(unsigned x, uint32_t NewValue);
	};


/** 16 bit planes for 3 channels R,G,B */
class Raster1D_16BitRGB: virtual public Raster1DAbstractRGB
	{
public:	Raster1D_16BitRGB(int InitSize1D)	{Allocate1D(InitSize1D);}
	Raster1D_16BitRGB(void)	{};

	virtual unsigned GetSize1D(void) const {return(Size1D);};
	virtual int GetPlanes(void) const {return(3*16);};

	virtual void Get(unsigned index, RGBQuad *RGB) const;
	//virtual void Set(unsigned index, const RGBQuad *RGB);
        void Set(const Raster1DAbstract &R1);

	virtual void Get24BitRGB(void *Buffer24Bit) const;

        virtual uint32_t GetValue1DRAW(unsigned x) const {if(x<3*Size1D) return(((uint16_t *)Data1D)[x]); return(0);};
	virtual void SetValue1DRAW(unsigned x, uint32_t NewValue) {if(x<3*Size1D) ((uint16_t *)Data1D)[x]=NewValue;};

	virtual uint32_t GetValue1D(unsigned x) const;
	virtual void SetValue1D(unsigned x, uint32_t NewValue);

#ifdef _REENTRANT
protected:
        virtual uint32_t PTR_GetValue1DRAW(const void *RAW_Data1D, unsigned x) const 
            {if(x<3*Size1D) return(((uint16_t *)RAW_Data1D)[x]); return(0);};
	virtual void PTR_SetValue1DRAW(void *RAW_Data1D, unsigned x, uint32_t NewValue)
            {if(x<3*Size1D) ((uint16_t *)RAW_Data1D)[x]=NewValue;};
        virtual uint32_t PTR_GetValue1D(const void *RAW_Data1D, unsigned x) const; 
	virtual void PTR_SetValue1D(void *RAW_Data1D, unsigned x, uint32_t NewValue) const;
#endif
	};


/** 8 bit planes for 4 channels R,G,B, A */
class Raster1D_8BitRGBA: virtual public Raster1DAbstractRGBA
	{
public:	Raster1D_8BitRGBA(int InitSize1D)  {Allocate1D(InitSize1D);};
	Raster1D_8BitRGBA(void)		{};

	virtual int GetPlanes(void) const {return(4*8);};
	virtual unsigned GetSize1D(void) const {return(Size1D);};

	virtual uint32_t GetValue1DRAW(unsigned x) const {if(x<4*Size1D) return(((uint8_t *)Data1D)[x]); return(0);};
	virtual void SetValue1DRAW(unsigned x, uint32_t NewValue) {if(x<4*Size1D) ((uint8_t *)Data1D)[x]=NewValue;};

	virtual uint32_t GetValue1D(unsigned x) const;
	virtual void SetValue1D(unsigned x, uint32_t NewValue);
	virtual void Get(unsigned x, RGBQuad *RGB) const;
	virtual void Set(const Raster1DAbstract &R1);
	void Get24BitRGB(void *Buffer24Bit) const;

#ifdef _REENTRANT
	virtual uint32_t PTR_GetValue1D(const void *RAW_Data1D, unsigned x) const;
	virtual void PTR_SetValue1D(void *RAW_Data1D, unsigned x, uint32_t NewValue) const;
protected:
        virtual uint32_t PTR_GetValue1DRAW(const void *RAW_Data1D, unsigned x) const 
            {if(x<4*Size1D) return(((uint8_t *)RAW_Data1D)[x]); return(0);};
	virtual void PTR_SetValue1DRAW(void *RAW_Data1D, unsigned x, uint32_t NewValue) 
            {if(x<4*Size1D) ((uint8_t *)RAW_Data1D)[x]=NewValue;};
#endif

	virtual void Peel8Bit(void *Buffer8Bit, uint8_t plane8) const;
	virtual void Join8Bit(const void *Buffer8Bit, uint8_t plane8);
	};


/** 16 bit planes for 4 channels R,G,B, A */
class Raster1D_16BitRGBA: virtual public Raster1DAbstractRGBA
	{
public:	Raster1D_16BitRGBA(int InitSize1D)	{Allocate1D(InitSize1D);}
	Raster1D_16BitRGBA(void)		{};

	virtual unsigned GetSize1D(void) const {return(Size1D);};
	virtual int GetPlanes(void) const {return(4*16);};

        virtual uint32_t GetValue1DRAW(unsigned x) const {if(x<4*Size1D) return(((uint16_t *)Data1D)[x]);return(0);};
	virtual void SetValue1DRAW(unsigned x, uint32_t NewValue) {if(x<4*Size1D) ((uint16_t *)Data1D)[x]=NewValue;};

	virtual uint32_t GetValue1D(unsigned x) const;
	virtual void SetValue1D(unsigned x, uint32_t NewValue);
        virtual void Set(const Raster1DAbstract &R1);

#ifdef _REENTRANT
	virtual uint32_t PTR_GetValue1D(const void *RAW_Data1D, unsigned x) const;
        virtual void PTR_SetValue1D(void *RAW_Data1D, unsigned x, uint32_t NewValue) const;
protected:
        virtual uint32_t PTR_GetValue1DRAW(const void *RAW_Data1D, unsigned x) const 
            {if(x<4*Size1D) return(((uint16_t *)RAW_Data1D)[x]);return(0);};
	virtual void PTR_SetValue1DRAW(void *RAW_Data1D, unsigned x, uint32_t NewValue) 
            {if(x<4*Size1D) ((uint16_t *)RAW_Data1D)[x]=NewValue;};
#endif
};


/********************* Specialised Raster 2D modules *********************/

class Raster2D_1Bit: virtual public Raster2DAbstract, public Raster1D_1Bit {};
class Raster2D_2Bit: virtual public Raster2DAbstract, public Raster1D_2Bit {};
class Raster2D_4Bit: virtual public Raster2DAbstract, public Raster1D_4Bit {};
class Raster2D_8Bit: virtual public Raster2DAbstract, public Raster1D_8Bit {};
class Raster2D_16Bit:virtual public Raster2DAbstract, public Raster1D_16Bit {};
class Raster2D_24Bit:virtual public Raster2DAbstract, public Raster1D_24Bit {};
class Raster2D_32Bit:virtual public Raster2DAbstract, public Raster1D_32Bit {};
#if defined(uint64_t_defined)
class Raster2D_64Bit:virtual public Raster2DAbstract, public Raster1D_64Bit {};
#endif

class Raster2D_32FltBit:virtual public Raster2DAbstract,public Raster1D_32FltBit {};
class Raster2D_64FltBit:virtual public Raster2DAbstract,public Raster1D_64FltBit {};

class Raster2D_8BitRGB: virtual public Raster2DAbstractRGB, virtual public Raster1D_8BitRGB {};
class Raster2D_16BitRGB: virtual public Raster2DAbstractRGB, virtual public Raster1D_16BitRGB {};

class Raster2D_16Bit565: virtual public Raster2DAbstractRGB, virtual public Raster1D_16Bit565 {};

class Raster2D_8BitRGBA: virtual public Raster2DAbstractRGBA, virtual public Raster1D_8BitRGBA {};
class Raster2D_16BitRGBA: virtual public Raster2DAbstractRGBA, virtual public Raster1D_16BitRGBA {};


/********************* Specialised Raster 3D modules *********************/
#ifdef RASTER_3D

class Raster3D_1Bit: virtual public Raster3DAbstract, public Raster2D_1Bit {};
class Raster3D_2Bit: virtual public Raster3DAbstract, public Raster2D_2Bit {};
class Raster3D_4Bit: virtual public Raster3DAbstract, public Raster2D_4Bit {};
class Raster3D_8Bit: virtual public Raster3DAbstract, public Raster2D_8Bit {};
class Raster3D_16Bit: virtual public Raster3DAbstract, public Raster2D_16Bit {};
class Raster3D_24Bit: virtual public Raster3DAbstract, public Raster2D_24Bit {};
class Raster3D_32Bit: virtual public Raster3DAbstract, public Raster2D_32Bit {};
#if defined(uint64_t_defined)
class Raster3D_64Bit: virtual public Raster3DAbstract, public Raster2D_64Bit {};
#endif

class Raster3D_32FltBit:virtual public Raster3DAbstract, public Raster2D_32FltBit {};
class Raster3D_64FltBit:virtual public Raster3DAbstract, public Raster2D_64FltBit {};

#endif


#ifdef _MSC_VER 
 #pragma warning (pop)
#endif
#endif // _RAS_PROT_H_