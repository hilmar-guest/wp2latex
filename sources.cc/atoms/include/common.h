#ifndef __Common_H_INCLUDED
#define __Common_H_INCLUDED

/******** Fixing some special features of compilers **********
(c) 1997-2024 Jaroslav Fojtik
  if you don't use below mentionted compiler, please correct this items
  for your compiler and send me your correction to:
		JaFojtik@seznam.cz or JaFojtik@yandex.com

Outputs: 
  __Strict_Const_Procs		compiler is very touht on const arguments
  __string_already_used		the class string is used internally
  __Disable_NULL_printf		the printf("%s",NULL) crashes
  __Ellipsis_Expand_Short       ellipse ... convert all char and short arguments to int
  __Limited_Proc_Size		several old compilers have severe limitation of procedure code size <24kB
  __No_bool			Compiler do not support bool type
  NO_LVAL_CAST			Compiler cannot compile ((char *)ptr)++
  __FixNonTemplateFriend	Templates are supported on the obsolette basis, needed <> after operator
  __Mkdir_Has_1_arg		Mkdir is defined with 1 argument only
  explicit			for compilers that do not support this keyword redefine it to "private:"
*/


#define _HAVE_VSNPRINTF


#if defined(__unix__) || defined(__unix) || defined(unix)
  #ifndef __UNIX__
    #define __UNIX__
  #endif
#endif


#ifdef __BORLANDC__
  #define __Have_stricmp
  #define __Mkdir_Has_1_arg
  #define LO_ENDIAN
  #ifdef __OS2__
    #define __Strict_Const_Procs
    #define __string_already_used
  #endif
  #define __FixNonTemplateFriend	//Borland C++ has templates implemented wrongly
  #if __BORLANDC__ < 0x0500
    #define mutable			// Mutable is not supportted
    #define __Limited_Proc_Size
    #define __No_bool
//    #define __Safe_strchr
    #define __Safe_strstr
//    #define __Safe_strcmp
    #define __Safe_strlen
    #define explicit /* unsupported */
    #undef _HAVE_VSNPRINTF
  #endif
  #ifdef __WIN32__
    #define __Strict_Const_Procs
    #ifdef IN
      #undef IN
    #endif
    #define IN %
  #else
    typedef signed char  bool;
    #define true 1
    #define false 0
  #endif
#endif


#if defined(__EGC__) || defined(__GNUC__)
  #define __Have_strcasecmp
  #define PACKED  __attribute__((aligned(1))) __attribute__((packed))

  #if __GNUC__ >= 3
    #define Have_snprintf
    #define __StrictSingUnsign  
    #define __Ellipsis_Expand_Short    
    #define _has_MemStreams
    #if (__GNUC__ == 3 && __GNUC_MINOR__ >= 4) || (__GNUC__ > 3)
      #define __FixNonTemplateFriend
    #endif
    #if (__GNUC__ == 4 && __GNUC_MINOR__ >= 4) || (__GNUC__ > 4)
      #define __Strict_Const_Procs
    #endif
    #if __GNUC__ >= 4
      #define NO_LVAL_CAST
    #endif
  #else
    #undef _HAVE_VSNPRINTF
  #endif
  #ifndef __DJGPP__
    #ifndef __UNIX__
      #define __UNIX__
    #endif
  #else		//DJGPP
    #define __Have_stricmp
    #ifdef __UNIX__
      #undef __UNIX__
    #endif
  #endif
  #ifdef sun  
    #define __Disable_NULL_printf
  #endif
 #if defined(__MINGW32__) || defined(__MINGW64__)
  #define __Mkdir_Has_1_arg
 #endif
#endif


#ifndef __GNUC__
#ifdef __hpux
  #ifndef __HPUXC__
    #define __HPUXC__
  #endif
#endif


#ifdef hpux
  #ifndef __HPUXC__
    #define __HPUXC__
  #endif
#endif
#endif


#ifdef _MSC_VER
  #define __Have_stricmp
  #define __StrictSingUnsign
  #define __FixNonTemplateFriend
  #define __Mkdir_Has_1_arg
  #if _MSC_VER <= 800
    #define explicit private:
  #endif
  #if _MSC_VER >= 1400	
    #define __Strict_Const_Procs
  #endif
  #if _MSC_VER < 1500	/* Supported since MSVC 2008 */
    #define vsnprintf _vsnprintf
  #endif
  #define NO_LVAL_CAST
#endif


#ifdef __WATCOMC__
 #define __Have_stricmp
 #ifdef __MSDOS__
   #define __Mkdir_Has_1_arg
 #endif
#endif


#if defined(__IAR_SYSTEMS_ICC__) || defined(__ARMCC_VERSION)
 #define PACKED __packed
#endif


#ifndef PACKED
 #define	PACKED		/**< Default definition of packed - nothing. */
#endif


typedef enum
{
  StringsId = 0x100,
  SetsId =    0x200,
  ListsId =   0x300,
  MatrixId =  0x400,
  StackId =   0x500,
  IntervalId =0x600,
  RasterId =  0x700,
  DblListId = 0x800
} TComponentId;

#ifndef ERROR_HANDLER
  #define RaiseError(ErrNo,Instance)	{}; /**/
#else
 #ifdef __cplusplus
  extern "C" {
 #endif
 void RaiseError(int ErrNo, const void *Instance);
 #ifdef __cplusplus
  }
 #endif
#endif


#ifndef __Have_stricmp
 #ifdef __Have_strcasecmp
  #define stricmp strcasecmp
  #define strnicmp strncasecmp
 #endif
 #ifndef stricmp        //already fixed?
  #define stricmp strcmp
 #endif
#endif


#endif
