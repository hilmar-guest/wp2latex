/**************************************************
* unit:    strings           release 0.26         *
* purpose: test of functionality of unit string   *
* Licency: GPL                                    *
* Copyright: (c) 1998-2024 Jaroslav Fojtik        *
***************************************************/
#ifdef _MSC_VER
    #include <windows.h> 
#endif
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <math.h>

#define CurrentDir

#ifdef __BORLANDC__
#include <alloc.h>
#else
#define heapcheck() 1
#endif

#ifdef Streams
#include <iostream.h>
#endif

#if defined(__EGC__) || defined(__GNUC__)
 #if (__GNUC__ >= 5) || (__GNUC__ == 4 && __GNUC_MINOR__ >= 8)
  #include "../test/itoa.c"
 #endif
#endif


#include "stringa.h"

void Benchmark(void);

#ifdef ERROR_HANDLER
 void RaiseError(int ErrNo, const void *Instance)
     {
     fprintf(stderr,"Strings::Error %d (%p)\n",ErrNo,Instance);
     }
#endif


//#include<libintl.h>
//#include "regexp.h"

char BigArray[] =
   "00000000000000000000000000000000000000000000000000000000000000000000"
   "11111111111111111111111111111111111111111111111111111111111111111111"
   "22222222222222222222222222222222222222222222222222222222222222222222"
   "33333333333333333333333333333333333333333333333333333333333333333333"
   "44444444444444444444444444444444444444444444444444444444444444444444"
   "55555555555555555555555555555555555555555555555555555555555555555555"
   "66666666666666666666666666666666666666666666666666666666666666666666"
   "77777777777777777777777777777777777777777777777777777777777777777777"
   "88888888888888888888888888888888888888888888888888888888888888888888"
   "99999999999999999999999999999999999999999999999999999999999999999999";


class NewString:string
{
};


int test1(string s)
{
 return(check(s));
}


/*
temp_string akcel(void)
{
  return temp_string("Ahoj");
}

temp_string akce2(void)
{
  string s("Ahoj svete");
  return temp_string(s);
}

string akce3(void)
{
  string s("Ahoj svete");
  return s;
}


void pokus(void)
{
  string s = akcel();
  s = akce2();
  s = akce3();
}
*/

int main(int argc, char *argv[])
{
string s0,s1,s2,s3,s4;
char *ch;
int i,j;
unsigned u;
char c;
//string *ps=new string[10]("ahoj");
int Ops;


 puts("");
 puts("<<<strings>>> (c)1998-2024 Jaroslav Fojtik");
 printf("              This program tests the string unit, release %d.%d\n",
	STRINGS_VERSION>>8, STRINGS_VERSION&0xFF);

 //pokus();

 if(argc>1)
    {
    Ops = 0;
    for(i=1; i<argc; i++)
      {
      if(!strcmp(argv[i],"-bench")) Ops|=3;
      if(!strcmp(argv[i],"-demo")) Ops|=4;
      if(!strcmp(argv[i],"-test")) Ops|=1;
      if(!strcmp(argv[i],"-help") || !strcmp(argv[i],"/help"))
        {
        printf("Command line parameters:\n-bench -demo -test -help\n");
        return 1;
        }
      }
    }
 else Ops=3;


//Benchmark();

/*printf(" %ld ",coreleft()); 


for(i=1; i<32000; i++)
        s0+="Ahoj Svete";
s0="";
printf(" %ld ",coreleft());*/

//////////////////////////////////
if(Ops&1)
{
puts(" copy operator =");

//Zero string
s1="";
i=check(s1);
if(i!=0) goto Abnormal_End;

//char
s2='?';
i=check(s2);
if(i!=0) goto Abnormal_End;

ch=s2();

//Short string
s3="123, 456, 789";
i=check(s3);
if(i!=0) goto Abnormal_End;

//Zero string after Short string
s3="";
i=check(s3);
if(i!=0) goto Abnormal_End;
if(s3!="") goto Abnormal_End;


//Long string
s4=BigArray;
i=check(s4);
if(i!=0) goto Abnormal_End;

s3=string(BigArray);
if(s3!=s4)
        {
        i=-1;
        goto Abnormal_End;
        }
i=check(s3);
if(i!=0) goto Abnormal_End;
        
s3=s2;
s3=s3;
if(*s3()!=*s2())
        {
        i=-2;
        goto Abnormal_End;
        }



//////////////////////////////////
puts(" comparing operator ==  !=");
s3="123, 456, 789";

//Zero string
i = s1=="";
if(!i)  {
        i = -10;
        goto Abnormal_End;
        }
if(s1!="")
       {
       i = -11;
       goto Abnormal_End;
       }

i = s1==string();
if(!i)  {
        i = -12;
        goto Abnormal_End;
        }
if(s1!=string())
       {
       i = -13;
       goto Abnormal_End;
       }

i = string()==s1;
if(!i)  {
        i = -14;
        goto Abnormal_End;
        }
if(string()!=s1)
       {
       i = -15;
       goto Abnormal_End;
       }

//Character
i = s2=='?';
if(!i)  {
        i=-10;
        goto Abnormal_End;
        }
if(s2!='?')
        {
        i=-12;
        goto Abnormal_End;
        }
//Short string
i = (s3=="123, 456, 789");
if(!i)  {
        i=-10;
        goto Abnormal_End;
        }
if(s3!="123, 456, 789")
        {
        i=-12;
        goto Abnormal_End;
        }

if("123, 456, 789"!=s3)
        {
        i=-13;
        goto Abnormal_End;
        }
i = ("123, 456, 789"==s3);
if(!i)  {
        i=-14;
        goto Abnormal_End;
        }
        

i= s4==BigArray;
if(!i) {
       i=-15;
       goto Abnormal_End;
       }
i=check(s4);
if(i!=0) goto Abnormal_End;
i=check(s3);
if(i!=0) goto Abnormal_End;

s2='d';
if(s2!='d')
      {
      i=-16;
      goto Abnormal_End;
      }
if('d'!=s2)
      {
      i=-16;
      goto Abnormal_End;
      }


if(s2>'d' || s2<'d' || 'd'<s2 || 'd'>s2)
      {
      i=-17;
      goto Abnormal_End;
      }

if('c'>=s2 || 'e'<=s2)
      {
      i=-18;
      goto Abnormal_End;
      }

if(!('c'<=s2 && 'e'>=s2))
      {
      i=-19;
      goto Abnormal_End;
      }

i=(s2<='e');
i=s2<='b';
i=s2<='a';
if((s2<='b') )
      {
      i=-19;
      goto Abnormal_End;
      }



//////////////////////////////////
puts(" calling procedures with string argument");
ch=NULL;

//Zero strings
s1+="";
i=test1(s1);
if(i!=0)
       {
       i=-20;
       goto Abnormal_End;
       }
i=test1(s2);
if(i!=0)
       {
       i=-21;
       goto Abnormal_End;
       }
i=test1(s3);
if(i!=0)
       {
       i=-21;
       goto Abnormal_End;
       }
i=test1(s4);
if(i!=0)
       {
       i=-23;
       goto Abnormal_End;
       }



/////////////////////////////////////////
puts(" string concatenation operator + +=");
ch=NULL;

//Zero strings
s1="";
s2='?';
s1+="";
if(s1!="")
       {
       i=-30;
       goto Abnormal_End;
       }
s1=s1+"";
if((s1!="")||(length(s1)!=0))
       {
       i=-31;
       goto Abnormal_End;
       }
i=check(s1);
if(i!=0) goto Abnormal_End;

s1=s1+ch;
if((s1!="")||(length(s1)!=0))
       {
       i=-32;
       goto Abnormal_End;
       }
i=check(s1);
if(i!=0) goto Abnormal_End;

s1=ch+s1;
if((s1!="")||(length(s1)!=0))
       {
       i=-33;
       goto Abnormal_End;
       }
i=check(s1);
if(i!=0) goto Abnormal_End;


s1=string();
s1+=s2;
if((s1!="?")||(length(s1)!=1))
       {
       i=-34;
       goto Abnormal_End;
       }
i=check(s1);
if(i!=0) goto Abnormal_End;
s1=s0;

//Character
s2+="";
if(s2!='?')
	{
	i=-35;
	goto Abnormal_End;
	}
s2+='^';
if(s2!="?^")
	{
	i=-36;
	goto Abnormal_End;
	}
i=check(s2);
if(i!=0) goto Abnormal_End;


//Short string
s3+=s2+s1;
if(s3!="123, 456, 789?^")
	{
	i=-37;
	goto Abnormal_End;
	}
i=check(s3);
if(i!=0) goto Abnormal_End;
s3=s3+'&';
if(s3!="123, 456, 789?^&")
	{
	i=-38;
	goto Abnormal_End;
	}
i=check(s3);
if(i!=0) goto Abnormal_End;

s3='@'+s3;
if(s3!="@123, 456, 789?^&")
	{
	i=-39;
	goto Abnormal_End;
	}
i=check(s3);
if(i!=0) goto Abnormal_End;

s3.erase();
s3='@'+s3;
if(s3!='@')
	{
	i=-40;
	goto Abnormal_End;
	}
i = check(s3);
if(i!=0) goto Abnormal_End;

s3.erase();
s3=s3+'!';
if(s3!='!')
	{
	i=-41;
	goto Abnormal_End;
	}
i=check(s3);
if(i!=0) goto Abnormal_End;

s4 = (s2+s4)+s2;
s3="?^"
   "00000000000000000000000000000000000000000000000000000000000000000000"
   "11111111111111111111111111111111111111111111111111111111111111111111"
   "22222222222222222222222222222222222222222222222222222222222222222222"
   "33333333333333333333333333333333333333333333333333333333333333333333"
   "44444444444444444444444444444444444444444444444444444444444444444444"
   "55555555555555555555555555555555555555555555555555555555555555555555"
   "66666666666666666666666666666666666666666666666666666666666666666666"
   "77777777777777777777777777777777777777777777777777777777777777777777"
   "88888888888888888888888888888888888888888888888888888888888888888888"
   "99999999999999999999999999999999999999999999999999999999999999999999"
   "?^";
if(s4!=s3)
       {
       printf("\n%s\n",s4());
       i=-42;
       goto Abnormal_End;
       }
i=check(s4);
if(i!=0) goto Abnormal_End;

s4="";
for(u=1; u<=255; u++)
	{
	s4+=(unsigned char)u;
	if(length(s4)!=u)
		{
		i=-43;
		goto Abnormal_End;
		}
       if(s4.length()!=u)
		{
		i=-44;
		goto Abnormal_End;
		}
	}
for(u=1; u<=255; u++)
	{
	if((unsigned char)s4[u-1]!=u)
		{
		i=-45;
		goto Abnormal_End;
		}
	}

c=0;
s4=c+s2;
if(s4!="?^")
	{
	i=-46;
	goto Abnormal_End;
	}
i=check(s4);
if(i!=0) goto Abnormal_End;


//////////////////////////////////
puts(" indexing arrays  []");
s1=string();
s4="?^"+string(BigArray)+"?^";

for(j=0;j<10;j++)
        {
        if(s1[j]!=0)
		{
                i=-50;
                goto Abnormal_End;
                }
	}

for(u=0; u<=length(s4); u++)
        {
        if(s4[u]!=s3()[u])
                {
		i=-51;
                goto Abnormal_End;
                }
        }
for(u=length(s4); u<2*length(s4); u++)
        {
	if(s4[u]!=0)
                {
                i=-52;
                goto Abnormal_End;
                }
        }

//////////////////////////////////
puts(" operator IN");
s1="00000000000000000000000000000000000000000000000000000000000000000000";
if(!(s1 IN s4))
        {
	i=-60;
        goto Abnormal_End;
        }
s1+='1';
if(!(s1 IN s4))
        {
        i=-61;
        goto Abnormal_End;
        }
s1="00"+s1;
if(s1 IN s4)
        {
	i=-62;
        goto Abnormal_End;
        }


s1="";

//////////////////////////////////
puts(" generating substrings: function copy");

erase(s2);
s2=copy(s2,0,5);
if(s2!="")
        {
        i=-70;
        goto Abnormal_End;
        }

if(copy(s1,0,10)!="")
        {
        i=-70;
        goto Abnormal_End;
        }

while(s3[0]!='1')
        {
        s3=copy(s3,1,length(s3));

	i=check(s3);
        if(i!=0) goto Abnormal_End;
        }
s3="?^00000000000000000000000000000000000000000000000000000000000000000000"+s3;
if(s3!=s4)
        {
        i=-71;
        goto Abnormal_End;
	}


s3=copy(s3,2,length(s4)-2);
while(s3[length(s3)-1]!='8')
        {
        s3=copy(s3,0,length(s3)-1);

	i=check(s3);
        if(i!=0) goto Abnormal_End;
        }
if(s3!=
   "00000000000000000000000000000000000000000000000000000000000000000000"
   "11111111111111111111111111111111111111111111111111111111111111111111"
   "22222222222222222222222222222222222222222222222222222222222222222222"
   "33333333333333333333333333333333333333333333333333333333333333333333"
   "44444444444444444444444444444444444444444444444444444444444444444444"
   "55555555555555555555555555555555555555555555555555555555555555555555"
   "66666666666666666666666666666666666666666666666666666666666666666666"
   "77777777777777777777777777777777777777777777777777777777777777777777"
   "88888888888888888888888888888888888888888888888888888888888888888888")
        {
        i=-72;
        goto Abnormal_End;
	}
s4=copy(s4,2,length(s4)-4);
if((string)(s3+"99999999999999999999999999999999999999999999999999999999999999999999")!=s4)
        {
        i=-73;
        goto Abnormal_End;
        }
i=check(s4);
if(i!=0) goto Abnormal_End;


///////////////////////////////////////////////////
puts(" functions insert, delete and trim");

s2=s4="Hello Word Hello Word Hello Word Hello Word";
s3=del(s4,6,4);

if(s3!=del(s3,0,0))
        {
        i=-81;
        goto Abnormal_End;
        }
if(s3!=del(s3,3,0))
        {
	i=-81;
        goto Abnormal_End;
        }
s4=s3;

u = length(s3);
while(s3!="")
        {
        s3 = del(s3,0,1);
        u--;
        if(u!=length(s3) || u>0xFFFC)
          {
	  i=-82;
          goto Abnormal_End;
          }
        }

s3=insert(s4,"Word",6);
if(s3 != s2 )
        {
        i=-83;
        goto Abnormal_End;
        }

s3=insert(s4,"X",0);
s2="X"+s4;
if(s3 != s2 )
        {
        i=-84;
        goto Abnormal_End;
        }

s3=insert(s4,"Y",length(s4));
s4+="Y";
if(s3 != s4 )
        {
	i=-85;
        goto Abnormal_End;
        }


s2="  \n  ";
s1=s4+s2;
s1="  "+s1;
s3=trim(s1);
if(s3!=s4)
        {
        i=-86; goto Abnormal_End;
	}
s1.trim();
if(s1!=s4)
        {
        i=-87; goto Abnormal_End;
        }

s3=trim(s2);
s2.trim();
if(length(s2)>0 || length(s3)>0)
        {
        i=-88; goto Abnormal_End;
	}

s2=trim(string("  \t    n  "));
if(s2!='n')
        {
        i=-89; goto Abnormal_End;
        }




///////////////////////////////////////////////////
puts(" replacement of substrings");
s2=s4="Hello Word Hello Word Hello Word Hello Word";
s3=replacesubstring(s4, "Hello", "Ahoj" );
s3=replacesubstring(s3, "Word", "Sv�te" );

if(s3!="Ahoj Sv�te Ahoj Sv�te Ahoj Sv�te Ahoj Sv�te" || s3==s2)
        {
        i=-91;
        goto Abnormal_End;
        }
s3=replacesubstring(s3, "Ahoj", "Hello" );
s3=replacesubstring(s3, "Sv�te", "Word" );

if(s3!=s2)
        {
        i=-92;
        goto Abnormal_End;
        }

for(j=1;j<255;j++) s2+=(char)j;

erase(s4);
s4 = s2;
s4 = ToUpper(s4);
for(u=0; u<=length(s4); u++)
	{
	if(s4[u] != (char)toupper((unsigned char)s2()[u]))
		{
		printf("Toupper do not work properly j=%d \n s4=%s \n\r s2=%s\n",s4[u],s4(),s2());
                i = -93;
                goto Abnormal_End;
                }
        }

s4.ToLower();
for(u=0; u<=length(s4); u++)
        {
        if(s4[u]!=(char)tolower((unsigned char)s2()[u]))
                {
                i=-94;
                goto Abnormal_End;
                }
        }

erase(s3);
s3=ToLower(s2);
if(s4!=s3)
	{
	i=-95;
        goto Abnormal_End;
        }

s3.ToUpper();
for(u=0; u<=length(s4); u++)
        {
        if((char)toupper((unsigned char)s4[u])!=s3[u])
                {
                i=-96;
                goto Abnormal_End;
		}
        }



///////////////////////////////////////////////////
puts(" operators * *=");

erase(s0);
s2="Zello";
s4="Hello";
c=*s4;
if(*s2!='Z' || c!='H')
        {
        i=-100;
        goto Abnormal_End;
        }
*s2='H';
if(s2!=s4)
        {
        i=-101;
        goto Abnormal_End;
        }
if(*s0!=0)
	{
        i=-102;
        goto Abnormal_End;
        }

s3 = s2*2;
s1 = s2+s2;
if(s1!=s3)
        {
        i=-103;
        goto Abnormal_End;
        }
s1 = s2+s3*2;
s4 *= 5;
if(s1!=s4)
        {
        i=-104;
        goto Abnormal_End;
        }

///////////////////////////////////////////////////
puts(" printer");

const int iValues[] = {-32768, -22000, -2345, -954, -71, -8, -2, 0,
		       1, 3, 9, 17, 94, 562, 1067, 19876, 32767};
char buff[32];
for(j=0; j<sizeof(iValues)/sizeof(int); j++)
	{
	erase(s2);

	itoa(iValues[j],buff,10);
	s1.printf("%d", iValues[j]);
	if(s1!=buff)
	  {
          printf("Defective string::printf(%d), value obtained '%s', expected '%s'!\n", iValues[j], s1(), buff);
	  i = -110;
	  goto Abnormal_End;
	  }
	s2.printf("%d", iValues[j]);
	if(s2!=buff)
	  {
          printf("Defective string::printf(%d), value obtained '%s', expected '%s'!\n", iValues[j], s2(), buff);
	  i = -111;
	  goto Abnormal_End;
	  }

	if(s1.length()!=strlen(buff))
	  {
	  i = -112;
	  goto Abnormal_End;
	  }
	if(s2.length()!=strlen(buff))
	  {
	  i = -113;
	  goto Abnormal_End;
	  }

	if(s1.check()<0)
	  {
	  i = -114;
	  goto Abnormal_End;
	  }
	if(s2.check()<0)
	  {
	  i = -115;
	  goto Abnormal_End;
	  }
	}

for(char ch='1'; ch<='z'; ch++)
	{
	s2.erase();
	s1.printf("%c", ch);
	if(s1!=ch)
	  {
	  i = -116;
	  goto Abnormal_End;
	  }
	if(s1.check()<0)
	  {
	  i = -117;
	  goto Abnormal_End;
	  }

	s2.printf("%c", ch);
	if(s2!=ch)
	  {
	  i = -118;
	  goto Abnormal_End;
	  }
	if(s2.check()<0)
	  {
	  i = -119;
	  goto Abnormal_End;
	  }
	}

s1.printf("%s","<X>");
for(char ch2='0'; ch2<='9'; ch2++)
	s1.cat_printf("%c",ch2);

if(s1!="<X>0123456789")
	{
	i = -120;
	goto Abnormal_End;
	}
if(s1.check()<0)
	{
	i = -121;
	goto Abnormal_End;
	}

const float FltVal = 9876.5432;
s1.printf("%.4f",9876.5432);
const char *FltValS = "9876.5432";
if(s1!=FltValS)
	{
	printf("Defective string::printf(%f), value obtained '%s', expected '%s'!\n", FltVal, s1(), FltValS);
	i = -123;
	goto Abnormal_End;
	}



//cout<<s3;
s2="Ahoj";
s1="~";
s1<<"Svete"<<" co je "<< 15 < '?';

}  // end of testing


if(i==0 && (Ops & 2)) Benchmark();



//////////////////////////
{
//RegExp Reg("ahoj");

s1="ahoj";
//printf("%d",s1==Reg);

}
//////



i=0;
Abnormal_End:
if(heapcheck()<=0)
         {
         printf("Bad allocation %d",heapcheck());
         return(-1);
         }
if(i!=0) {
         printf("Unit Error %d", i);
         return(-1);
         }
puts(" .............. Test passed OK ..............");
return(0);
}


#include "../test/bench.h"


void Addition(long i)
{
string s1,s2,s3,s4,s5;

while(i-->0)
        {
        s1='!';
        s2="ahoj svete";
        s4=s2+"???";
        s4+='!';
        s3=s2+s1;
        s3=s3+'1';
        s1+=s2;
        s1+="ABCDEFGH";
        s5=s1+s2+s3+s4;
        }
}


void Copy(long i)
{
string s1,s2,s3,s4,s5;

s1=" aBcDeFgHiJkLmNoP ... 1234567891234567890 ";
while(i-->0)
        {
        s2=copy(s1,0,4);
        s3=copy(s3,5,4);
        s3=copy(s1,5,4);
        s4=copy(s1,5,20);
        s5=copy(copy(s1,1,20),2,3);
        }
}


void Replace(long i)
{
string s1,s2,s3,s4,s5;

while(i-->0)
        {
        s1=replacesubstring("Ahoj ty muj m. svete","m.","maly");
        s2="ty";

        s3=replacesubstring(s1,s2,"tyty");
        s4=replacesubstring(s3,s2,"tam je ");
        s4=replacesubstring(s4,"to tam neni","tytyt");
        }
}


void UpLoSpaces(long i)
{
string s1,s2,s3,s4,s5;

s1=" aBcDeFgHiJkLmNoP ... 1234567891234567890 ";
s2="                     ";
while(i-->0)
        {
        s3=ToUpper(s1);
        s4=ToLower(s1);
        s3.ToLower();
        s5=ToUpper(ToLower(s1));
        s3=trim(s3);
        s3=trim(s2);
        }
}


void Compare(long i)
{
string s1,s2,s3,s4,s5;
long q = 0;

s1="abc";
s2="cba";
s3="bbb";

while(i-->0)
        {
        if(s1<s2) q++;
        if(s1<=s3) q++;
        if(s1>s2) q++;
        if(s1>=s3) q++;
        if(s1==s2) q++;
        if(s1!=s3) q++;

        if(s1<"aaa") q++;
        if(s1<="aaa") q++;
        if(s1>"aaa") q++;
        if(s1>="aaa") q++;
        if(s1=="bbb") q++;
        if(s1!="bbb") q++;

        if("aaa"<s1) q++;
        if("aaa"<=s1) q++;
        if("bbb">s1) q++;
        if("bbb">=s1) q++;
        if("ccc"==s2) q++;
        if("ddd"!=s1) q++;

        if('a'<s1) q++;
        if('a'<=s1) q++;
        if('b'>s1) q++;
        if('b'>=s1) q++;
        if('c'==s2) q++;
        if('d'!=s1) q++;

        if(s1<'e') q++;
        if(s1<='f') q++;
        if(s1>'g') q++;
        if(s1>='h') q++;
        if(s2=='c') q++;
        if(s2!='d') q++;
        }
}


#ifndef __BORLANDC__
void Erase(long i)
{

 while(i-->0)
     {
       {
       string s1((unsigned int)1025), s2((unsigned int)16);
       s1.erase();
       s2.erase();
       }
     }
}

void EraseAssign(long i)
{

 while(i-->0)
     {
       {
       string s1((unsigned int)1025), s2((unsigned int)16);
       s1 = "";
       s2 = "";
       }
     }
}
#endif


void Multiply(long i)
{
const string s1("Ahoj");
string s2;

 while(i-->0)
	{
	s2 = s1 * 5;
	s2 *= 3;
	}
}


void StrPrintf(long i)
{
string s1;
string s2;

 while(i-->0)
	{
	s1.printf("%d %u %c", (int)i, (unsigned)i, i & 0xFF);
	s2.printf("%s %.1f", s1(), i/10.0);
	}
}


static char c = 'A';

string GenerString1(void)
{
  string s(c);
  c++;
  if(c>'Z') c='A';

return s;
}

void StrReturnArg(long i)
{
string s1;

 while(i-->0)
	{
        s1 = GenerString1();
	}
}


temp_string GenerString2(void)
{
  string s(c);
  c++;
  if(c>'Z') c='A';

return temp_string(s);
}

void StrReturnTArg(long i)
{
string s1;

 while(i-->0)
	{
        s1 = GenerString2();
	}
}


temp_string GenerString3(void)
{
  string s(c);
  c++;
  if(c>'Z') c='A';

return temp_string(s);
}

void StrReturnT2Arg(long i)
{
temp_string s1;

 while(i-->0)
	{
        s1 = GenerString3();
	}
}



string GenerString1L(void)
{
  string s(BigArray);
return s;
}

void StrReturnArgL(long i)
{
string s1;

 while(i-->0)
	{
        s1 = GenerString1L();
	}
}


temp_string GenerString2L(void)
{
  string s(BigArray);  

return temp_string(s);
}

void StrReturnTArgL(long i)
{
string s1;

 while(i-->0)
	{
        s1 = GenerString2L();
	}
}


temp_string GenerString3L(void)
{
  string s(BigArray);

return temp_string(s);
}

void StrReturnT2ArgL(long i)
{
temp_string s1;

 while(i-->0)
	{
        s1 = GenerString3L();
	}
}


void StrIsEmpty(long i)
{
string s1("Hello");
string s2;
volatile bool b;

 while(i-->0)
	{
        b = s1.isEmpty();
	b = s2.isEmpty();
	}
}


void StrCompEmpty(long i)
{
string s1("Hello");
string s2;
volatile bool b;

 while(i-->0)
	{
        b = s1 == "";
	b = s2 == "";
	}
}



/* void StrPrintfOLD(long i)
{
char BUFF[1024];
string s1;
string s2;

 while(i-->0)
	{
	sprintf(BUFF,"%d %u %c", (int)i, (unsigned)i, i & 0xFF);
        s1=BUFF;
	sprintf(BUFF,"%s %.1f", s1(), i/10.0);
        s2=BUFF;
	}
} */


void Benchmark(void)
{
float f, f2;

 puts(" .............. Benchmarks ..............");

#ifndef __BORLANDC__
 f = AutoMeasure(Erase);
 printf(" Create & erase strings: %fus\n",f);
 f = AutoMeasure(EraseAssign);
 printf(" Create & erase assign strings: %fus\n",f);
#endif

 f = AutoMeasure(Compare);
 printf(" Compare strings: %fus\n",f);

 f = AutoMeasure(Addition);
 printf(" Addition loop time: %fus\n",f);

 f = AutoMeasure(Copy);
 printf(" Copy loop time: %fus\n",f);

 f = AutoMeasure(Replace);
 printf(" Replace loop time: %fus\n",f);

 f = AutoMeasure(UpLoSpaces);
 printf(" Toupper & Tolower loop time: %fus\n",f);

 f = AutoMeasure(Multiply);
 printf(" Multiply loop time: %fus\n",f);

 f = AutoMeasure(StrPrintf);
 printf(" Printf to class string: %fus\n",f);
// f = AutoMeasure(StrPrintfOLD);
// printf(" Printf to class string OLD: %fus\n",f);

 f = AutoMeasure(StrIsEmpty)/2.0;
 printf(" IsEmpty check: %fus\n",f);
 f = AutoMeasure(StrCompEmpty)/2.0;
 printf(" Compare Empty check: %fus\n",f);

 f = AutoMeasure(StrReturnArg);
 f2 = AutoMeasure(StrReturnArgL);
 printf(" Function string=string proc(): 1B:%fus %uB:%fus\n", f, (unsigned)strlen(BigArray),f2);
 f = AutoMeasure(StrReturnTArg);
 f2 = AutoMeasure(StrReturnTArgL);
 printf(" Function string=temp_string proc(): 1B:%fus %uB:%fus\n", f, (unsigned)strlen(BigArray),f2);
 f = AutoMeasure(StrReturnT2Arg);
 f2 = AutoMeasure(StrReturnT2ArgL);
 printf(" Function temp_string=temp_string proc(): 1B:%fus %uB:%fus\n", f, (unsigned)strlen(BigArray),f2);

}
