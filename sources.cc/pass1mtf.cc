/******************************************************************************
 * program:     wp2latex                                                      *
 * function:    conversion from MTEF to LaTeX                                 *
 * modul:       pass1mtf.cc                                                   *
 * description: Module for conversion Math Type Equations that are embeded    *
 *              inside RTF or Word documents.                                 *
 *              MTEF is Math Type Equation Format                             *
 * licency:     GPL		                                              *
 ******************************************************************************/
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include <ctype.h>

#include "sets.h"
#include "lists.h"
#include "stringa.h"
#include "struct.h"

#ifdef __UNIX__
 #include <unistd.h>
#endif


#include "wp2latex.h"
#include "cp_lib/cptran.h"
#include "raster.h"
#include "cp_lib/out_dir/mtef.trn"


typedef struct
     {
     uint16_t dx;
     uint16_t dy;
     } Tnudge;

class TconvertedPass1_MTF:public TconvertedPass1
     {
public:
     string Formula;
     uint16_t Version;
     uint8_t MTEF_Version;
     Tnudge nudge;

     virtual int Convert_first_pass(void);
     };

/*Register translators here*/
TconvertedPass1 *Factory_MTEF(void) {return new TconvertedPass1_MTF;}
FFormatTranslator FormatMTEF("MTEF",&Factory_MTEF);


#define MtefO_NUDGE		0x8

#define MtefO_CHAR_EMBELL	0x1
#define MtefO_CHAR_FUNC_START	0x2
#define MtefO_CHAR_ENC_CHAR_8	0x4
#define MtefO_CHAR_ENC_CHAR_16	0x10
#define MtefO_CHAR_ENC_NO_MTCODE 0x20

#define MtefO_LINE_NULL		0x1
#define MtefO_LINE_LSPACE	0x2

#define MtefO_LP_RULLER		0x2

#define MtefO_COLOR_CMYK	0x1
#define MtefO_COLOR_SPOT	0x2
#define MtefO_COLOR_NAME	0x3


void Interpret(TconvertedPass1 *cq, string & strB, string & strE, const char *seq, const char *symbol, const string arg[]);
void ProcessKeyMTEFSymbol(TconvertedPass1_MTF *cq);

static const char* GetTemplate_MTEF(int Selector, int Variation,int Version=3)
{
switch(Selector)
  {
  case 0:switch(Variation)
	   {
	   case 0:return("\\left\\langle 1\\right\\rangle !2");	//0.0=fence: angle-both
	   case 1:return("\\left\\langle 1\\right !2");		//0.1=fence: angle-left only
	   case 2:return("\\left. 1\\right\\rangle !2");	//0.2=fence: angle-right only
	   }
	 break;
  case 1:switch(Variation)
	   {
	   case 0:return("\\left( 1\\right) !2");	//1.0=fence: paren-both,
	   case 1:return("\\left( 1\\right. !2");	//1.1=fence: paren-left only
	   case 2:return("\\left. 1\\right) !2");	//1.2=fence: paren-right only
	   }
	 break;
  case 2:switch(Variation)
	   {
	   case 0:return("\\left\\{ 1\\right\\} !2");	//2.0=fence: brace-both,
	   case 1:return("\\left\\{ 1\\right. !2");	//2.1=fence: brace-left only
	   case 2:return("\\left. 1\\right\\} !2");	//2.2=fence: brace-right only
	   }
	 break;
  case 3:switch(Variation)
	   {
	   case 0:return("\\left[ 1\\right] !2");	//3.0=fence: brack-both
	   case 1:return("\\left[ 1\\right. !2");	//3.1=fence: brack-left only
	   case 2:return("\\left. 1\\right] !2");	//3.2=fence: brack-right only
	   }
	 break;
  case 4:switch(Variation)
	   {
	   case 0:return("\\left| 1\\right| !2");	//4.0=fence: bar-both
	   case 1:return("\\left| 1\\right. !2");	//4.1=fence: bar-left only
	   case 2:return("\\left. 1\\right| !2");	//4.2=fence: bar-right only
	   }
	 break;
  case 5:switch(Variation)
	   {
	   case 0:return("\\left\\| 1\\right\\| !2");	//5.0=fence: dbar-both
	   case 1:return("\\left\\| 1\\right. !2");	//5.1=fence: dbar-left only
	   case 2:return("\\left. 1\\right\\| !2");	//5.2=fence: dbar-right only
	   }
	 break;
  case 6:switch(Variation)
	   {
	   case 0:return("\\left\\lfloor 1\\right\\rfloor !2");	//6.0=fence: floor
	   }
	 break;
  case 7:switch(Variation)
	   {
	   case 0:return("\\left\\lceil 1\\right\\rceil !2");	//7.0=fence: ceiling
	   }
	 break;
  case 8:switch(Variation)
	   {
	   case 0:return("\\left[ 1\\right[ !2");	//8.0=fence: LBLB
	   }
	 break;
  case 9:switch(Variation)
	   {
	   case 0:return("\\left] 1\\right] !2");	//9.0=fence: RBRB
	   }
	  break;
  case 10:switch(Variation)
	   {
	   case 0:return("\\left] 1\\right[ !2");	//10.0=fence: RBLB
	   }
	  break;
  case 11:switch(Variation)
	   {
	   case 0:return("\\left[ 1\\right) !2");	//11.0=fence: LBRP
	   }
	  break;
  case 12:switch(Variation)
	   {
	   case 0:return("\\left( 1\\right] !2");	//12.0=fence: LPRB
	   }
	  break;
  case 13:switch(Variation)
	   {
	   case 0:return("\\sqrt{1} ");			//13.0=root: sqroot
	   case 1:return("\\sqrt[2]{1} ");		//13.1=root: nthroot
	   }
	  break;
  case 14:switch(Variation)
	   {
	   case 0:return("\\frac{\\hbox{\\Large 1}}{\\hbox{\\Large 2}} "); //14.0=fract: ffract
	   case 1:return("\\frac{1}{2} ");		//14.1=fract: pfract
	   }
	  break;
  case 15:switch(Variation)
	   {
	   case 0:if(Version>=3) return("0^{2}1 ");	//15.0=script: super
		  return("{}^{1} ");
	   case 1:if(Version>=3) return("0_{1}2 ");	//15.1=script: sub
		  return("{}_{1} ");
	   case 2:return("0_{1}^{2} ");			//15.2=script: subsup
	   }
	  break;
  case 16:switch(Variation)
	   {
	   case 0:return("\\underline{1} ");		//16.0=ubar
	   case 1:return("\\underline{\\underline{1}} ");//16.1=ubar: dubar
	   }
	  break;
  case 17:switch(Variation)
	   {
	   case 0:return("\\overline{1} ");		//17.0=obar: sobar
	   case 1:return("\\overline{\\overline{1}} "); //17.1=obar: dobar
	   }
	  break;
  case 18:switch(Variation)
	    {
            case 0:return("\\stackrel{1}{\\longleftarrow} !2"); //18.0=larrow: box on top
	    case 1:return("\\mathrel{\\mathop{\\kern@0pt\\longleftarrow}\\limits_{1}} !2");//18.1=larrow: box below
            }
	  break;
  case 19:switch(Variation)
	    {
	    case 0:return("\\stackrel{1}{\\longrightarrow} !2");//19.0=rarrow: box on top
	    case 1:return("\\mathrel{\\mathop{\\kern@0pt\\longrightarrow}\\limits_{1}} !2");//19.1=rarrow: box below
            }
	  break;
  case 20:switch(Variation)
	   {
	   case 0:return("{\\buildrel {1}\\over\\longleftrightarrow} !2");  //20.0=barrow: box on top
	   case 1:return("\\mathrel{\\mathop{\\kern@0pt\\longleftrightarrow}\\limits_{1}} !2"); //20.1=barrow: box below
	   }
  case 21:switch(Variation)
	   {
	   case 0:return("\\int 1 !2");			//21.0=integrals: single - no limits
	   case 1:return("\\int\\nolimits_{2} 1 !3");	//21.1=integrals: single - lower only
	   case 2:return("\\int\\nolimits_{2}^{3} 1 !4"); //21.2=integrals: single - both,
	   case 3:return("\\oint 1 !2");		//21.3=integrals: contour - no limits
	   case 4:return("\\oint\\nolimits_{2} 1 !3");	//21.4=integrals: contour - lower only
	   }
	  break;
  case 22:if(Wasy>=STYLE_NOTUSED)
            {
	    switch(Variation)
	      {
	      case 0:Wasy=true;return("\\iint 1 !2");		    //22.0=integrals: double - no limits
  	      case 1:Wasy=true;return("\\iint\\nolimits_{2} 1 !3"); //22.1=integrals: double - lower only
	      case 2:Wasy=true;return("\\oiint 1 !2");		    //22.2=integrals: area - no limits
	      case 3:Wasy=true;return("\\oiint\\nolimits_{2} 1 !3");//22.3=integrals: area - lower only
              }
	    }
	  switch(Variation)
	    {
  	    case 0:return("\\int\\int 1 !2");		    //22.0=integrals: double - no limits
  	    case 1:return("\\int\\int\\nolimits_{2} 1 !3"); //22.1=integrals: double - lower only
	    case 2:return("\\int\\int 1 ");		    //22.2=integrals: area - no limits
	    case 3:return("\\int\\int\\nolimits_{2} 1 !3"); //22.3=integrals: area - lower only
            }
	  break;
  case 23:if(Wasy>=STYLE_NOTUSED)
            {
            switch(Variation)
	      {
	      case 0:Wasy=true;return("\\iiint 1 !2");		   //23.0=integrals: triple - no limits
	      case 1:Wasy=true;return("\\iiint\\nolimits_{2} 1 !3"); //23.1=integrals: triple - lower only
	      case 2:Wasy=true;return("\\oiiint 1 ");		   //23.2=integrals: volume - no limits
	      case 3:Wasy=true;return("\\oiiint\\nolimits_{2} 1 !3");//23.3=integrals: volume - lower only
              }
	    }
	  switch(Variation)
	    {
	    case 0:return("\\int\\int\\int 1 ");		//23.0=integrals: triple - no limits
	    case 1:return("\\int\\int\\int\\nolimits_{2} 1 !3");//23.1=integrals: triple - lower only
	    case 2:return("\\int\\int\\int 1 ");		//23.2=integrals: volume - no limits
	    case 3:return("\\int\\int\\int\\nolimits_{2} 1 !3");//23.3=integrals: volume - lower only
            }
	  break;
  case 24:switch(Variation)
	   {
	   case 0:return("\\int\\limits_{2}^{3} 1 !4");	 //24.0=integrals: single - sum style - both
	   case 1:return("\\int\\limits_2 1 !3");	 //24.1=integrals: single - sum style - lower only
	   case 2:return("\\oint\\limits_{2} 1 !3");	 //24.2=integrals: contour - sum style - lower only
	   }
	  break;
  case 25:if(Wasy>=STYLE_NOTUSED)
            {
  	    switch(Variation)
	      {
	      case 0:Wasy=true;return("\\oiint\\limits_{2} 1 !3"); //25.0=integrals: area - sum style - lower only
	      case 1:Wasy=true;return("\\iint\\limits_{2} 1 !3");  //25.1=integrals: double - sum style - lower only
	      }
            }
	  switch(Variation)
	    {
	    case 0:return("circ\\int\\int\\limits_{2} 1 !3");	//25.0=integrals: area - sum style - lower only
	    case 1:return("\\int\\int\\limits_{2} 1 !3");	//25.1=integrals: double - sum style - lower only
	    }
	  break;
  case 26:if(Wasy>=STYLE_NOTUSED)
            {
	    switch(Variation)
	      {
	      case 0:Wasy=true;return("\\oiiint\\limits_2 1 !3"); //26.0=integrals: volume - sum style - lower only
	      case 1:Wasy=true;return("\\iiint\\limits_2 1 !3");  //26.1=integrals: triple - sum style - lower only
	      }
            }
	  switch(Variation)
	    {
	    case 0:return("\\int\\int\\int\\limits#2[L][STARTSUB][ENDSUB] 1 ");	//26.0=integrals: volume - sum style - lower only
	    case 1:return("\\int\\int\\int\\limits_2 1 !3");	//26.1=integrals: triple - sum style - lower only
	    }
	  break;
  case 27:switch(Variation)
	   {
	   case 0:return("\\overbrace{1}^{2} !3");	//27.0=horizontal braces: upper
	   }
	  break;
  case 28:switch(Variation)
	   {
	   case 0:return("\\underbrace{1}_{2} !3");	//28.0=horizontal braces: lower
	   }
	  break;
  case 29:switch(Variation)
	   {
	   case 0:return("\\sum\\limits_{2} 1 !3");	//29.0=sum: limits top/bottom - lower only
	   case 1:return("\\sum\\limits_{2}^{3} 1 !4");	//29.1=sum: limits top/bottom - both
	   case 2:return("\\sum 1 !2");			//29.2=sum: no limits,
	   }
	  break;
  case 30:switch(Variation)
	   {
	   case 0:return("\\sum\\nolimits_{2} 1 !3");	 //30.0=sum: limits right - lower only
	   case 1:return("\\sum\\nolimits_{2}^{3} 1 !4");//30.1=sum: limits right - both
	   }
	  break;
  case 31:switch(Variation)
	   {
	   case 0:return("\\prod\\limits_{2} 1 !3");	//31.0=product: limits top/bottom - lower only
	   case 1:return("\\prod\\limits_{2}^{3} 1 !4");//31.1=product: limits top/bottom - both
	   case 2:return("\\prod 1 !2");		//31.2=product: no limits
	   }
	  break;
  case 32:switch(Variation)
	   {
	   case 0:return("\\prod\\nolimits_{2} 1 !3");		//32.0=product: limits right - lower only
	   case 1:return("\\prod\\nolimits_{2}^{3} 1 !4");	//32.1=product: limits right - both
	   }
	  break;
  case 33:switch(Variation)
	   {
	   case 0:return("\\coprod\\limits_{2} 1 !3");		//33.0=coproduct: limits top/bottom - lower only
	   case 1:return("\\coprod\\limits_{2}^{3} 1 !4");	//33.1=coproduct: limits top/bottom - both
	   case 2:return("\\coprod 1 !2");			//33.2=coproduct: no limits
	   }
	  break;
  case 34:switch(Variation)
	   {
	   case 0:return("\\coprod\\nolimits_{2} 1 !3");	//34.0=coproduct: limits right - lower only
	   case 1:return("\\coprod\\nolimits_{2}^{3} 1 !4");	//34.1=coproduct: limits right - both
	   }
	  break;
  case 35:switch(Variation)
	   {
	   case 0:return("\\bigcup\\limits_{2} 1 !3");		//35.0=union: limits top/bottom - lower only
	   case 1:return("\\bigcup\\limits_{2}^{3} 1 !4");	//35.1=union: limits top/bottom - both
	   case 2:return("\\bigcup 1 !2");			//35.2=union: no limits
	   }
	  break;
  case 36:switch(Variation)
	   {
	   case 0:return("\\bigcup\\nolimits_{2} 1 !3");	//36.0=union: limits right - lower only
	   case 1:return("\\bigcup\\nolimits_{2}^{3} 1 !4");	//36.1=union: limits right - both
	   }
	  break;
  case 37:switch(Variation)
	   {
	   case 0:return("\\bigcap\\limits_{2} 1 !3");		//37.0=intersection: limits top/bottom - lower only
	   case 1:return("\\bigcap\\limits_{2}^{3} 1 !4");	//37.1=intersection: limits top/bottom - both
	   case 2:return("\\bigcap 1 !2");			//37.2=intersection: no limits
	   }
	  break;
  case 38:switch(Variation)
	   {
	   case 0:return("\\bigcap\\nolimits_{2} 1 !3");	//38.0=intersection: limits right - lower only
	   case 1:return("\\bigcap\\nolimits_{2}^{3} 1 !4");	//38.1=intersection: limits right - both
	   }
	  break;
  case 39:switch(Variation)
	   {
	   case 0:return("\\mathop{1}\\limits^2 ");		//39.0=limit: upper
	   case 1:return("\\mathop{1}\\limits_2 ");		//39.1=limit: lower
	   case 2:return("\\mathop{1}\\limits_{2}^3 ");		//39.2=limit: both
	   }
	  break;
  case 40:switch(Variation)
	   {
	   case 0:return("\\mathop{\\left){\\vphantom{@1{1}}}\\right.\\!\\!\\!\\!\\overline{\\,\\,\\,\\vphantom @1{1}}}\\limits^{\\displaystyle\\hfill\\,\\,\\, {2}}");		//40.0=long divisionW
	   case 1:return("\\left){\\vphantom{@1{1}}}\\right.\\!\\!\\!\\!\\overline{\\,\\,\\,\\vphantom @1{1}}");		//40.1=long divisionWO
	   }
	  break;
  case 41:switch(Variation)
	   {
	   case 0:return("\\raise@0.@7ex\\hbox{$1$}\\!\\mathord{\\left/ {\\vphantom {1 2}}\\right.\\kern-\\nulldelimiterspace}\\!\\lower@0.@7ex\\hbox{$2$}");
					//41.0=slash fraction: normal
	   case 1:return("1 \\mathord{\\left/ {\\vphantom {1 2}} \\right. \\kern-\\nulldelimiterspace} 2");
		 //return("1/2 ");	//41.1=slash fraction: baseline
	   case 2:return("\\raise@0.@5ex\\hbox{$\\scriptstyle 1$}\\kern-@0.@1em/\\kern-@0.@1@5em\\lower@0.@2@5ex\\hbox{$\\scriptstyle 2$}");
					//41.2=slash fraction: subscript-sized
	   }
	  break;
  case 42:switch(Variation)
	   {
	   case 0:return("12\\hbox{\\Large 4}^{3} ");		//42.0=INTOP: upper
	   case 1:return("13\\hbox{\\Large 4}_{2} ");		//42.1=INTOP: lower
	   case 2:return("1\\hbox{\\Large 4}_{2}^{3} ");	//42.2=INTOP: both
	   }
	  break;

  case 43:switch(Variation)
	   {
	   case 0:return("12\\mathop{\\hbox{\\Large 4}}\\limits^{3} ");	//43.0=SUMOP: upper
	   case 1:return("13\\mathop{\\hbox{\\Large 4}}\\limits_{2} ");	//43.1=SUMOP: lower
	   case 2:return("1\\mathop{\\hbox{\\Large 4}}\\limits_{2}^{3} ");	//43.2=SUMOP: both
	   }
	  break;
  case 44:switch(Variation)
	   {
	   case 0:return("{}^21 ");	//44.0=leadingSUPER
	   case 1:return("{}_12 ");	//44.1=leadingSUB
	   case 2:return("{}^{2}_{1} ");//44.2=leadingSUBSUP
	   }
	  break;
  case 45:switch(Variation)
	   {
	   case 0:return("\\left\\langle{\\left. 1\\right| 2}\\right\\rangle !3"); //45.0=Dirac: both
	   case 1:return("\\left\\langle 1\\right| !2");	//45.1=Dirac: left
	   case 2:return("\\left| 12\\right\\rangle !3");	//45.2=Dirac: right
	   }
	  break;
  case 46:switch(Variation)
	   {
	   case 0:return("\\underleftarrow{1} ");	//46.0=under arrow: left
	   case 1:return("\\underrightarrow{1} ");	//46.1=under arrow: right
	   case 2:return("\\underleftrightarrow{1} ");	//46.2=under arrow: both
	   }
	  break;
  case 47:switch(Variation)
	   {
	   case 0:return("\\overleftarrow{1} ");	//47.0=over arrow: left
	   case 1:return("\\overrightarrow{1} ");	//47.1=over arrow: right
	   case 2:return("\\overleftrightarrow{1} ");	//47.2=over arrow: both
	   }
	  break;
  }
return(NULL);
}

/*
fence: braket \left\langle {%1} \mathrel{\left| {\vphantom {%1 %2}} \right. \kern-\nulldelimiterspace} {%2} \right\rangle
fence:braket/l \left\langle %1 \right|
fence:braket/r\left| %1 \right\rangle 
fence:hbrace \underbrace %1_%2
hbrace/t \overbrace %1^%2
hbrack \underbrace %1_%2
hbrack/t \overbrace %1^%2
obrack \left[\kern-0.15em\left[ %1 \right]\kern-0.15em\right]
obrack/l \left[\kern-0.15em\left[ %1 \right.
obrcak/r \left. %1 \right]\kern-0.15em\right]

fraction:
frac/sl         \raise0.7ex\hbox{$%1$} \!\mathord{\left/ {\vphantom {%1 %2}}\right.\kern-\nulldelimiterspace}\!\lower0.7ex\hbox{$%2$}
frac/sl/base    %1 \mathord{\left/ {\vphantom {%1 %2}} \right. \kern-\nulldelimiterspace} %2
frac/sl/sm      \raise0.5ex\hbox{$\scriptstyle %1$}\kern-0.1em/\kern-0.15em\lower0.25ex\hbox{$\scriptstyle %2$}
frac/sm		\textstyle{%1 \over %2}
ldiv		\mathop{\left){\vphantom{1%1}}\right.\!\!\!\!\overline{\,\,\,\vphantom 1{%1}}}\limits^{\displaystyle\hfill\,\,\, %2}
ldiv/nq		\left){\vphantom{1%1}}\right.\!\!\!\!\overline{\,\,\,\vphantom 1{%1}}

sub sup:
intop		\mathop %3\nolimits_%1^%2
intop/b		\mathop %2\nolimits_%1
intop/t		\mathop %2\nolimits^%1
lim		\mathop %1\limits_%2
lim/t		\mathop %1\limits^%2
lim/tb		\mathop %1\limits_%2^%3
sub		_%1
sub/pre		{ }_%1
subsup		_%1^%2
subsup/pre	{ }_%1^%2
sumop		\mathop %3\limits_%1^%2
sumop/b		\mathop %2\limits_%1 
sumop/t		\mathop %2\limits^%1 
sup		^%1
sup/pre		{ }^%1


integral:
int		\int_%2^%3 %1 
int/2		\int\!\!\!\int_%2 %1 
int/2/c		\mathop{{\int\!\!\!\!\!\int}\mkern-21mu \bigcirc}\nolimits_%2 %1 
int/2/c/nol	\mathop{{\int\!\!\!\!\!\int}\mkern-21mu \bigcirc} %1 
int/2/c/sum	\mathop{{\int\!\!\!\!\!\int}\mkern-21mu \bigcirc}\limits_%2 %1 
int/2/nol	\int\!\!\!\int %1 
int/2/sum	\int\!\!\!\int\limits_%2 %1 	
int/3		\int\!\!\!\int\!\!\!\int_%2 %1 
int/3/c		\mathop{{\int\!\!\!\!\!\int\!\!\!\!\!\int}\mkern-31.2mu \bigodot}\nolimits_%2 %1 
int/3/c/nol	\mathop{{\int\!\!\!\!\!\int\!\!\!\!\!\int}\mkern-31.2mu \bigodot} %1 
int/3/c/sum	\mathop{{\int\!\!\!\!\!\int\!\!\!\!\!\int}\mkern-31.2mu \bigodot}\limits_%2 %1 	
int/3/nol	\int\!\!\!\int\!\!\!\int %1 
int/3/sum	\mathop{\int\!\!\!\int\!\!\!\int}\limits_{\kern-5.5pt %2} %1 
int/b		\int_%2 %1 
int/c		\oint_%2 %1 
int/c/nol	\oint %1 
int/c/sum	\oint\limits_%2 %1 

int/sum		\int\limits_%2^%3 %1 
int/sum/b	\int\limits_%2 %1 
*/



//[EMBELLS] format is "math template,text template" (different from all the above)
const char *Template_EMBELLS[] = {
 "",
 "",
 "\\dot{0}",			/* embDOT     */
 "\\ddot{0}",			/* embDDOT    */
 "\\mathord{\\buildrel{\\lower@3pt\\hbox{$...$}}\\over{0}}",
				/* embTDOT    */
 "0' ",				/* embPRIME   */
 "0'' ",			/* embDPRIME  */
 "`0 ",				/* embBPRIME  */
 "\\tilde{0}",			/* embTILDE   */
 "\\hat{0}",			/* embHAT     */
 "\\not{0}",			/* embNOT     */
 "\\vec{0}",			/* embRARROW  */
 "\\overleftarrow{0}",		/* embLARROW  */
 "\\mathord{\\buildrel{\\lower@3pt\\hbox{$\\scriptscriptstyle\\leftrightarrow$}}\\over{0}}",
				/* embBARROW  */
 "\\mathord{\\buildrel{\\lower@3pt\\hbox{$\\scriptscriptstyle\\rightharpoonup$}}\\over{0}}",
				/* embR1ARROW */
 "\\mathord{\\buildrel{\\lower@3pt\\hbox{$\\scriptscriptstyle\\leftharpoonup$}}\\over{0}}",
				/* embL1ARROW */
 "\\rlap{--}{0}",		/* embMBAR    */
 "\\bar{0}",			/* embOBAR    */
 "0''' ",			/* embTPRIME  */
 "\\mathord{\\buildrel{\\lower@3pt\\hbox{$\\scriptscriptstyle\\frown$}}\\over{0}}",
				/* embFROWN   */
 "\\breve{0}" 			/* embSMILE   */
};



static uint32_t GetSize(TconvertedPass1 *cq)
{
#ifdef DEBUG
  fprintf(cq->log,"\n#GetSize() ");fflush(cq->log);
#endif
uint8_t SizeTag;
uint8_t test;
uint32_t New_Size=0;

 SizeTag=cq->by & 0x0F;

 if ( SizeTag>=10 && SizeTag<=14 )
    {
/*  new_size->type  =  size_tag;
    new_size->lsize =  size_tag - FULL;
    new_size->dsize =  0;*/
    }
 else
    {
    test = fgetc(cq->wpd);
    if(test==100)
       {
/*     new_size->type  =  test;                        (*src_index)++;
       new_size->lsize =  *(src+*src_index);           (*src_index)++;
       new_size->dsize =  *(src+*src_index);           (*src_index)++;
       new_size->dsize +=  ( *(src+*src_index) << 8 ); (*src_index)++;*/
       fseek(cq->wpd,3,SEEK_CUR);  //4  3+test
       }
    else if ( test==101 )
       {
/*     new_size->type  =  101;                         (*src_index)++;
       new_size->lsize =  *(src+*src_index);            (*src_index)++;*/
       fseek(cq->wpd,1,SEEK_CUR);  //2  1+test
       }
    else
       {
/*       new_size->type  =  0;
       new_size->lsize =  *(src+*src_index);            (*src_index)++;
       new_size->dsize =  *(src+*src_index);            (*src_index)++;*/
       fseek(cq->wpd,1,SEEK_CUR);  //4  3+test
       }
  }

return New_Size;
}



static uint8_t GetNblByte(TconvertedPass1 *cq,uint8_t & Nibble)
{
 if(Nibble==0)
	{
	cq->subby=fgetc(cq->wpd);
	Nibble=1;
	return(cq->subby>>4 & 0xF);
	}
 Nibble=0;
 return(cq->subby & 0xF);
}


static uint32_t GetNblSize(TconvertedPass1 *cq,uint8_t & Nibble)
{
#ifdef DEBUG
  fprintf(cq->log,"\n#GetNblSize() ");fflush(cq->log);
#endif
uint8_t Tag,B;
int i;

  Tag=GetNblByte(cq,Nibble);
  B=0;
  i=0;
  while(B!=0xF)
	{
	B=GetNblByte(cq,Nibble);
	if(B<10) i=10*i+B;
	}
//printf("i=%d, tag=%d ",i,(int)Tag);
return(i);
}


static void Rd_nudge(FILE *f,Tnudge & nudge)
{
if(f==NULL) return;
  nudge.dx=fgetc(f);
  nudge.dy=fgetc(f);
  if(nudge.dx==128 && nudge.dy==128)
    {
    Rd_word(f, &nudge.dx);
    Rd_word(f, &nudge.dy);
    }
}


static void MTEF_LINE(TconvertedPass1_MTF *cq)
{
#ifdef DEBUG
  fprintf(cq->log,"\n#MTEF_LINE() ");fflush(cq->log);
#endif
uint8_t OldBy, Ruller;
int16_t LineSpacing;

 static int LineNumber = 0;
 int iLineNo = LineNumber++;

 if(cq->Version<=0x3FF)
      {
      cq->subby = cq->by>>4;
      if(cq->subby & 8) Rd_nudge(cq->wpd, cq->nudge);
      if(cq->subby & 4) Ruller=fgetc(cq->wpd);
      if(cq->subby & 1)	{sprintf(cq->ObjType, "LINE{}");return;}
      }
 else {
      cq->subby=fgetc(cq->wpd);	//[options]
      if(cq->subby & MtefO_NUDGE) Rd_nudge(cq->wpd, cq->nudge);
      if(cq->subby & MtefO_LINE_LSPACE) Rd_word(cq->wpd, (uint16_t *)&LineSpacing);
      if(cq->subby & MtefO_LINE_NULL) {sprintf(cq->ObjType, "LINE{}");return;}
      }

 OldBy = cq->by;
 if(cq->log!=NULL)
	fprintf(cq->log,"\n%*s[LINE #%d]",cq->recursion * 2,"",iLineNo);
 cq->recursion++;
 while(!feof(cq->wpd) && cq->by!=0) 	//Object list
	{
	if(Verbosing >= 1)		//actualise a procentage counter
	      cq->perc.Actualise(cq->ActualPos);

	cq->by = fgetc(cq->wpd);
	ProcessKeyMTEFSymbol(cq);
	}
 cq->recursion--;
 cq->by=OldBy;
 sprintf(cq->ObjType, "~LINE #%d", iLineNo);
}



static void MTEF_EMBELL(TconvertedPass1_MTF *cq)
{
#ifdef DEBUG
  fprintf(cq->log,"\n#MTEF_EMBELL() ");fflush(cq->log);
#endif
uint8_t Embell,OldBy;
const char *TemplateStr, *str2;
string StrB,StrE;
string arg[4];

  if(cq->Version<=0x3FF)
      {
      cq->subby=cq->by>>4;
      if(cq->subby & 8) Rd_nudge(cq->wpd, cq->nudge);
      }
  else
      {
      cq->subby=fgetc(cq->wpd);	//[options]
      if(cq->subby && MtefO_NUDGE) Rd_nudge(cq->wpd, cq->nudge);
      }
  Embell=fgetc(cq->wpd);

  if(Embell<sizeof(Template_EMBELLS)/sizeof(char *))
	{
	TemplateStr=Template_EMBELLS[Embell];
	StrB=cq->Formula;	cq->Formula.erase();
	OldBy=cq->by;

	str2 = strchr(TemplateStr,'0');
	while(str2!=NULL && str2>TemplateStr)
		{
		if(*(str2-1)!='@') break;  //test whether @ preceeds 0
		str2 = strchr(str2+1,'0');
		}
	if(str2!=NULL)
		{
		RemoveLastArg(StrB,arg[0]);
		if(arg[0][0]!='{' && arg[0][0]!='\\' && length(arg[0])>1)
		   {
		   StrB+=copy(arg[0],0,length(arg[0])-1);
		   arg[0]=arg[0][length(arg[0])-1];
		   }
		}

	Interpret(cq, StrB, StrE, TemplateStr, "", arg);

	cq->Formula=StrB+StrE;
	cq->by=OldBy;
	}
  else {
       if(cq->err != NULL)
	 {
	 cq->perc.Hide();
	 fprintf(cq->err,_("\nWarning: Unknown embellishment=%u!"),(unsigned)Embell);
	 }
       }

sprintf(cq->ObjType, "EMBELL %u",(unsigned)Embell);
}


static void MTEF_TMPL(TconvertedPass1_MTF *cq)
{
#ifdef DEBUG
  fprintf(cq->log,"\n#MTEF_TMPL() ");fflush(cq->log);
#endif
uint16_t Variation;
uint8_t Selector,Options;
string StrB,StrE,arg[5];
const char *TemplateStr, *str2;
uint8_t OldBy;
int i;
char Num[2]="0";

  if(cq->Version<=0x3FF)
      {
      cq->subby=cq->by>>4;
      if(cq->subby & 8) Rd_nudge(cq->wpd, cq->nudge);
      }
  else
      {
      cq->subby=fgetc(cq->wpd);	//[options]
      if(cq->subby & MtefO_NUDGE) Rd_nudge(cq->wpd, cq->nudge);
      }
  Selector=fgetc(cq->wpd);	//[selector]
  Variation=fgetc(cq->wpd);	//[variation]
  if(Variation & 0x80)
	{
	Variation=(Variation & 0x7F) | (fgetc(cq->wpd)<<8); //??why is bit No 8 masked??
	}
  Options=fgetc(cq->wpd);		//[options]

  TemplateStr=GetTemplate_MTEF(Selector, Variation, cq->MTEF_Version);
  if(TemplateStr==NULL)
     {
     if(cq->err != NULL)
	 {
	 cq->perc.Hide();
	 fprintf(cq->err,_("\nWarning: Unknown MTEF template Selector=%u Variation=%d!"),Selector,Variation);
	 }
     goto Finish;
     }

  StrB=cq->Formula;	cq->Formula.erase();
  OldBy=cq->by;
  if(cq->log!=NULL)
	fprintf(cq->log,"\n%*s[TMPL %d,%u]",cq->recursion * 2,"",Selector,Variation);
  cq->recursion++;

  str2 = strchr(TemplateStr,'0');
  while(str2>TemplateStr)
	{
	if(*(str2-1)=='@')
		str2 = strchr(str2+1,'0');
	}
  if(str2!=NULL)
	{
	RemoveLastArg(StrB,*arg);
	if(*arg[0]!='{' && *arg[0]!='\\' && length(*arg)>1)
		   {
		   StrB+=copy(*arg,0,length(*arg)-1);
		   *arg=*arg[length(*arg)-1];
		   }
	}

  i=1;
  while(!feof(cq->wpd) && cq->by!=0) 	//Object list
    {
    if(Verbosing >= 1)		//actualise a procentage counter
       cq->perc.Actualise(cq->ActualPos);

    cq->by = fgetc(cq->wpd);
    ProcessKeyMTEFSymbol(cq);
    if(cq->by>=9 && cq->by<=14)
      {cq->by = fgetc(cq->wpd);ProcessKeyMTEFSymbol(cq);}

    *Num=i+'0';
    if(strstr(TemplateStr,Num)!=NULL && i<=4)
	 arg[i]=cq->Formula;
    else StrE = StrE+cq->Formula;	//??Unused end of template??
    cq->Formula.erase();
    i++;
    }
  if(strstr(TemplateStr,"!")!=NULL) StrE.erase();

  Interpret(cq, StrB, StrE, TemplateStr, "", arg);

  cq->Formula=StrB+StrE;
  cq->recursion--;
  cq->by=OldBy;
Finish:
  sprintf(cq->ObjType, "~TMPL %d,%d",(int)Selector,(int)Variation);
}


static void MTEF_PILE(TconvertedPass1_MTF *cq)
{
#ifdef DEBUG
  fprintf(cq->log,"\n#MTEF_PILE() ");fflush(cq->log);
#endif
uint8_t Halign,Valign;
uint8_t Ruler;

  if(cq->Version<=0x3FF)
      {
      cq->subby=cq->by>>4;
      if(cq->subby & 8) Rd_nudge(cq->wpd, cq->nudge);
      }
  else
      {
      cq->subby=fgetc(cq->wpd);	//[options]
      if(cq->subby & MtefO_NUDGE) Rd_nudge(cq->wpd, cq->nudge);
      }
  Halign=fgetc(cq->wpd);	//[Halign]
  Valign=fgetc(cq->wpd);	//[Valign]
  if(cq->subby & 0x2)
	{
	Ruler=fgetc(cq->wpd);
	}

  sprintf(cq->ObjType, "!PILE");
}


static void MTEF_MATRIX(TconvertedPass1_MTF *cq)
{
#ifdef DEBUG
  fprintf(cq->log,"\n#MTEF_MATRIX() ");fflush(cq->log);
#endif
int8_t OldBy;
uint8_t x,y;
uint8_t Valign,h_just,v_just,rows,cols;
Raster1DAbstract *row_parts=NULL,*col_parts=NULL;

  if(cq->Version<=0x3FF)
      {
      cq->subby=cq->by>>4;
      if(cq->subby & 8) Rd_nudge(cq->wpd, cq->nudge);
      }
  else
      {
      cq->subby=fgetc(cq->wpd);	//[options]
      if(cq->subby & MtefO_NUDGE) Rd_nudge(cq->wpd, cq->nudge);
      }
  Valign=fgetc(cq->wpd);	//[Valign]
  h_just=fgetc(cq->wpd);
  v_just=fgetc(cq->wpd);
  rows=fgetc(cq->wpd);
  cols=fgetc(cq->wpd);
  if(feof(cq->wpd)) goto ExitMatrix;

  row_parts = CreateRaster1D(rows+1,2);
  if(row_parts==NULL || row_parts->Data1D==NULL)
      goto ExitMatrix; //no memory
  col_parts = CreateRaster1D(cols+1,2);
  if(col_parts==NULL || col_parts->Data1D)
      goto ExitMatrix; //no memory

  fread(row_parts->Data1D, (rows+3+1)/4, 1, cq->wpd); //one more lines
  fread(col_parts->Data1D, (cols+3+1)/4, 1, cq->wpd);

  if(rows<=0 || cols<=0 || feof(cq->wpd))
    {
ExitMatrix:
    if(row_parts) {delete row_parts; row_parts=NULL;}
    if(col_parts) {delete col_parts; col_parts=NULL;}
    sprintf(cq->ObjType, "!MATRIX");
    return;
    }

  cq->Formula+="\\begin{array}{";
  for(x=0;x<cols;x++)
    {
    if(col_parts->GetValue1D(x&0xFC | (~x)&3)>0) //flip bit order
      cq->Formula+='|';

    switch(h_just)
      {
      case 0: cq->Formula+='l'; break; //left
      case 1: cq->Formula+='c'; break; //center
      case 2: cq->Formula+='r'; break; //left
//    case 3:			       //relational operator alignment
//    case 4:			       //decimal point operator alignment
      default: cq->Formula+='c';
      }
    }
  if(col_parts->GetValue1D(cols&0xFC | (~cols)&3)>0)
      cq->Formula+='|';
  cq->Formula+='}';
//  cq->Formula+='\n';

  if(cq->log!=NULL)
	fprintf(cq->log,"\n%*s[MATRIX %dx%d]", cq->recursion*2, "", (int)cols, (int)rows);
  OldBy=cq->by;
  cq->recursion++;


  if(row_parts->GetValue1D(0&0xFC | (~0)&3)>0)
      cq->Formula+="\\hline ";
  for(y=0;y<rows;y++)
    {
    for(x=cols;x>0;x--)
      {
      cq->by=255;

      while(!feof(cq->wpd) && cq->by!=0) 	//Object list
	{
	if(Verbosing >= 1)		//actualise a procentage counter
	      cq->perc.Actualise(cq->ActualPos);
/*
        if(cq->by==255)
	  {
	  cq->by = fgetc(cq->wpd);
          if(cq->by==0)
            {
	    cq->by = fgetc(cq->wpd);
            ProcessKeyMTEFSymbol(cq);
            }
          }
        else */
          cq->by = fgetc(cq->wpd);

	ProcessKeyMTEFSymbol(cq);

	if(cq->by==1) cq->by=0;	//line was processed - stop a loop
	}

      if(feof(cq->wpd)) break;
      if(x>1) cq->Formula+=" & ";
      }

    if(row_parts->GetValue1D(y&0xFC | (~y)&3)>0)
      {
      cq->Formula+="\\\\ \\hline ";
      }
    else
      if(y<rows-1) cq->Formula+=" \\\\ ";
    }

  cq->Formula+="\\end{array}";
  delete row_parts; row_parts=NULL;
  delete col_parts; col_parts=NULL;

  cq->recursion--;
  cq->by = fgetc(cq->wpd);	//get a matrix terminator
  ProcessKeyMTEFSymbol(cq);	//and process it

  cq->by=OldBy;
  sprintf(cq->ObjType, "~MATRIX");
}


static void MTEF_RULER(TconvertedPass1_MTF *cq)
{
#ifdef DEBUG
  fprintf(cq->log,"\n#MTEF_RULER() ");fflush(cq->log);
#endif
uint8_t Tabs;

  if(cq->Version<=0x3FF) cq->subby=cq->by>>4;
  else cq->subby=0;

  Tabs=fgetc(cq->wpd);		//[n_stops]
  fseek(cq->wpd,3*Tabs,SEEK_CUR);

  sprintf(cq->ObjType, "!RULER");
}


static void MTEF_FONTSTYLE(TconvertedPass1_MTF *cq)
{
#ifdef DEBUG
  fprintf(cq->log,"\n#MTEF_FONTSTYLE() ");fflush(cq->log);
#endif
uint16_t DefIndex;
uint8_t CharStyle,Tface;
char c;

  if(cq->Version<=0x3FF)
	{
	cq->subby=cq->by>>4;
	Tface=fgetc(cq->wpd);			//[character style]
	CharStyle=fgetc(cq->wpd);		//1 - italic, 2 bold
	do {					//[name]
	   if(feof(cq->wpd)) break;
	   c=fgetc(cq->wpd);
	   } while(c!=0);
	}
  else  {
	cq->subby=0;

	Rd_word(cq->wpd,&DefIndex);		//[font_def_index]
	CharStyle = fgetc(cq->wpd);		//[character style]
	}

  sprintf(cq->ObjType, "!FONT_STYLE");
}


static void MTEF_CHAR(TconvertedPass1_MTF *cq)
{
#ifdef DEBUG
  fprintf(cq->log,"\n#MTEF_CHAR() ");fflush(cq->log);
#endif
uint16_t CHAR;
int8_t Typeface;
const char *ChrStr;

  if(cq->Version<=0x3FF)
      {
      cq->subby=cq->by>>4;
      if(cq->subby & 8) Rd_nudge(cq->wpd, cq->nudge);
      Typeface=fgetc(cq->wpd);
      if(cq->MTEF_Version<=2)
		{
		CHAR=fgetc(cq->wpd);
		CHAR+=256*(uint16_t)Typeface;
		}
	   else Rd_word(cq->wpd, (uint16_t *)&CHAR);
      }
  else
     {
     cq->subby=fgetc(cq->wpd);	//[options]
     if(cq->subby & MtefO_NUDGE) Rd_nudge(cq->wpd, cq->nudge);
     Typeface=fgetc(cq->wpd);

     if(cq->subby & MtefO_CHAR_ENC_CHAR_8) CHAR=fgetc(cq->wpd);
     else {
	  Rd_word(cq->wpd, (uint16_t *)&CHAR);
	  }
     }

  sprintf(cq->ObjType, "CHAR %d:0x%X %c",(int)Typeface,CHAR,(char)CHAR);

  if(CHAR>=0xEB00 && CHAR<=0xEB08) //mapped spaces into UNICODE glyphspace
    {
    switch(CHAR)
        {
	//case 0xEB00: ChrStr="&"; break;	//?? according to a formating ??
	case 0xEB01: ChrStr=""; break;		    //no space
        case 0xEB02: ChrStr="$\\, $"; break;	    //short space
	case 0xEB04: ChrStr="$\\; $"; break;	    //wide space
	case 0xEB05: ChrStr="$\\quad $"; break;	    //m space
	case 0xEB08: ChrStr="$\\kern 1pt $"; break; //1 point
        default:     ChrStr=" "; break;
        }
    }
  else ChrStr=Ext_chr_str(CHAR, cq, cq->ConvertCpg);

  if(ChrStr!=NULL)
    {
    if(strchr(ChrStr,'\\')!=NULL || strchr(ChrStr,'$')!=NULL)
	{
	string s(ChrStr);
	cq->Formula+=FixFormulaStrFromTeX(s,CHAR/256);
	}
    else
	cq->Formula+=ChrStr;
    }
#ifdef DEBUG
  fprintf(cq->log,"\n#~MTEF_CHAR() ");fflush(cq->log);
#endif
}



void ProcessKeyMTEFSymbol(TconvertedPass1_MTF *cq)
{
#ifdef DEBUG
  fprintf(cq->log,"\n#ProcessKeyMTEFSymbol() ");fflush(cq->log);
#endif
uint8_t size,by;
uint8_t Nibble;

 *cq->ObjType = '\0';

 by=cq->by;
 if(cq->Version<=0x3FF) by&=0xF;

 switch(by)
	{
	case 0: sprintf(cq->ObjType, "END");	break;	//EndRecord;
	case 1: MTEF_LINE(cq);			break;
	case 2: MTEF_CHAR(cq);			break;
	case 3: MTEF_TMPL(cq);			break;
	case 4: MTEF_PILE(cq);			break;
	case 5: MTEF_MATRIX(cq);		break;
	case 6: MTEF_EMBELL(cq);		break;
	case 7: MTEF_RULER(cq);			break;
	case 8: MTEF_FONTSTYLE(cq);		break;
	case 9: sprintf(cq->ObjType,"SIZE"); GetSize(cq);	break;
	case 10:sprintf(cq->ObjType,"FULL"); GetSize(cq);	break;
	case 11:sprintf(cq->ObjType,"SUB");  GetSize(cq);	break;
	case 12:sprintf(cq->ObjType,"SUB2"); GetSize(cq);	break;
	case 13:sprintf(cq->ObjType,"SYM");  GetSize(cq);	break;
	case 14:sprintf(cq->ObjType,"SUBSYM");GetSize(cq);	break;

	case 17:sprintf(cq->ObjType, "FONT_DEF");
		cq->subby=fgetc(cq->wpd);	//[enc_def_index]
		cq->subby=1;
		while(cq->subby!=0 && !feof(cq->wpd)) cq->subby=fgetc(cq->wpd);
		break;

	case 18:sprintf(cq->ObjType, "EQN_PREFS");
		cq->subby=fgetc(cq->wpd);	//[options]
		size=fgetc(cq->wpd);
		Nibble=0;
		while(size-->0)
			{
			GetNblSize(cq,Nibble);
			cq->ActualPos = ftell(cq->wpd);
			}
		size=fgetc(cq->wpd);	//spacing
//			printf("spacing %lX %d\n",cq->ActualPos,(int)size);
		while(size-->0)
			{
			GetNblSize(cq,Nibble);
			cq->ActualPos = ftell(cq->wpd);
			}
		size=fgetc(cq->wpd);	//stylearray
//			printf("styles %lX %d\n",cq->ActualPos,(int)size);
		fseek(cq->wpd,size*2,SEEK_CUR);
		break;

	case 19:sprintf(cq->ObjType, "ENCODING_DEF");
		cq->subby=1;
		while(cq->subby!=0 && !feof(cq->wpd)) cq->subby=fgetc(cq->wpd);
		break;
	}

 if(cq->log!=NULL)
	{
	fprintf(cq->log,"\n%*sEQ Object #0x%X",cq->recursion * 2,"",(int)cq->by);
	if(*cq->ObjType != '\0')
		  fprintf(cq->log, " [%s]", cq->ObjType);
	fprintf(cq->log," 0x%lX", ftell(cq->wpd));
	}

 cq->ActualPos = ftell(cq->wpd);
}


/*******************************************************************/
/* This procedure provides all needed processing for the first pass*/
int TconvertedPass1_MTF::Convert_first_pass(void)
{
#ifdef DEBUG
  fprintf(log,"\n#Convert_pass1_MTEF() ");fflush(log);
#endif
uint32_t fsize;
TBox Box;
bool Line=false;

  DocumentStart=ftell(wpd);

  fsize = FileSize(wpd);
  perc.Init(ftell(wpd), fsize,_("First pass MTEF:") );

  MTEF_Version=fgetc(wpd);
  if(MTEF_Version<3) ConvertCpg=GetTranslator("mtef2TOinternal");
	        else ConvertCpg=GetTranslator("unicodeTOinternal");
  fseek(wpd,DocumentStart+2,SEEK_SET);
  RdWORD_HiEnd(&Version,wpd);

  if(Version>0x3FF)
    {
    by=1;
    while(by!=0 && !feof(wpd))
	{
	by=fgetc(wpd);	//application key
	}
    by=fgetc(wpd);					//EQ options
    }

  ActualPos = ftell(wpd);
  while(ActualPos < fsize)
      {
      if(Verbosing >= 1)		//actualise a procentage counter
	   perc.Actualise(ActualPos);

      by = fgetc(wpd);
      if(by==1)
        {
        if(Line) Formula += " \\nonumber \\\\ "; //multiline formula detected
	Line=true;
        }
      else
	{
        if(by!=10) Line=false;
	}

      ProcessKeyMTEFSymbol(this);
      }

  if(!Formula.isEmpty())
    {
    int i,j;

    FixFormula(this,Formula,i,j);	//Scan for syntaktical errors.

    if(strchr(Formula(),'\2')!=NULL)	//Is there any accent unconverted?
      {
      string StrEnd, StrBeg, arg[3];

      StrEnd.erase();
      StrBeg = Formula;

      for (i = 1; i <= MAX_ACCENT_TRN_ARR; i++)
        {
        StrEnd = StrBeg + StrEnd;
        StrBeg.erase();

        while(RemoveSymbol(StrBeg, StrEnd, FmlTransTable[i].szWP))
	   {
	   arg[0].erase();
	   arg[1].erase();
	   arg[2].erase();

	   if(strstr(FmlTransTable[i].szTeX, "1") != NULL)
	       {
	       Remove1stArg(StrEnd,arg[1]);
	       }
	   if(FmlTransTable[i].szTeX[0]=='\\' || FmlTransTable[i].szTeX[0]=='{')
			   StrBeg = trim(StrBeg);
	   StrEnd = trim(StrEnd);

	   Interpret(this, StrBeg, StrEnd, FmlTransTable[i].szTeX,
		     FmlTransTable[i].szWP, arg);
	   }
        }
      Formula = StrBeg + StrEnd;
      }

    initBox(Box);
    Box.AnchorType = 0;		/*0-Paragraph, 1-Page, 2-Character*/
    Box.HorizontalPos=2;	/*0-Left, 1-Right, 2-Center, 3-Full */
    Box.Type=0;
    Box.CaptionSize=0;
    PutFormula(this,Formula(),Box);
    }

  Finalise_Conversion(this);
  return(1);
}


/*--------------------End of PASS1_MTEF--------------------*/
