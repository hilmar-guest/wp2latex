#ifndef __CP_Lib_h
#define __CP_Lib_h

#include <stdio.h>

#include "typedfs.h"


class codepage
	{
public: codepage(void)	{name=NULL;characters=NULL;MaxUsed=Allocated=0;};
	~codepage(void);

	char *name;
	uint16_t Allocated;
	uint16_t MaxUsed;
	char **characters;

	void InsertCharDesc(uint16_t position, const char *Name, bool warning=true);
        void Erase(void);
	};


class Translator
	{
public: Translator(void)  {ToCp=FromCp=NULL;Data=NULL;}
	~Translator(void) {erase();}

	const codepage *FromCp;
	const codepage *ToCp;
	uint16_t *Data;

	void erase(void);
	};


int CreateTranslator(Translator & trn, const codepage & cp1, const codepage & cp2);
int WriteTranslator(const codepage & cp_from, const codepage & cp_to, Translator &trn, FILE *F);
void WriteStringTable(FILE *F,const codepage & cp,doublelist & ll);

void CreateCodePage(string cpfilename,codepage & cp,doublelist & ll);
bool LoadCodePage(string cpfilename, codepage & cp, int ShowAll=1);
void LoadTextCodePage(string cpfilename,doublelist & ll);

string & fGets2(FILE *f, string & pstr);


#ifndef coreleft
  #define coreleft() 0l
#endif


#endif
