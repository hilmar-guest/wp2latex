//Support flags 0 undefined - no support
//		1 typedefs only
//		2 load format only
//		3 save format only
//	      >=4 both load and save
// These support flags could be predefined externally.

#ifdef NO_IMG
 #define SupportAAI 0
 #define SupportART 0
 #define SupportAVS 0
 #define SupportBMP 2	//Read procedure only
 #define SupportCUR 0
 #define SupportCUT 0
 #define SupportCSV 0
 #define SupportDIB 0
 #define SupportFITS 0
 #define SupportFTG 0
 #define SupportGIF 0
 #define SupportHRZ 0
 #define SupportICO 0
 #define SupportJPG 0
 #define SupportMAC 0
 #define SupportMAT 0
 #define SupportOKO 0
 #define SupportPBM 0
 #define SupportPCX 2	//Read procedure only
 #define SupportPNG 0
 #define SupportRAS 0
 #define SupportRAS_SUN 0
 #define SupportRAW 0
 #define SupportSGI 0
 #define SupportTGA 0
 #define SupportTIFF 0
 #define SupportTXT 0
 #define SupportTXTgm 0
 #define SupportWBMP 0
 #define SupportWEBP 0
 #define SupportWMF 1
 #define SupportWPG 1	//Compile only supporting stub, build-in reader included
 #define SupportXPM 0
#else
 #define SupportAAI 2
 #define SupportART 2 	//Read procedure only
 #define SupportAVS 2
 #define SupportBMP 2	//Read procedure only
 #define SupportCSV 2	//Read procedure only
 #define SupportDIB 2
 #define SupportFITS 2	//Read procedure only
 #define SupportGIF 2
 #define SupportICO 2	//Read procedure only
 #define SupportMAT 2	//Read procedure only
 #define SupportOKO 2	//Read procedure only
 #define SupportPBM 2
 #define SupportPCX 2	//Read procedure only
 #define SupportRAS 2	//Read procedure only
 #define SupportRAS_SUN 2
 #define SupportRAW 2	//Read procedure only
 #define SupportTGA 2	//Read procedure only
 #define SupportTIFF 2
 #define SupportTXT 2	//Read procedure only
 #define SupportTXTgm 2
 #define SupportWBMP 2
 #define SupportWMF 1	//Use another WMF reader.
 #define SupportWPG 1	//Compile only supporting stub, build-in reader included
 #define SupportXPM 2
#endif
#define SupportFTG  0
#define SupportQOI  0

/*----------------------------------------*/

#ifndef SupportART
  #define SupportART  10
#endif
#ifndef SupportBMP
  #define SupportBMP  10
#endif
#ifndef SupportCSV
  #define SupportCSV  10
#endif
#ifndef SupportCUT
  #define SupportCUT  10
#endif
#ifndef SupportDIB
  #define SupportDIB  2
#endif
#ifndef SupportEPS
  #define SupportEPS  10
#endif
#ifndef SupportFITS
  #define SupportFITS 10
#endif
#ifndef SupportGIF
  #define SupportGIF  10
#endif
#ifndef SupportHRZ
  #define SupportHRZ  2
#endif
#ifndef SupportICO
  #define SupportICO  10
#endif
#ifndef SupportMAC
  #define SupportMAC  2
#endif
#ifndef SupportMAT
  #define SupportMAT  10
#endif
#ifndef SupportOKO
  #define SupportOKO  10
#endif
#ifndef SupportPBM
  #define SupportPBM  10
#endif
#ifndef SupportPCX
  #define SupportPCX  10
#endif
#ifndef SupportRAS
  #define SupportRAS  10
#endif
#ifndef SupportRAS_SUN
  #define SupportRAS_SUN  10
#endif
#ifndef SupportRAW
  #define SupportRAW  10
#endif
#ifndef SupportTGA
  #define SupportTGA 10
#endif
#ifndef SupportTIFF
  #define SupportTIFF 10
#endif
#ifndef SupportTXT
  #define SupportTXT 10
#endif
#ifndef SupportTXTgm
  #define SupportTXTgm 10
#endif
#ifndef SupportWMF
  #define SupportWMF  2
#endif
#ifndef SupportWPG
  #define SupportWPG  10
#endif
#ifndef SupportXPM
  #define SupportXPM  10
#endif


#ifndef SupportJPG
  #define SupportJPG  2
#endif
#ifndef SupportPNG
  #define SupportPNG  2
#endif

#ifndef SupportWEBP
  #define SupportWEBP  0
#endif


#ifndef SupportFTG
  #define SupportFTG  2
#endif
