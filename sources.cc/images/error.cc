#include <stdio.h>
#include <stdlib.h>

#include "raster.h"


/** This error is emitted when some internal failure of library occurs. */
void AbstractError(const char *Name)
{
 fprintf(stderr, "Abstract function '%s' called!\n", Name==NULL?"":Name);
 abort();
}


#if 0 // #ifdef ERROR_HANDLER
 void RaiseError(int ErrNo, const void *Instance)
     {
     const char *str="?";
     switch(ErrNo >> 8)
	{
        case 1: str="Strings"; break;	// StringsId = 0x100,
        case 2: str="Sets"; break;	// SetsId =    0x200,
        case 3: str="Lists"; break;	// ListsId =   0x300,
        case 4: str="Matrix"; break;	// MatrixId =  0x400,
	case 5: str="Stack"; break;	// StackId =   0x500,
	case 6: str="Interval"; break;	// IntervalId =0x600,
	case 7: str="Raster"; break;	// RasterId =  0x700,
	case 8: str="DblList"; break;	// DblListId = 0x800
	}

     const char *Reason=NULL;
     switch(0xFF & ErrNo)
       {
       case 1: Reason="Not enough memory"; break;
       }

     if(Reason==NULL)
       fprintf(stderr, "%s::Error %d (%p)\n", str, 0xFF&ErrNo, Instance);
     else
       fprintf(stderr, "%s::Error \"%s\" (%p)\n", str, Reason, Instance);
     }
#endif
