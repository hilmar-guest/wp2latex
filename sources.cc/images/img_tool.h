/***************************************************************************
 * Unit:    raster            release 0.33                                 *
 * Purpose: Prototypes for low level pixelbuff operations.                 *
 * Modul:   img_tool.h                                                     *
 * Licency: GPL or LGPL                                                    *
 * Copyright: (c) 1998-2025 Jaroslav Fojtik                                *
 ***************************************************************************/
#ifndef _IMG_TOOL_H
#define _IMG_TOOL_H

#ifdef __cplusplus
extern "C" {
#endif

void swab16(unsigned char *block, int pixels);
void swab32(unsigned char *block, int pixels);
void swab64(unsigned char *block, int pixels);

void MaxU8(unsigned char *R1, const unsigned char *R2, unsigned size);
void MinU8(unsigned char *R1, const unsigned char *R2, unsigned size);
void MaxU16(unsigned char *R1, const unsigned char *R2, unsigned size);
void MinU16(unsigned char *R1, const unsigned char *R2, unsigned size);

void RGB_BGR(char *Data, int size);
void RGB_BGR2(unsigned char *OutData, const unsigned char *InData, unsigned PixelCount);
void RGB32_BGR24(unsigned char *OutData, const unsigned char *InData, unsigned PixelCount);
void RGBA32_BGRiA32(unsigned char *Data, int PixelCount);
void RGBA64_BGRiA64(unsigned char *Data, int PixelCount);
void RGB_Gray(unsigned char *OutData, const unsigned char *InData, unsigned PixelCount);
void RGB_Gray24(unsigned char *OutData, const unsigned char *InData, unsigned PixelCount);
void BGR_Gray24precise(unsigned char *OutData, const unsigned char *InData, unsigned PixelCount);
void BGR32_Gray24(unsigned char *OutData, const unsigned char *InData, unsigned PixelCount);
void RGB32_Gray(unsigned char *OutData, const unsigned char *InData, unsigned PixelCount);

void NotR(char *R, int size);
void OrR(void *R1, const void *R2, unsigned maxX);	// R1:=R1 or R2
void AndR(void *R1, const void *R2, unsigned maxX);	// R1:=R1 and R2
void ShrR(void *R, unsigned maxX);			// R1:=R1 shr 1
void ShlR(void *R, unsigned maxX, unsigned char zbytek);	// R1:=R1 shl 1

void YUV_RGB(unsigned char *OutData, const unsigned char *y, const unsigned char *u, const unsigned char *v, unsigned PixelCount);
void Y_UV_RGB(unsigned char *OutData, const unsigned char *y, const unsigned char *uv, unsigned PixelCount);
void YUYV_RGB(unsigned char *OutData, const unsigned char *yuyv, unsigned PixelCount);
void UYVY_RGB(unsigned char *OutData, const unsigned char *yuyv, unsigned PixelCount);
void YVYU_RGB(unsigned char *OutData, const unsigned char *yuyv, unsigned PixelCount);

#ifdef _TYPEDEFS_H_
void AddLu32u8(unsigned Size, uint32_t *Accu, unsigned char *Add);
void SubLu32u8(unsigned Size, uint32_t *Accu, unsigned char *Sub);

void V210_RGB(uint8_t *out, const uint32_t *in, int LoopCount);
void V210_Gray24(uint8_t *out, const uint32_t *in, int LoopCount);
void V210_Gray8(uint8_t *out, const uint32_t *in, int LoopCount);

void AbsCopy_u32(uint32_t *Out, uint32_t *In1, uint32_t *In2, int SizeX);
void AbsHor_u32(uint32_t *Out, uint32_t *In, int SizeX);
void Gaus1D_u32(uint32_t *line, int sizex, uint16_t times);
void GausVer1_u32(uint32_t *Out, const uint32_t *L1, const uint32_t *L2, const uint32_t *L3 , int sizex);
void DiffHor1_u32(uint32_t *Out, const uint32_t *In , int SizeX);
void DiffVer1_u32(uint32_t *Out, const uint32_t *In, const uint32_t *In2, int SizeX);
#endif

#ifdef __cplusplus
}
#endif


#endif	/* _IMG_TOOL_H */
