#note for user: You could comment out any conversion module to create shorter wp2latex
#conversion modules for all converters

OBJECTS+=pass1_1$(OBJ) #Conversion module for Wordperfect 1.x
OBJECTS+=pass1_3$(OBJ) #Conversion module for Wordperfect 2,3,4.x for MAC
OBJECTS+=pass1_4$(OBJ) #Conversion module for Wordperfect 4.x for PC
OBJECTS+=pass1_5$(OBJ) #Conversion module for Wordperfect 5.x
OBJECTS+=pass1_6$(OBJ) #Conversion module for Wordperfect 6,7..12.x
OBJECTS+=pass1c45$(OBJ) #Decryptor for Wordperfect 4.x and Wordperfect 5.x 
OBJECTS+=pass1ole$(OBJ) cole/new_cole$(OBJ) #OLE wrapper for WP7.x; WP8.x; RTF and Word

OBJECTS+=cp-trn$(OBJ) ## codepage library for HTML & RTF ##

OBJECTS+=pass1acc$(OBJ) #Accent
OBJECTS+=pass1xml$(OBJ) #XML support for HTML, AbiWord, Accent and docbook
OBJECTS+=pass1htm$(OBJ) #Conversion module for HTML
OBJECTS+=pass1abi$(OBJ) #Conversion module for AbiWord
OBJECTS+=pass1dcb$(OBJ) #Conversion module for docbook
OBJECTS+=pass1svg$(OBJ) #Conversion module for SVG

OBJECTS+=pass1rtf$(OBJ) #Conversion module for RTF
OBJECTS+=pass1mtf$(OBJ) #Conversion module for math type MTEFF
OBJECTS+=pass1wmf$(OBJ) #Conversion module for Windows MetaFile (experimental)

OBJECTS+=pass1602$(OBJ) #Conversion module for T606
OBJECTS+=pass1wrd$(OBJ) #Conversion module for  MS Word

