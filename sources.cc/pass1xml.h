/******************************************************************************
 * program:     wp2latex                                                      *
 * function:    module with XML support prototypes for HTML, AbiWord and Accent*
 * modul:       pass1xml.h                                                   *
 * description: 							      *
 ******************************************************************************/
#ifndef __PASS1_XML_H__
#define __PASS1_XML_H__


class TconvertedPass1_XML: virtual public TconvertedPass1
     {
public:
     uint32_t (*CharReader)(FILE *F);
     CpTranslator *ConvertHTML;
     string TAG;
     doublelist TAG_Args;
     uint8_t TablePos;

     void SelectTranslator(const char *CharSet);

     //cq->by = 0 (normal char), 1 (extended char &xxx;), 2 (tag <>), 3 (end tag </>), 4 comment, 5 unfinished &xxx, 6 expanded unicode, 127 fail
     void ReadXMLTag(bool MakeUpper=true);

     /*This function converts comment inside HTML*/
     void CommentXML(void);
     };


typedef enum
{
  XML_char = 0,     ///< normal char
  XML_extchar,
  XML_tag,
  XML_closetag,
  XML_comment,
  
  XML_badextchar,
  //XML_badspecchar,
  XML_unicode,

  XML_CDATA,
  
  XML_fail = 127,
} XML_STATUS;


typedef struct
{
  const char* MimeType;
  int TypeLen;
  const char *ImageExt;
} TMimeItem;

// See: https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types/Complete_list_of_MIME_types
extern const TMimeItem MimeList[8];


void base64_decode(const char *encoded_string, FILE *FOut);

inline bool is_base64(uint8_t c)
{
 return (isalnum(c) || (c == '+') || (c == '/'));
}


#endif	//#define __PASS1_XML_H__
