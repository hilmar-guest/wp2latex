/** Empty stub that could be used insteda of jobs.cc when architecture does not support multiple
 * threads. Never compile both jobs.cc and job_fix.cc in one project.
 * (c) 2013 - 2023 Jaroslav Fojtik.
 * This code could be distributed under LGPL licency. */
#include "jobs.h"


volatile JOB_COUNT_TYPE JobBase::JobCount = 0;


JobBase::JobBase()
{
  JobCount++;
}

JobBase::~JobBase()
{
  JobCount--;
}



unsigned int __stdcall JobThread(void * param)
{
  return 0;
}


int InitJobs(int ThrCount)
{
  return 1;
}


int RunJob(JobBase *js)
{
  if(js==NULL) return -1;
  js->Run();
  delete js;
  return 0;
}


void FinishJobs(int WaitMs)
{
}


void EndJobs(void)
{
}


void AbortJobs(void)
{
}
